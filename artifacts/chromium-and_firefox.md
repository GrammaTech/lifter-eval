# Chromium nad Firefox results

| tool              | rewritten |
| ---               | ---       |
| ddisasm.chrome    | 269/390   |
| ddisasm.chrome.S  | 385/390   |
| ddisasm.firefox.S | 20/21     | "libxul.so.S" faied (maybe OOM)
| ddisasm.firefox   |  6/21     |
| zipr.chrome       | 71/390    | chrome "main" binary fails (after ~15 hours)
| zipr.firefox      | 20/21     | firefox "main" binary fails


```shell
ls results/chromium.clang.Ostar.pie.nostrip/*.so results/chromium.clang.Ostar.pie.nostrip/chrome | wc
ls results/chromium.clang.Ostar.pie.nostrip.ddisasm/*.S | wc
ls results/chromium.clang.Ostar.pie.nostrip.ddisasm/*.so results/chromium.clang.Ostar.pie.nostrip.ddisasm/chrome | wc
ls results/chromium.clang.Ostar.pie.nostrip.zipr/*.so results/chromium.clang.Ostar.pie.nostrip.zipr/chrome | wc

ls results/firefox.gcc.Ostar.nopie.nostrip/*.so results/firefox.gcc.Ostar.nopie.nostrip/firefox results/firefox.gcc.Ostar.nopie.nostrip/firefox-bin | wc
ls results/firefox.gcc.Ostar.nopie.nostrip.ddisasm/*.S | wc
ls results/firefox.gcc.Ostar.nopie.nostrip.ddisasm/*.so results/firefox.gcc.Ostar.nopie.nostrip.ddisasm/firefox results/firefox.gcc.Ostar.nopie.nostrip.ddisasm/firefox-bin | wc
ls results/firefox.gcc.Ostar.nopie.nostrip.zipr/*.so results/firefox.gcc.Ostar.nopie.nostrip.zipr/firefox results/firefox.gcc.Ostar.nopie.nostrip.zipr/firefox-bin | wc
```
