# ubuntu20/ubuntu16/x86_64/clang/gcc/ollvm results

| tool                          | .func/.errno |
| ---                           | ---          |
| lighttpd.elf                  | 54/54        |
| lighttpd.ddisasm.rewritten    | 0/54         |
| lighttpd.dyninst.rewritten    | 54/54        |
| lighttpd.egalito.rewritten    | 40/40        |
| lighttpd.e9patch.rewritten    | 54/54        |
| lighttpd.multiverse.rewritten | 0/0          |
| lighttpd.reopt.rewritten      | 2/16         |
| lighttpd.retrowrite.rewritten | 0/9          |
| lighttpd.uroboros.rewritten   | 0/0          |
| lighttpd.zipr.rewritten       | 44/54        |
| nginx.elf                     | 108/108      |
| nginx.ddisasm.rewritten       | 108/108      |
| nginx.dyninst.rewritten       | 108/108      |
| nginx.egalito.rewritten       | 40/40        |
| nginx.e9patch.rewritten       | 108/108      |
| nginx.multiverse.rewritten    | 20/24        |
| nginx.reopt.rewritten         | 4/76         |
| nginx.retrowrite.rewritten    | 16/26        |
| nginx.uroboros.rewritten      | 0/0          |
| nginx.zipr.rewritten          | 98/108       |
| redis.elf                     | 48/54        |
| redis.ddisasm.rewritten       | 46/54        |
| redis.dyninst.rewritten       | 47/53        |
| redis.egalito.rewritten       | 24/30        |
| redis.e9patch.rewritten       | 48/54        |
| redis.multiverse.rewritten    | 0/0          |
| redis.reopt.rewritten         | 2/13         |
| redis.retrowrite.rewritten    | 0/0          |
| redis.uroboros.rewritten      | 0/0          |
| redis.zipr.rewritten          | 32/54        |

Note: "redis" binaries built with "-Ofast" fail the full test suite.
