#!/bin/bash
#
# This wrapper is called by "gtirb-pprinter" with arguments:
#   -o <output binary> <temporary assembler file .s> <other options>
# We need to slightly modify the assembler file ($3) to make "afl-gcc" work.
#
set -ex

SOURCE=$3
sed 's/^\.text$/\t.text/' -i $SOURCE
sed 's/^\s\{1,\}/\t/' -i $SOURCE

AFL_AS_FORCE_INSTRUMENT=1 afl-gcc $@
