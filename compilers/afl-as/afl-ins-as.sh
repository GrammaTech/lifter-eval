#!/bin/bash
#
# Run afl-as to assemble with AFL instrumentation.
#
set -ex

SOURCE=$(readlink -f $1); shift
TARGET=$(readlink -f $1); shift

AS_FLAGS=$(echo $TARGET | grep -q 'results/x86\.' && echo "--32" ||echo "")

sed 's/^\.text$/\t.text/' -i $SOURCE
sed 's/^\s\{1,\}/\t/' -i $SOURCE

temp_dir=$(mktemp -d)
pushd $temp_dir
AFL_AS_FORCE_INSTRUMENT=1 AFL_KEEP_ASSEMBLY=1 /usr/local/lib/afl/afl-as $AS_FLAGS $SOURCE
gcc a.out $@ -o $TARGET
popd
rm -r $temp_dir
