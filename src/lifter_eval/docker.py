from abc import ABC, abstractmethod
from dataclasses import dataclass
import itertools
from pathlib import Path
import shlex
from typing import Dict, List, Optional

from .parallelize import Process
from .traits import ISA, OS, Compiler, LLVMObfuscation, Traits


RESULTS_SUBDIR = "results"

HOST_ROOT = Path(__file__).absolute().parent.parent.parent
HOST_RESULTS = HOST_ROOT / RESULTS_SUBDIR

DOCKER_ROOT = Path("/lifter-eval")
DOCKER_RESULTS = DOCKER_ROOT / RESULTS_SUBDIR


def sandbox_volume(isa: ISA, os: OS):
    return f"sandbox.{isa.value}.{os.value}"


@dataclass
class DockerOptions:
    image: Optional[str] = None


class Runner:
    def __init__(
        self,
        image: "DockerImage",
        volumes: Dict[str, Path] = {},
    ):
        self.__image = image
        self.__volumes = volumes

    def image(self) -> "DockerImage":
        return self.__image

    def make_cmd(self, cmd, env=None, hostname=None):
        result = ["docker", "run", "--rm", "-i", "-w", DOCKER_ROOT.as_posix()]

        # make git work when we map a docker volume as a git submodule
        container_env = {"GIT_DISCOVERY_ACROSS_FILESYSTEM": 1}
        if env:
            container_env.update(env)
        result.extend(
            itertools.chain(*(["-e", f"{k}={v}"] for k, v in container_env.items()))
        )
        if hostname:
            result += ["--hostname", hostname]

        result.extend(
            itertools.chain(
                *(["-v", f"{k}:{v.as_posix()}"] for k, v in self.__volumes.items())
            )
        )

        result += [self.__image.name] + cmd
        return result


def _as_bash_args(cmds):
    return ["bash", "-x", "-c", " && ".join(cmds)]


class DockerImage(ABC):
    ROOT_IMAGE = "registry.gitlab.com/grammatech/lifter-eval"

    @property
    @abstractmethod
    def name(self) -> str:
        ...

    @property
    def base_image(self) -> Optional["DockerImage"]:
        return None

    @abstractmethod
    def build(self) -> Process:
        ...


class PrebuiltDockerImage(DockerImage):
    def __init__(self, name: str):
        self.__name = name

    @property
    def name(self) -> str:
        return self.__name

    def build(self) -> Process:
        description = f"using existing image: {self.__name}"
        return Process(["echo", description], description=description)


class BuildDockerImage(DockerImage):
    def __init__(self, traits: Traits):
        self.__traits = traits

    @property
    def name(self) -> str:
        return f"{self.ROOT_IMAGE}/build:{_traits_image_suffix(self.__traits)}"

    def build(self) -> Process:
        isa = self.__traits.isa
        return _build_docker_image(
            isa=isa,
            image_name=self.name,
            cwd=HOST_ROOT / "compilers",
            docker_file=self.__build_docker_file_name(),
        )

    def __build_docker_file_name(self):
        isa = self.__traits.isa
        os = self.__traits.os
        compiler = self.__traits.compiler
        isa_file_name = isa.value
        compiler_file_name = compiler.value
        if compiler in [Compiler.clang, Compiler.gcc]:
            if self.__traits.flags in LLVMObfuscation:
                compiler_file_name = "ollvm"
            else:
                if isa in [ISA.x64, ISA.arm64, ISA.arm]:
                    isa_file_name = "x86_64"
                    if os != OS.ubuntu16:
                        isa_file_name += "_arm"
                compiler_file_name = "clang_gcc"

        return f"Dockerfile.build.{isa_file_name}.{os.value}.{compiler_file_name}"


def _traits_image_suffix(traits: Traits):
    suffix = f"{traits.isa.value}.{traits.os.value}"
    if traits.compiler not in [Compiler.gcc, Compiler.clang]:
        suffix += "." + traits.compiler.value
    elif traits.flags in LLVMObfuscation:
        assert traits.compiler == Compiler.clang
        suffix += ".ollvm"
    return suffix


def _build_docker_image(
    isa,
    image_name,
    cwd: Optional[Path] = None,
    docker_file: Optional[Path] = None,
    content: Optional[bytes] = None,
) -> Process:
    assert not docker_file or not content
    platform_map = {
        ISA.x64: "linux/amd64",
        ISA.x86: "linux/amd64",
        ISA.arm64: "linux/arm64",
        ISA.arm: "linux/arm/v7",
    }
    cmd = [
        "docker",
        "buildx",
        "build",
        "--platform",
        platform_map[isa],
        "-t",
        image_name,
    ]
    if content:
        cmd += ["-"]
        input = content
    else:
        if docker_file:
            cmd += ["-f", docker_file]
            input = None
        cmd += ["."]

    return Process(cmd, cwd=cwd, input=input, description=f"building {image_name}")


class SubjectDockerImage(DockerImage):
    def __init__(self, subject: str, traits: Traits):
        self.subject = subject
        self.traits = traits

    @property
    def apt_packages(self) -> List[str]:
        return []

    @property
    def name(self) -> str:
        return f"{self.ROOT_IMAGE}/{self.subject}:{_traits_image_suffix(self.traits)}"

    @property
    def base_image(self) -> DockerImage:
        return BuildDockerImage(self.traits)

    @property
    def commands(self) -> List[str]:
        return ["RUN " + cmd for cmd in self.run_commands]

    @property
    def run_commands(self) -> List[str]:
        result = []
        if apt := self.apt_packages:
            result.append(
                "apt -y update && DEBIAN_FRONTEND=noninteractive apt -y install "
                + " ".join(apt)
            )
        return result

    def build(self) -> Process:
        content = [f"FROM {self.base_image.name}"]
        content.extend(self.commands)
        return _build_docker_image(
            self.traits.isa, image_name=self.name, content="\n".join(content).encode()
        )


class _GenericBuildDockerImage(SubjectDockerImage):
    @property
    def base_image(self) -> DockerImage:
        generic_traits = Traits(self.traits.isa, self.traits.os, Compiler.gcc)
        return BuildDockerImage(generic_traits)


class HaskellDockerImage(_GenericBuildDockerImage):
    apt_packages = ["haskell-stack"]

    @property
    def run_commands(self) -> List[str]:
        return super().run_commands + ["stack upgrade"]


class OcamlDockerImage(_GenericBuildDockerImage):
    apt_packages = ["opam"]

    @property
    def run_commands(self) -> List[str]:
        return (
            [
                "apt update -y && apt install -y software-properties-common && apt-add-repository ppa:avsm/ppa"
            ]
            + super().run_commands
            + ["opam init --disable-sandboxing"]
        )


class GoDockerImage(_GenericBuildDockerImage):
    @property
    def run_commands(self) -> List[str]:
        tar_url = "https://go.dev/dl/go1.19.5.linux-amd64.tar.gz"
        return super().run_commands + [f"wget -O- {tar_url} | tar -C /usr/local -xzf -"]

    @property
    def commands(self) -> List[str]:
        return super().commands + ["ENV PATH=$PATH:/usr/local/go/bin"]


class FortranDockerImage(_GenericBuildDockerImage):
    apt_packages = ["gfortran"]
