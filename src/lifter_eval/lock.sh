#!/bin/bash
set -e

function lock() {
lock_dir=$1

retry=1
while true; do
    err_output=$(mktemp)
    set +e
    mkdir $lock_dir 2> $err_output
    err=$?
    set -e
    cat $err_output
    if test $err -eq 0 ; then
        break
    fi

    if test $err -eq 1 && grep -q "File exists" $err_output ; then
        retry=$(( $retry + 1 ))
        if test $retry -lt 100 ; then
            timeout=5
            echo "seems like another git operation is in progress, waiting for $timeout seconds..."
            sleep $timeout
        else
            echo "giving up after $retry attempts" >&2
            exit 1
        fi
    else
        exit 1
    fi
done

trap "rmdir $lock_dir" EXIT
}