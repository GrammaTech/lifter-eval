import argparse
import json
import sys
import os 

# Parsing functions per tool used 
# Given a string extract Symbols,
# A dictionary of objects with names, address and references are filled

def ddisasm(stream):
    symbols = {}
    in_symbol = False
    for line in stream:
        if(":" in line and line.strip()[-1] == ":"):
            symbol_name = line.strip()[:-1]
            in_symbol = True 
        elif in_symbol:
            address = line.strip().split(" ")[0]
            if( len(address) == 0 or address[-1] != ":"):
                continue 
            else:
                address = address[:-1] #Get rid of colon
                try:
                    address = hex(int(address, 16))
                    in_symbol = False
                    symbols[symbol_name] = {
                        "Name" : symbol_name,
                        "Address" : address,
                        "References" : []
                        }                   
                except ValueError:
                    continue
    stream.seek(0)
    for line in stream:
        address = line.strip().split(" ")[0]
        if( len(address) == 0 or address[-1] != ":"):
            continue 
        else:
            address = address[:-1] #Get rid of colon
            try:
                address = hex(int(address, 16))
                insns = line.strip().split(" ")[-1]
                if("RIP+" in insns):
                    insns = insns.strip("[RIP+")
                    insns = insns.split("@")[0]
                    insns = insns.strip("]")
                if(insns in symbols):
                    symbols[insns]["References"].append(address)
            except ValueError:
                continue
    return symbols
    
#Takes in a stream of a assembly dissassembled by retroWrite                    
def retroWrite(stream):
    symbols = {}
    for line in stream:
        if(":" in line and line.strip()[-1] == ":"):
            if(line[2] != "C" and line[0] == "."):
                symbol_name = line.strip()[:-1]
                symbols[symbol_name] = {
                        "Name" : symbol_name,
                        "Address" : symbol_name,
                        "References" : []
                    }
    stream.seek(0)
    prev_line = ""
    for line in stream:
        insns = line.strip().split(" ")[-1]
        if(insns in symbols):
            symbols[insns]["References"].append(prev_line.strip()[:-1])
        prev_line = line
    return symbols
def dumpSymbolResult(symbols, file = sys.stdout):
    file.write(json.dumps(symbols))

def main(): 
    parser = argparse.ArgumentParser(description='Given the dissambeler and assembly file get a file with addresses.')
                    
    parser.add_argument('dissasembler', type=str,
                    help='Name of Dissasembler Used ')
                    
    parser.add_argument('assembly_file_name', type=str,
                    help='Assembly File Path')
                    
    parser.add_argument('txt_file_name', type=str, nargs= "?",
                    help='Text File to store information',
                    default = None)
                    
    args = parser.parse_args()
    if(not os.path.exists(args.assembly_file_name)):
        raise FileNotFoundError("Assembly file not found")
           
    if(args.dissasembler == "retro"):
        with open(args.assembly_file_name,"r") as file:    
            symbols = retroWrite(file);
    elif(args.dissasembler == "ddisasm"):
        with open(args.assembly_file_name,"r") as file:    
            symbols = ddisasm(file);
    else:
        raise ValueError("Dissasembler not handled")
        
    if(args.txt_file_name):
        with open(args.txt_file_name, "w") as file:
            dumpSymbolResult(symbols, file);
    else:
        dumpSymbolResult(symbols, sys.stdout);
                   
if __name__ == '__main__':
    main()
