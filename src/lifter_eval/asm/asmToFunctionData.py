import argparse
import json, sys, os
                
#Takes in a stream of a assembly dissassembled by retroWrite
#Function information, such as name, start and end address will be filled                   
def retroWrite(stream):
    functions = {}
    in_function = False 
    start_address_found = False
    start_address = ""
    end_address = ""
    for line in stream:
        if( "@function" in line):
            name = line.strip().split(" ")[1][:-1]
            in_function = True
        elif(in_function):
            address = line.strip()
            if(address[-1] == ":" and address[0:3] == ".LC"):
                if(not start_address_found):
                    start_address = address
                    start_address_found = True
                else:
                    end_address = address
            if(".size" in line):
                in_function = False
                functions[name] = {
                "Name" : name,
                "Address" : [{"Start_address" : start_address,
                "End_address": end_address}]
            }
    return functions
                
def dumpResult(functions, file = sys.stdout):
    file.write(json.dumps(functions))
    
def main():
    parser = argparse.ArgumentParser(description='Given the dissambeler and assembly file get a file with addresses.')
                    
    parser.add_argument('dissasembler', type=str,
                    help='Name of Dissasembler Used ')
                    
    parser.add_argument('assembly_file_name', type=str,
                    help='Assembly File Path')
                    
    parser.add_argument('txt_file_name', type=str, nargs= "?",
                    help='Text File to store information',
                    default = None)
                    
    args = parser.parse_args()
    if(not os.path.exists(args.assembly_file_name)):
        raise FileNotFoundError("Assembly file not found")
           
    if(args.dissasembler == "retro"):
        with open(args.assembly_file_name,"r") as file:    
            functions = retroWrite(file);
    elif(args.dissasembler == "ddisasm"):
        raise ValueError("Ddisasm supported use the gtirb script")
    else:
        raise ValueError("Dissasembler not handled")
        
    if(args.txt_file_name):
        with open(args.txt_file_name, "w") as file:
            dumpResult(functions, file);
    else:
        dumpResult(functions, sys.stdout);
                   
if __name__ == '__main__':
    main()
