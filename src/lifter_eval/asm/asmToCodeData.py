import argparse
import json
import os
import sys
from typing import List


DATA_INSTRUCTIONS = {".byte", ".string", "BYTE"}

# Parsing functions per tool used
#Given a string extract address and instructions
#If instruction is .byte put address into data
#Else put address into code

#Takes in a stream of a assembly dissassembled by ddisasm
#Data, Code will be filled with integers representing the memory addresses in decimal format
def ddisasm(stream, Data, Code):
    for line in stream:
        tmp = line.strip().split(":")
        if(len(tmp) == 2):
            if(tmp[1] != ""):
                line2parse(tmp[0],tmp[1].strip().split(" ")[0], Data, Code)

#Takes in a stream of a assembly dissassembled by retroWrite
#Data, Code will be filled with integers representing the memory addresses in decimal format
def retroWrite(stream, Data, Code):
    while True:
        try:
            line = next(stream)
            if( ":" in line):
                line2 = next(stream)
                line2 =  line2.strip().split(" ")[0]
                tmp = (line.strip()+line2).split(":")
                if(len(tmp) == 2):
                    line2parse(tmp[0].lstrip(".LC"),tmp[1], Data, Code)
        except StopIteration:
            break

# General Functions
def truncate(og_data):
    if(len(og_data) <= 1):
        return og_data
    data = sorted(og_data)
    start = data[0]
    next_address = start+1
    tmp = []
    for i in range(1,len(data)):
        if(data[i] != next_address):
            if(start != data[i-1]):
                tmp.append(hex(start) + "-" + hex(data[i-1]))
            else:
                tmp.append(hex(data[i-1]))
            start = data[i]
        next_address = data[i]+1
    if(start != data[-1]):
        tmp.append(hex(start) + "-" + hex(data[-1]))
    else:
        tmp.append(hex(data[-1]))
    return tmp

def dumpResult(Data, Code, file = sys.stdout):
    storage = {
        "Data_Addresses" : truncate(Data),
        "Code_Addresses": truncate(Code)
    }
    file.write(json.dumps(storage))

def line2parse(address, instr, Data, Code):
    try:
        address = int(address,16)
    except ValueError:
        return
    instr1 = instr.strip().split(" ")[0]
    if instr in DATA_INSTRUCTIONS:
        Data.append(address);
        if(instr1 == ".string"):
            instr.replace(".string","")
            for i in range(1,len(instr)):
                Data.append(address+i)
    else:
        Code.append(address);

def main():
    Data: List[int] = []
    Code: List[int] = []

    parser = argparse.ArgumentParser(description='Given the dissambeler and assembly file get a file with addresses.')

    parser.add_argument('dissasembler', type=str,
                    help='Name of Dissasembler Used ')

    parser.add_argument('assembly_file_name', type=str,
                    help='Assembly File Path')

    parser.add_argument('txt_file_name', type=str, nargs= "?",
                    help='Text File to store information',
                    default = None)

    args = parser.parse_args()
    if(not os.path.exists(args.assembly_file_name)):
        raise FileNotFoundError("Assembly file not found")

    if(args.dissasembler == "retro"):
        with open(args.assembly_file_name,"r") as file:
            retroWrite(file, Data, Code);
    elif(args.dissasembler == "ddisasm"):
        with open(args.assembly_file_name,"r") as file:
            ddisasm(file, Data, Code);
    else:
        raise ValueError("Dissasembler not handled")

    if(args.txt_file_name):
        with open(args.txt_file_name, "w") as file:
            dumpResult(Data, Code, file);
    else:
        dumpResult(Data, Code, sys.stdout);

if __name__ == '__main__':
    main()
