from itertools import chain
from pathlib import Path
from shlex import quote
from typing import Callable, Dict, List, Optional, Type, Union
from .docker import (
    DOCKER_ROOT,
    HOST_ROOT,
    RESULTS_SUBDIR,
    BuildDockerImage,
    DockerImage,
    DockerOptions,
    FortranDockerImage,
    GoDockerImage,
    HaskellDockerImage,
    OcamlDockerImage,
)
from .traits import ISA, OS, Compiler, LLVMObfuscation, Optimization, Pie, Spec, Traits

SUBJECTS_SUBDIR = "subjects"
SUBJECTS_DIR = HOST_ROOT / SUBJECTS_SUBDIR

TIMEOUT_EXPECTED_CODE = 124


class Env:
    def __init__(self, traits: Traits, docker_opts: Optional[DockerOptions]):
        self.__docker_opts = docker_opts
        self.__traits = traits

    def traits(self):
        return self.__traits

    def docker_options(self) -> Optional[DockerOptions]:
        return self.__docker_opts

    def root(self) -> Path:
        return DOCKER_ROOT if self.__docker_opts else HOST_ROOT

    def sandbox_path(self, subdir: Path) -> Path:
        return self.root() / SUBJECTS_SUBDIR / subdir

    def results_path(self, subdir: Path) -> Path:
        return self.root() / RESULTS_SUBDIR / subdir


class Properties:
    # To be overridden
    name: str
    supported_traits = Spec()

    def __init__(self, env: Env):
        self.__env = env

    @property
    def env(self) -> Env:
        return self.__env

    @property
    def traits(self) -> Traits:
        return self.__env.traits()


def lock_script():
    with open(HOST_ROOT / "src" / "lifter_eval" / "lock.sh") as f:
        return f.read()


class BuildProperties(Properties):
    # To be overridden
    @property
    def build(self):
        return "make"

    build_subdir = Path()
    cleanup_exclude_patterns: List[str] = []

    @property
    def binary_name(self):
        return self.name

    @property
    def binary_path_in_repo(self) -> Path:
        return self.build_subdir / self.binary_name

    @property
    def build_variables(self) -> Dict[str, str]:
        return {}

    @property
    def check_built_binary(self) -> str:
        return "\n".join(
            [
                _check_pie_bash_snippet(self.binary_path, self.traits.pie),
                _check_isa_bash_snippet(self.binary_path, self.traits.isa),
            ]
        )

    @property
    def repo_subdir(self):
        return Path(self.name)

    @property
    def repo_dir(self):
        return self.env.sandbox_path(self.repo_subdir)

    @property
    def git_submodules(self) -> List[Path]:
        return [self.repo_subdir]

    @property
    def binary_path(self):
        return self.repo_dir / self.binary_path_in_repo

    @property
    def init_filesystem(self):
        root = self.env.root().as_posix()

        def init_submodule(submodule: Path):
            submodule_subdir = (SUBJECTS_SUBDIR / submodule).as_posix()
            return f"""\
if git -C {root} cat-file -t HEAD:{submodule_subdir} | grep tree -q; then
    git -C {root} checkout -- {submodule_subdir}
else
    git -C {root} submodule update --init --recursive {submodule_subdir}
fi"""

        cmds = [lock_script(), f"lock {root}/git.lock"]
        if self.env.docker_options() is not None:
            safe_dirs = [self.env.root()] + [
                self.env.root() / SUBJECTS_SUBDIR / s for s in self.git_submodules
            ]
            cmds.extend(
                f"git config --global --add safe.directory " + d.as_posix()
                for d in safe_dirs
            )
        cmds.extend(map(init_submodule, self.git_submodules))
        result = "\n".join(cmds)
        return f"({result})"

    @property
    def patch(self):
        def patch_submodule(submodule: Path):
            patch_file_name = Path(submodule.as_posix().replace("/", ".") + ".patch")
            host_patch_path = SUBJECTS_DIR / patch_file_name
            if not host_patch_path.exists():
                return ""
            repo_path = self.env.sandbox_path(submodule).as_posix()
            patch_path = self.env.sandbox_path(patch_file_name).as_posix()
            return (
                f"git -C {repo_path} checkout -- ."
                f" && git -C {repo_path} apply {patch_path}"
            )

        return "\n".join(map(patch_submodule, self.git_submodules))

    @property
    def cleanup(self):
        def clean_submodule(submodule: Path):
            git_root = self.env.sandbox_path(submodule).as_posix()
            result = f"git -C {git_root} clean -dfx"
            if exclude := " ".join(
                "-e " + quote(p) for p in self.cleanup_exclude_patterns
            ):
                result += " " + exclude
            return result

        return "\n".join(map(clean_submodule, self.git_submodules))

    @property
    def building_image(self) -> DockerImage:
        traits = self.env.traits()
        if traits.compiler == Compiler.go:
            return GoDockerImage("build", traits)
        elif traits.compiler == Compiler.ghc:
            return HaskellDockerImage("build", traits)
        elif traits.compiler == Compiler.ocaml:
            return OcamlDockerImage("build", traits)
        elif traits.compiler == Compiler.gfortran:
            return FortranDockerImage("build", traits)
        return BuildDockerImage(self.env.traits())

    @property
    def docker_volume_suffix(self) -> str:
        traits = self.traits
        compiler = traits.compiler.value
        if traits.flags in LLVMObfuscation:
            compiler = "ollvm"
        return f".{traits.isa.value}.{traits.os.value}.{compiler}"

    @property
    def docker_volumes(self) -> Dict[str, Path]:
        suffix = self.docker_volume_suffix
        return {f"{s}{suffix}": self.env.sandbox_path(s) for s in self.git_submodules}

    @property
    def docker_hostname(self) -> Optional[str]:
        return None


class SmokeTestProperties(BuildProperties):
    smoke_expected_code = 0

    # redirect stdin from `Path` or write `str` into it or do nothing
    smoke_stdin: Optional[Union[str, Path]] = Path("/dev/null")

    smoke_timeout: int = 5  # seconds

    @property
    def smoke_args(self) -> List[str]:
        return []

    @property
    def smoke_test_binary(self):
        return self.binary_path

    @property
    def smoke_test(self) -> str:
        smoke_cmd = [self.smoke_test_binary.as_posix()] + self.smoke_args
        if timeout := self.smoke_timeout:
            smoke_cmd = ["timeout", f"{timeout}s"] + smoke_cmd
        smoke_shell_cmd = " ".join(map(quote, smoke_cmd))
        if expected_code := self.smoke_expected_code:
            if timeout and expected_code == TIMEOUT_EXPECTED_CODE:
                raise Exception(
                    f"`smoke_expected_code = {TIMEOUT_EXPECTED_CODE}` cannot be used together with `smoke_timeout`"
                )
            smoke_shell_cmd += f"; smoke_exit_code=$?; test $smoke_exit_code == {expected_code} || (exit $smoke_exit_code)"
        if (stdin := self.smoke_stdin) is not None:
            if expected_code:
                smoke_shell_cmd = _bash_output_group(smoke_shell_cmd)
            if isinstance(stdin, Path):
                smoke_shell_cmd += f" < {stdin.as_posix()}"
            else:
                smoke_shell_cmd += " <<< " + quote(stdin)
        return smoke_shell_cmd


def _bash_output_group(script: str):
    return "{ " + script + "; }"


class SmokeTestWithGrepMixin(SmokeTestProperties):
    # To be overridden
    grep_pattern: str
    grep_stderr = False
    grep_stdin = False

    @property
    def smoke_test(self) -> str:
        out = f"{self.smoke_test_binary.as_posix()}.smoke-test.stdout"
        smoke_test = super().smoke_test
        if self.smoke_expected_code and not self.smoke_stdin:
            smoke_test = _bash_output_group(smoke_test)

        # Do not use piping (|) here because it hides crashes (segfaults).
        # Also, make original output visible (use `tee`).
        fd = "2" if self.grep_stderr else "0" if self.grep_stdin else ""
        grep = f"grep -q '{self.grep_pattern}' {out}"
        return smoke_test + f" {fd}> {out} && cat {out} && {grep}"


class SubjectProperties(SmokeTestProperties):
    # To be overridden
    @property
    def evaluate(self) -> str:
        return ""

    @property
    def evaluate_subdir(self) -> Path:
        return self.repo_subdir

    @property
    def afl_args(self) -> List[str]:
        return self.smoke_args

    @property
    def afl_fuzz(self) -> str:
        script = self.env.root() / "tools" / "afl-fuzz-smoke.sh"
        result = f"timeout 2m {script.as_posix()} {self.binary_path.as_posix()}"
        if self.afl_args:
            result += " " + " ".join(map(quote, self.afl_args))
        return result


class CProperties(SubjectProperties):
    supported_traits = Spec(
        os_list=[OS.ubuntu20, OS.ubuntu16],
        compilers=[Compiler.gcc, Compiler.clang, Compiler.icx],
    )

    @property
    def cc(self):
        return self.traits.compiler.value

    @property
    def cxx(self):
        all = {Compiler.gcc: "g++", Compiler.clang: "clang++", Compiler.icx: "icpx"}
        return all.get(self.traits.compiler, "")

    @property
    def cflags(self) -> List[str]:
        result = []
        if self.traits.isa == ISA.x86:
            result.append("-m32")

        if cflags_pie := self.cflags_pie:
            result.append(cflags_pie)

        flags = self.traits.flags
        if isinstance(flags, LLVMObfuscation):
            result += self.cflags_obfuscate
        else:
            result.append(f"-{flags.value}")
        return result

    @property
    def ldflags(self):
        result = []
        if self.traits.isa == ISA.x86:
            result.append("-m32")
        if ldflags_pie := self.ldflags_pie:
            result.append(ldflags_pie)
        return result

    @property
    def cflags_pie(self) -> str:
        return "-fpie" if self.traits.pie == Pie.pie else "-fno-pie"

    @property
    def cflags_obfuscate(self) -> List[str]:
        flags = self.traits.flags
        if not isinstance(flags, LLVMObfuscation):
            return []
        return {
            LLVMObfuscation.fla: ["-mllvm", "-fla"],
            LLVMObfuscation.sub: ["-mllvm", "-sub"],
            LLVMObfuscation.bcf: ["-mllvm", "-bcf", "-mllvm", "-bcf_prob=100"],
        }[flags]

    @property
    def ldflags_pie(self) -> str:
        if self.traits.pie == Pie.pie:
            return "-pie"
        elif self.traits.compiler == Compiler.clang:
            return "-nopie"
        else:
            return "-no-pie"

    @property
    def build_variables(self) -> Dict[str, str]:
        c_cxx_flags = " ".join(self.cflags)
        return {
            "CC": self.cc,
            "CXX": self.cxx,
            "CFLAGS": c_cxx_flags,
            "CXXFLAGS": c_cxx_flags,
            "LDFLAGS": " ".join(self.ldflags),
        }


class GoProperties(SubjectProperties):
    supported_traits = Spec(
        [ISA.x64], [OS.ubuntu20], [Compiler.go], optimizations=[Optimization._0]
    )
    cache_subdir = ".gocache"
    cleanup_exclude_patterns = ["/" + cache_subdir]

    @property
    def build_variables(self) -> Dict[str, str]:
        result = {"GOMODCACHE": (self.repo_dir / self.cache_subdir).as_posix()}
        if self.traits.pie == Pie.pie:
            result["GOFLAGS"] = "-buildmode=pie"
        return result


class HaskellProperties(SubjectProperties):
    supported_traits = Spec(
        [ISA.x64],
        [OS.ubuntu20],
        [Compiler.ghc],
        [Pie.nopie],
        optimizations=[Optimization._0, Optimization._1, Optimization._2],
    )
    build_subdir = Path("bin")
    stack_root = ".stack"
    cleanup_exclude_patterns = ["/" + stack_root]

    @property
    def build(self) -> str:
        stack_root = (self.repo_dir / self.stack_root).as_posix()
        ghc_opts = "--ghc-options " + quote(self.ghc_options)
        bin_path = self.build_subdir.as_posix()
        return (
            f"mkdir -p {bin_path}\n"
            f"stack --stack-root {stack_root} --local-bin-path {bin_path} build {ghc_opts} --copy-bins --no-strip"
        )

    @property
    def ghc_options(self) -> str:
        # return "-pie -fPIC" if self.traits.pie == Pie.pie else "-no-pie"
        # return "-optc-pie -optl-pie" if self.traits.pie == Pie.pie else "-no-pie"
        return f"-{self.traits.flags.value}"


class OcamlProperties(SubjectProperties):
    supported_traits = Spec(
        [ISA.x64],
        [OS.ubuntu20],
        [Compiler.ocaml],
        [Pie.pie],
        optimizations=[Optimization._1],
    )
    build_subdir = Path("build") / "bin"

    @property
    def build(self) -> str:
        build_dir = self.build_subdir.parent.as_posix()
        return (
            f"mkdir -p {build_dir}\n"
            f"opam install -y --debug --destdir {build_dir} {self.name}\n"
        )


class FortranProperties(SubjectProperties):
    supported_traits = Spec(
        [ISA.x64],
        [OS.ubuntu20, OS.ubuntu16],
        [Compiler.gfortran],
        optimizations=[o for o in Optimization],
    )

    @property
    def compiler_flags(self) -> List[str]:
        flags = [f"-{self.traits.flags.value}"]
        if self.traits.pie == Pie.pie:
            flags += ["-fpie", "-pie"]
        else:
            flags += ["-fno-pie", "-no-pie"]
        return flags + ["-o", self.binary_name]

    @property
    def build(self) -> str:
        flags = " ".join(map(quote, self.compiler_flags))
        return f"{self.traits.compiler.value} {flags} {self.name}.f90"


def _check_pie_bash_snippet(path: Path, expected_pie: Pie):
    pie_substr = {Pie.pie: "(shared object|pie)", Pie.nopie: "executable"}[expected_pie]
    return f"file {path.as_posix()} | (grep -q -E 'LSB {pie_substr}' || (echo 'unexpected resulting pie' >&2 && false))"


def _check_isa_bash_snippet(path: Path, expected_isa: ISA):
    isa_bits = 32 if expected_isa in [ISA.x86, ISA.arm] else 64
    isa_bits_regex = f"ELF {isa_bits}-bit"
    isa_arch_regex = {
        ISA.x64: ", x86-64,",
        ISA.x86: ", Intel 80386,",
        ISA.arm64: ", ARM aarch64,",
        ISA.arm: ", ARM,",
    }[expected_isa]
    return " && ".join(
        f"file {path.as_posix()} | (grep -q '{regex}' || (echo '{msg}' >&2 && false))"
        for regex, msg in [
            (isa_bits_regex, "unexpected bit-size"),
            (isa_arch_regex, "unexpected acrchitecture"),
        ]
    )


def make_environment(traits: Traits, docker_opts: Optional[DockerOptions]) -> Env:
    return Env(traits, docker_opts)


def make_properties(
    traits: Traits,
    prop_cls: Type[SubjectProperties],
    docker_opts: Optional[DockerOptions] = None,
):
    return prop_cls(make_environment(traits, docker_opts))


RegisterHandlerT = Callable[[Type[SubjectProperties]], None]


class Registry:
    def __init__(self):
        self.__properties_by_name: Dict[str, Type[SubjectProperties]] = {}
        self.__handlers: List[RegisterHandlerT] = []

    def __iter__(self):
        for v in self.__properties_by_name.values():
            yield v

    def add_handler(self, handler: RegisterHandlerT):
        self.__handlers.append(handler)

    def register(self, props: Type[SubjectProperties]):
        self.__properties_by_name[props.name] = props
        for handler in self.__handlers:
            handler(props)

    def properties_by_name(self, name: str):
        return self.__properties_by_name[name]


registry = Registry()


def register(props: Type[SubjectProperties]):
    """Decorator to register `Properties`"""
    registry.register(props)


def properties_hook(handler: RegisterHandlerT):
    """Decorator to handle registered `SubjectProperties`"""
    registry.add_handler(handler)
