from argparse import ArgumentParser
from enum import Enum
from functools import partial
from pathlib import Path
from typing import Callable, List, Sequence, Type, Union

from .docker import HOST_RESULTS, DockerImage, DockerOptions
from .parallelize import Process, Task, hook_process_exit_code, run_tasks
from .subject import (
    all_subjects,
    make_afl_fuzz_process,
    make_build_process,
    make_eval_process,
    make_smoke_process,
)
from .subject_properties import SubjectProperties, make_properties
from .target import Target, binary_name
from .traits import (
    ISA,
    OS,
    Compiler,
    LLVMObfuscation,
    Optimization,
    Pie,
    Strip,
    Spec,
    Traits,
)


def enum_help(e: Union[Type[Enum], Sequence[Enum]]):
    return "{%s}" % ",".join(v.value for v in e)


def add_options_no_target(parser: ArgumentParser):
    parser.add_argument("--isa", nargs="+", default=[], help=enum_help(ISA), type=ISA)
    parser.add_argument("--os", nargs="+", default=[], help=enum_help(OS), type=OS)
    parser.add_argument(
        "--subject",
        nargs="+",
        default=all_subjects(),
        help="subject(s), all by default: %(default)s",
    )
    parser.add_argument(
        "--ignore-subject",
        nargs="+",
        default=[],
        help="ignore subject(s), useful to filter the default (all) subjects set",
    )
    parser.add_argument(
        "--compiler", nargs="+", help=enum_help(Compiler), type=Compiler
    )
    parser.add_argument(
        "--optimize",
        nargs="+",
        default=[],
        help=enum_help(Optimization),
        type=Optimization,
    )
    parser.add_argument(
        "--obfuscate",
        nargs="+",
        default=[],
        help=enum_help(LLVMObfuscation),
        type=LLVMObfuscation,
    )
    parser.add_argument("--pie", nargs="+", help=enum_help(Pie), type=Pie)
    parser.add_argument("--strip", nargs="+", help=enum_help(Strip), type=Strip)


def combined_subjects(opts) -> List[str]:
    exclude = set(opts.ignore_subject)
    result = list(filter(lambda x: x not in exclude, opts.subject))
    if len(result) == 0:
        raise Exception("empty subjects list")
    return result


def add_target_options(parser: ArgumentParser):
    add_options_no_target(parser)
    parser.add_argument("target_ext", nargs="+", type=str, help="target extension(s)")


def parse_subject_args(prop_cls: Type[SubjectProperties]):
    parser = ArgumentParser(description=f"build/test '{prop_cls.name}'")
    subparsers = parser.add_subparsers(
        dest="action", required=True, help="specify one of these actions"
    )

    parser.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="only print what is going to be run",
    )

    _add_image_parser(subparsers, prop_cls)
    _add_build_parser(subparsers, prop_cls)
    _add_smoke_parser(subparsers, prop_cls)

    # "evaluate" is disabled by default, check if it was overridden (to enable)
    if prop_cls.evaluate != SubjectProperties.evaluate:
        _add_eval_parser(subparsers, prop_cls)

    # "afl-fuzz" is enabled by default, check if it was overridden (to disable: `afl_fuzz = ""`)
    if prop_cls.afl_fuzz:  # type: ignore [truthy-function]
        _add_afl_parser(subparsers, prop_cls)

    result = parser.parse_args()
    result.func(result)


def _add_image_parser(subparsers, prop_cls: Type[SubjectProperties]):
    parser = subparsers.add_parser("image", help=f"build docker image")
    _add_image_traits_options(prop_cls, parser)
    parser.set_defaults(func=partial(_handle_image, parser, prop_cls))


def _add_build_parser(subparsers, prop_cls: Type[SubjectProperties]):
    parser = subparsers.add_parser("build", help=f"build binary")
    add_docker_options(parser)
    _add_image_traits_options(prop_cls, parser)

    spec = prop_cls.supported_traits
    if opt := spec.optimizations:
        parser.add_argument(
            "--optimize",
            help=f"{enum_help(opt)}, default: {opt[0].value}",
            type=Optimization,
        )

    _add_trait_option("--pie", spec.pie_options, parser)
    parser.set_defaults(func=partial(_handle_build, parser, prop_cls))


def _add_smoke_parser(subparsers, prop_cls: Type[SubjectProperties]):
    parser = subparsers.add_parser(
        "smoke-test", help=f"smoke-test '{prop_cls.name}' binary"
    )
    add_docker_options(parser)
    _add_image_traits_options(prop_cls, parser)
    _add_binary_test(parser, prop_cls, make_smoke_process)


def _add_binary_test(
    parser: ArgumentParser,
    prop_cls: Type[SubjectProperties],
    factory: Callable[[SubjectProperties, Path], Process],
):
    parser.add_argument("binary", help="binary path to test")

    def func(args):
        props = _make_props(parser, prop_cls, args, docker_options(args))
        binary = _host_path_in_results(Path(args.binary))
        _run_process(args, factory(props, binary))

    parser.set_defaults(func=func)


def _add_eval_parser(subparsers, prop_cls: Type[SubjectProperties]):
    parser = subparsers.add_parser(
        "evaluate", help=f"run functional tests for '{prop_cls.name}' binary"
    )
    add_docker_options(parser)
    _add_image_traits_options(prop_cls, parser)
    _add_binary_test(parser, prop_cls, make_eval_process)


def _add_afl_parser(subparsers, prop_cls: Type[SubjectProperties]):
    parser = subparsers.add_parser(
        "afl-fuzz", help=f"run AFL for '{prop_cls.name}' binary"
    )
    add_docker_options(parser)
    _add_image_traits_options(prop_cls, parser)
    _add_binary_test(parser, prop_cls, make_afl_fuzz_process)


def add_docker_options(parser: ArgumentParser):
    parser.add_argument(
        "--current-env",
        action="store_true",
        default=False,
        help="use current environment as is, do not use docker containers",
    )
    parser.add_argument("--image", help="use this custom docker image")


def _add_trait_option(name: str, trait_choices: Sequence[Enum], parser: ArgumentParser):
    if len(trait_choices) > 1:
        default = trait_choices[0]
        parser.add_argument(
            name,
            default=default,
            help=f"{enum_help(trait_choices)}, default: {trait_choices[0].value}",
            type=type(default),
        )


def _add_image_traits_options(
    prop_cls: Type[SubjectProperties], parser: ArgumentParser
):
    spec = prop_cls.supported_traits
    _add_trait_option("--isa", spec.isas, parser)
    _add_trait_option("--os", spec.os_list, parser)
    _add_trait_option("--compiler", spec.compilers, parser)
    if obf := spec.obfuscations:
        parser.add_argument(
            "--obfuscate",
            help=f"{enum_help(obf)}, assumes --compiler={Compiler.clang.value}",
            type=LLVMObfuscation,
        )


def _make_props(
    parser: ArgumentParser,
    prop_cls: Type[SubjectProperties],
    args,
    docker_opts: DockerOptions,
):
    traits = _image_traits(parser, args, prop_cls.supported_traits)
    return make_properties(traits, prop_cls, docker_opts)


def _handle_image(parser: ArgumentParser, prop_cls: Type[SubjectProperties], args):
    properties = _make_props(parser, prop_cls, args, DockerOptions())
    _build_image(properties.building_image, args)


def _build_image(image: DockerImage, args):
    if base_image := image.base_image:
        _build_image(base_image, args)
    _run_process(args, image.build())


def _first_traits(spec: Spec) -> Traits:
    return Traits(
        spec.isas[0],
        spec.os_list[0],
        spec.compilers[0],
        spec.pie_options[0],
        (spec.optimizations or spec.obfuscations)[0],
    )


def _handle_build(parser: ArgumentParser, prop_cls: Type[SubjectProperties], args):
    spec = prop_cls.supported_traits
    if len(spec.optimizations) and len(spec.obfuscations) and args.optimize:
        if args.obfuscate:
            parser.error('"--optimize" cannot be used togeter with "--obfuscate".')

    traits = _image_traits(parser, args, spec)
    if pie := len(spec.pie_options) > 1 and args.pie:
        traits.pie = pie
    if opt := len(spec.optimizations) > 1 and args.optimize:
        traits.flags = opt

    props = make_properties(traits, prop_cls, docker_options(args))
    binary = Path(binary_name(Target(props, Strip.nostrip)))
    _run_process(args, make_build_process(props, binary))


def _image_traits(parser: ArgumentParser, args, spec: Spec):
    default_traits = _first_traits(spec)
    if len(spec.obfuscations) > 1 and args.obfuscate:
        if args.compiler != Compiler.clang:
            parser.error('"--obfuscate" can be used only with --compiler=clang.')

    return Traits(
        (len(spec.isas) > 1 and args.isa) or default_traits.isa,
        (len(spec.os_list) > 1 and args.os) or default_traits.os,
        (len(spec.compilers) > 1 and args.compiler) or default_traits.compiler,
        default_traits.pie,
        (len(spec.obfuscations) > 1 and args.obfuscate) or default_traits.flags,
    )


def docker_options(args):
    return None if args.current_env else DockerOptions(args.image)


def _host_path_in_results(host_path: Path) -> Path:
    abs_host_path = host_path.absolute()
    try:
        return abs_host_path.relative_to(HOST_RESULTS)
    except ValueError:
        raise Exception(f"path {abs_host_path} must be relative to {HOST_RESULTS}")


def _run_process(args, process: Process):
    def exit_if_failed(exit_code: int):
        if exit_code != 0:
            exit(exit_code)

    task = Task(hook_process_exit_code(process, exit_if_failed))
    if args.dry_run:
        task.dry_run()
    else:
        run_tasks([task])
