from dataclasses import dataclass
from enum import Enum
from typing import List, Optional, Union


class ISA(Enum):
    x64 = "x86_64"
    x86 = "x86"
    arm64 = "aarch64"
    arm = "arm"


class OS(Enum):
    ubuntu16 = "ubuntu16"
    ubuntu20 = "ubuntu20"


class Pie(Enum):
    pie = "pie"
    nopie = "nopie"


class Compiler(Enum):
    gcc = "gcc"
    clang = "clang"
    icx = "icx"
    ghc = "ghc"
    ocaml = "ocaml"
    go = "go"
    gfortran = "gfortran"

class Optimization(Enum):
    _0 = "O0"
    _1 = "O1"
    _2 = "O2"
    _3 = "O3"
    s = "Os"
    fast = "Ofast"


class LLVMObfuscation(Enum):
    fla = "fla"
    sub = "sub"
    bcf = "bcf"


class Strip(Enum):
    nostrip = "nostrip"
    strip = "strip"


@dataclass
class Traits:
    isa: ISA = ISA.x64
    os: OS = OS.ubuntu20
    compiler: Compiler = Compiler.gcc
    pie: Pie = Pie.nopie
    flags: Union[Optimization, LLVMObfuscation] = Optimization._0


class Spec:
    def __init__(
        self,
        isas: Optional[List[ISA]] = None,
        os_list: Optional[List[OS]] = None,
        compilers: Optional[List[Compiler]] = None,
        pie_options: Optional[List[Pie]] = None,
        optimizations: Optional[List[Optimization]] = None,
        obfuscations: Optional[List[LLVMObfuscation]] = None,
        strip_options: Optional[List[Strip]] = None,
    ):
        self.isas = isas or list(ISA)
        self.os_list = os_list or list(OS)
        self.compilers = compilers or list(Compiler)
        self.pie_options = pie_options or list(Pie)
        self.optimizations = optimizations or (
            [] if obfuscations else list(Optimization)
        )
        self.obfuscations = obfuscations or (
            [] if optimizations else list(LLVMObfuscation)
        )
        self.strip_options = strip_options or list(Strip)
