import itertools
from pathlib import Path
import sys
from typing import Any, List, Optional, Sequence, Set, Tuple

from lifter_eval.docker import HOST_RESULTS
from lifter_eval.make import collect_targets
from lifter_eval.subject import FUNCTIONAL_TEST_EXT, SMOKE_TEST_EXT
from lifter_eval.target import binary_base, Target
from lifter_eval.traits import ISA, OS, Pie, Strip, Spec


class Colors:
    RED = "\033[31m"
    GREEN = "\033[32m"
    ENDC = "\033[m"


def summary(subjects, target_exts, spec: Spec, results_dir: Path):
    def format_target(isa: ISA, os: OS, target_ext: str):
        return f"{isa.value}.{os.value}{target_ext}"

    isa_os_targets = list(itertools.product(spec.isas, spec.os_list, target_exts))
    padding = max(len(format_target(*x)) for x in isa_os_targets)

    def add_row(transform, rate, fail):
        print(f"| {str.ljust(transform, padding)} | {rate:<{7}} | {fail:<{9}}|")

    skipped = []
    add_row("transform", "rate", "failures")
    add_row("---", "---", "---")
    for isa, os, t in isa_os_targets:
        isa_os_spec = Spec(
            [isa],
            [os],
            spec.compilers,
            spec.pie_options,
            spec.optimizations,
            spec.obfuscations,
            spec.strip_options,
        )
        ok, fail = subject_stat(subjects, t, isa_os_spec, results_dir)
        n = len(ok) + len(fail)
        if n == 0:
            skipped.append((isa.value, os.value, t))
        else:
            rate = len(ok) * 100 / n
            rate_str = f"{rate:.2f}%"
            add_row(format_target(isa, os, t), rate_str, len(fail))
    if skipped:
        print("no results for: " + _agg(skipped))


def stat(
    subjects,
    target_exts,
    spec: Optional[Spec] = None,
    results_dir: Path = HOST_RESULTS,
):
    sum_ok, sum_fail = 0, 0
    for s in subjects:
        for t in target_exts:
            ok, fail = subject_stat([s], t, spec, results_dir)
            print(f"{s}{t}")
            if ok:
                print(
                    f"{Colors.GREEN}OK:  "
                    f"{len(ok):<{2}} {agg_targets(ok)}{Colors.ENDC}"
                )
            if fail:
                print(
                    f"{Colors.RED}ERR: "
                    f"{len(fail):<{2}} {agg_targets(fail)}{Colors.ENDC}"
                )
            sum_ok += len(ok)
            sum_fail += len(fail)
            print()

    total = sum_ok + sum_fail
    assert total
    rate = float(sum_ok) / total * 100
    print(f"success rate: {rate:.2f}%")
    print(f"OK/ERR: {Colors.GREEN}{sum_ok}/{Colors.RED}{sum_fail}{Colors.ENDC}")


def _check(t: Target, ext: str, results_dir: Path):
    fname = binary_base(t) + ext
    path = results_dir / fname
    return path.exists() and check_content(path)


def check_content(path: Path):
    if path.suffix not in [SMOKE_TEST_EXT, FUNCTIONAL_TEST_EXT]:
        return True
    with open(path) as f:
        exit_code = int(f.read())
    return exit_code == 0


def subject_stat(
    subjects,
    target_ext,
    spec: Optional[Spec] = None,
    results_dir: Path = HOST_RESULTS,
) -> Tuple[List[Target], List[Target]]:
    ok: List[Target] = []
    fail: List[Target] = []
    targets, _ = collect_targets(subjects, spec)
    for t in targets:
        res = ok if _check(t, target_ext, results_dir) else fail
        res.append(t)
    return ok, fail


def agg_targets(targets: List[Target]):
    return _agg(
        list(
            (
                t.isa.value,
                t.os.value,
                t.compiler.value,
                t.flags.value,
                t.pie.value,
                t.strip.value,
            )
            for t in targets
        )
    )


def _agg(tuples: Sequence[Tuple[Any, ...]]):
    if len(tuples) == 0:
        return "-"

    n = len(tuples[0])
    result: List[Set] = [set() for _ in range(n)]
    for t in tuples:
        assert len(t) == n
        for i, x in enumerate(t):
            result[i].add(x)

    return ".".join(map(_format_agg, result))


def _format_agg(l: Set[Any]):
    if len(l) == 1:
        return str(next(iter(l)))
    return "{" + ",".join(sorted(l)) + "}"


class Transform:
    def __init__(self, ext, dependency: Optional["Transform"] = None, can_run=None):
        self.ext = ext
        self.__dependency = dependency
        self.__can_run = can_run

    def can_run(self, t: Target, results_dir: Path):
        dep = self.__dependency
        return (dep is None or _check(t, dep.ext, results_dir)) and (
            self.__can_run is None or self.__can_run(t)
        )


def csv(subjects, target_ext, spec: Spec, results_dir: Path):
    add_record = lambda r: sys.stdout.write(",".join(r) + "\n")
    if target_ext:
        transforms = [Transform(t) for t in target_ext]
    else:
        transforms = default_transforms()

    columns = ["isa", "os", "binary", "compiler", "flags", "pie", "strip"] + [
        t.ext for t in transforms
    ]
    add_record(columns)
    for s in subjects:
        _subject_csv(s, transforms, add_record, spec, results_dir)


def _subject_csv(
    subject,
    transforms: List[Transform],
    add_record,
    spec: Spec,
    results_dir: Path,
):
    targets, _ = collect_targets([subject], spec)
    for t in targets:
        record = [
            t.isa.value,
            t.os.value,
            t.subject,
            t.compiler.value,
            t.flags.value,
            t.pie.value,
            t.strip.value,
        ]
        for trans in transforms:
            if not trans.can_run(t, results_dir):
                status = "-"
            else:
                status = "OK" if _check(t, trans.ext, results_dir) else "ERR"
            record.append(status)
        add_record(record)


def transform_chain(base, exts, common_dependency):
    result = []
    dependency = common_dependency
    for ext in exts:
        prefix = f".{base}" if base else ""
        t = Transform(f"{prefix}.{ext}", dependency)
        result.append(t)
        dependency = t
    return result


def transform_chains(base, chains, common_dependency=None):
    result = [common_dependency] if common_dependency else []
    for c in chains:
        result.extend(transform_chain(base, c, common_dependency))
    return result


def default_transforms():
    ddisasm_transforms = transform_chains(
        "ddisasm",
        [["S", "rewritten", "rewritten.errno"], ["att.S", "afl", "afl.errno"]],
    )

    egalito_transforms = transform_chains(
        "egalito", [["rewritten", "rewritten.errno"], ["afl"]]
    )

    mctoll_transforms = transform_chains(
        "mctoll",
        [["rewritten", "rewritten.errno"], ["afl", "afl.errno"]],
        Transform(".mctoll.ll"),
    )

    def multiverse_can_run(t: Target):
        return t.isa == ISA.x64 and t.os == OS.ubuntu16

    multiverse_transforms = transform_chains(
        "multiverse",
        [["rewritten.errno"]],
        Transform(".multiverse.rewritten", can_run=multiverse_can_run),
    )

    def retrowrite_can_run(t: Target):
        return t.pie != Pie.nopie and t.strip != Strip.strip

    retrowrite_transforms = transform_chains(
        "retrowrite",
        [["rewritten", "rewritten.errno"], ["afl", "afl.errno"]],
        Transform(".retrowrite.S", can_run=retrowrite_can_run),
    )

    def uroboros_can_run(t: Target):
        return t.pie != Pie.pie

    uroboros_transforms = transform_chains(
        "uroboros",
        [["rewritten", "rewritten.errno"], ["afl", "afl.errno"]],
        Transform(".uroboros.S", can_run=uroboros_can_run),
    )

    zipr_transforms = transform_chains(
        "zipr", [["rewritten", "rewritten.errno"], ["afl", "afl.errno"]]
    )

    result = (
        ddisasm_transforms
        + egalito_transforms
        + mctoll_transforms
        + multiverse_transforms
        + retrowrite_transforms
        + uroboros_transforms
        + zipr_transforms
    )
    return result
