from typing import Union

from .subject_properties import SubjectProperties
from .traits import (
    ISA,
    OS,
    Compiler,
    LLVMObfuscation,
    Optimization,
    Pie,
    Strip,
    Traits,
)


BINARY_EXT = ".elf"

class Target:
    def __init__(self, properties: SubjectProperties, strip: Strip = Strip.nostrip):
        self.properties = properties
        self.strip = strip

    @property
    def traits(self) -> Traits:
        return self.properties.traits

    @property
    def isa(self) -> ISA:
        return self.traits.isa

    @property
    def os(self) -> OS:
        return self.traits.os

    @property
    def compiler(self) -> Compiler:
        return self.traits.compiler

    @property
    def pie(self) -> Pie:
        return self.traits.pie

    @property
    def flags(self) -> Union[Optimization, LLVMObfuscation]:
        return self.traits.flags

    @property
    def subject(self) -> str:
        return self.properties.name


def target_traits(t: Target):
    return Traits(t.isa, t.os, t.compiler, t.pie, t.flags)


def binary_base(t: Target):
    compiler = "ollvm" if t.flags in LLVMObfuscation else t.compiler.value
    return (
        f"{t.isa.value}.{t.os.value}"
        f".{t.subject}.{compiler}.{t.flags.value}.{t.pie.value}"
        f".{t.strip.value}"
    )


def binary_name(t: Target):
    return binary_base(t) + BINARY_EXT
