from pathlib import Path
from typing import Optional
from pytest import fixture, mark
from lifter_eval.docker import DOCKER_RESULTS, DOCKER_ROOT, HOST_ROOT, DockerOptions
from lifter_eval.subject import (
    afl_fuzz_script,
    build_script,
    evaluate_script,
    smoke_script,
)

from lifter_eval.subject_properties import (
    BuildProperties,
    CProperties,
    Env,
    GoProperties,
    SmokeTestWithGrepMixin,
    SubjectProperties,
    lock_script,
)
from lifter_eval.target import (
    ISA,
    OS,
    Compiler,
    LLVMObfuscation,
    Optimization,
    Pie,
    Traits,
)


class Fixture:
    @fixture
    def isa(self):
        return ISA.x64

    @fixture
    def os(self):
        return OS.ubuntu20

    @fixture
    def compiler(self):
        return Compiler.gcc

    @fixture
    def pie(self):
        return Pie.pie

    @fixture
    def flags(self):
        return Optimization._0

    @fixture
    def traits(self, isa, os, compiler, pie, flags):
        return Traits(isa, os, compiler, pie, flags)

    @fixture
    def docker_opts(self) -> Optional[DockerOptions]:
        return DockerOptions()

    @fixture
    def results_root(self):
        return DOCKER_RESULTS

    @fixture
    def env(self, traits: Traits, docker_opts: DockerOptions):
        return Env(traits, docker_opts)


class TestBuildProperties(Fixture):
    @fixture
    def cleanup_exclude(self):
        return []

    @fixture
    def props(self, env, cleanup_exclude):
        class Test(BuildProperties):
            name = "test_build_props"
            cleanup_exclude_patterns = cleanup_exclude

        return Test(env)

    def test_init_filesystem(self, props: BuildProperties):
        assert (
            props.init_filesystem
            == f"""\
({lock_script()}
lock {DOCKER_ROOT.as_posix()}/git.lock
git config --global --add safe.directory {DOCKER_ROOT.as_posix()}
git config --global --add safe.directory {DOCKER_ROOT.as_posix()}/subjects/test_build_props
if git -C {DOCKER_ROOT.as_posix()} cat-file -t HEAD:subjects/test_build_props | grep tree -q; then
    git -C {DOCKER_ROOT.as_posix()} checkout -- subjects/test_build_props
else
    git -C {DOCKER_ROOT.as_posix()} submodule update --init --recursive subjects/test_build_props
fi)"""
        )

    @mark.parametrize("docker_opts", [None])
    def test_init_filesystem_without_docker(self, props: BuildProperties):
        assert (
            props.init_filesystem
            == f"""\
({lock_script()}
lock {HOST_ROOT.as_posix()}/git.lock
if git -C {HOST_ROOT.as_posix()} cat-file -t HEAD:subjects/test_build_props | grep tree -q; then
    git -C {HOST_ROOT.as_posix()} checkout -- subjects/test_build_props
else
    git -C {HOST_ROOT.as_posix()} submodule update --init --recursive subjects/test_build_props
fi)"""
        )

    def test_cleanup(self, props: BuildProperties):
        assert props.cleanup == f"git -C /lifter-eval/subjects/{props.name} clean -dfx"

    @mark.parametrize("cleanup_exclude", [["test_exclude1", "test_exclude2"]])
    def test_cleanup_with_explude_patterns(self, props: BuildProperties):
        assert (
            props.cleanup
            == f"git -C /lifter-eval/subjects/{props.name} clean -dfx -e test_exclude1 -e test_exclude2"
        )

    @mark.parametrize("isa", [ISA.x64])
    @mark.parametrize("pie", [Pie.pie])
    def test_check_built_binary_x64_pie(self, props: BuildProperties):
        assert (
            props.check_built_binary
            == """\
file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q -E 'LSB (shared object|pie)' || (echo 'unexpected resulting pie' >&2 && false))
file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q 'ELF 64-bit' || (echo 'unexpected bit-size' >&2 && false)) && file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q ', x86-64,' || (echo 'unexpected acrchitecture' >&2 && false))"""
        )

    @mark.parametrize("isa", [ISA.arm])
    @mark.parametrize("pie", [Pie.nopie])
    def test_check_built_binary_arm_nopie(self, props: BuildProperties):
        assert (
            props.check_built_binary
            == """\
file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q -E 'LSB executable' || (echo 'unexpected resulting pie' >&2 && false))
file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q 'ELF 32-bit' || (echo 'unexpected bit-size' >&2 && false)) && file /lifter-eval/subjects/test_build_props/test_build_props | (grep -q ', ARM,' || (echo 'unexpected acrchitecture' >&2 && false))"""
        )


class TestCProperties(Fixture):
    @fixture
    def props(self, env):
        class Test(CProperties):
            name = "test_c_props"

        return Test(env)

    @mark.parametrize(
        "traits",
        [Traits(ISA.x64, OS.ubuntu20, Compiler.clang, Pie.pie, LLVMObfuscation.fla)],
    )
    def test_x64_ubuntu20_ollvm_pie_build(self, props: CProperties):
        assert props.build_variables == {
            "CC": "clang",
            "CXX": "clang++",
            "CFLAGS": "-fpie -mllvm -fla",
            "CXXFLAGS": "-fpie -mllvm -fla",
            "LDFLAGS": "-pie",
        }

    @mark.parametrize(
        "traits",
        [Traits(ISA.arm, OS.ubuntu20, Compiler.gcc, Pie.nopie, Optimization._0)],
    )
    def test_arm_ubuntu20_gcc_nopie_build(self, props: CProperties):
        assert props.build_variables == {
            "CC": "gcc",
            "CXX": "g++",
            "CFLAGS": "-fno-pie -O0",
            "CXXFLAGS": "-fno-pie -O0",
            "LDFLAGS": "-no-pie",
        }


class TestGoProperties(Fixture):
    @fixture
    def props(self, env):
        class Test(GoProperties):
            name = "test_go_props"

        return Test(env)

    @mark.parametrize("pie", [Pie.pie])
    def test_pie_build(self, props: GoProperties):
        assert props.build_variables == {
            "GOMODCACHE": "/lifter-eval/subjects/test_go_props/.gocache",
            "GOFLAGS": "-buildmode=pie",
        }

    @mark.parametrize("pie", [Pie.nopie])
    def test_nopie_build(self, props: GoProperties):
        assert props.build_variables == {
            "GOMODCACHE": "/lifter-eval/subjects/test_go_props/.gocache"
        }


class TestScripts(Fixture):
    @fixture
    def build_variables(self):
        return {}

    @fixture
    def props(self, env, build_variables):
        _build_variables = build_variables

        class TestProperties(SubjectProperties):
            name = "test_subject"
            binary_path_in_repo = Path("subdir/test_binary")
            init_filesystem = "test_init_filesystem"
            patch = "test_patch"
            cleanup = "test_cleanup"
            smoke_test = "binary_path_in_repo -v"
            evaluate = "make tests"
            build = "make test_subject"
            check_built_binary = "test_check_built_binary"
            build_variables = _build_variables

        return TestProperties(env)

    @fixture
    def binary_to_test(self, results_root):
        return Path(results_root / "path_to_test")

    def test_build_script(self, props, binary_to_test: Path):
        script, result_path = build_script(props, binary_to_test)
        subject_dir = "/lifter-eval/subjects/test_subject"
        assert (
            script
            == f"""\
set -ex
test_init_filesystem
test_cleanup
test_patch
cd {subject_dir}
make test_subject
test_check_built_binary
cp -p {subject_dir}/subdir/test_binary {binary_to_test.as_posix()}
"""
        )
        assert result_path == binary_to_test

    @mark.parametrize("build_variables", [{"TEST_VAR1": "value1"}])
    def test_build_script_with_variables(self, props, binary_to_test: Path):
        script, _ = build_script(props, binary_to_test)
        subject_dir = "/lifter-eval/subjects/test_subject"
        assert (
            script
            == f"""\
set -ex
test_init_filesystem
test_cleanup
test_patch
cd {subject_dir}
function build(){{
make test_subject
}}
TEST_VAR1=value1 build
test_check_built_binary
cp -p {subject_dir}/subdir/test_binary {binary_to_test.as_posix()}
"""
        )

    def test_smoke_script(self, props, binary_to_test: Path):
        script, result_path = smoke_script(props, binary_to_test)
        subject_dir = "/lifter-eval/subjects/test_subject"
        assert (
            script
            == f"""\
set -ex
cp {binary_to_test.as_posix()} {subject_dir}/subdir/test_binary
cd {subject_dir}
set +e
binary_path_in_repo -v
test_result=$?
set -e

echo $test_result > {binary_to_test.as_posix()}.errno
exit $test_result
"""
        )
        assert str(result_path) == f"{binary_to_test}.errno"

    def test_evaluate_script(self, props, binary_to_test: Path):
        script, result_path = evaluate_script(props, binary_to_test)
        subject_dir = "/lifter-eval/subjects/test_subject"
        time_file = binary_to_test.as_posix() + ".func.time"
        timeout_file = time_file + "out"
        assert (
            script
            == f"""\
set -ex
cp {binary_to_test.as_posix()} {subject_dir}/subdir/test_binary
cd {subject_dir}
set +e
rm -f {timeout_file}; /usr/bin/time -v -o {time_file} -- timeout 1h bash -e -c 'make tests' || (/usr/bin/test $? == 124 && echo "timed out: 1h" && echo 1h > {timeout_file} && exit 124)
test_result=$?
set -e
if ! diff -q {binary_to_test.as_posix()} {subject_dir}/subdir/test_binary; then
    echo "the target has been modified (rebuilt?) while running the test suite" >&2 && false
fi

echo $test_result > {binary_to_test.as_posix()}.func
exit $test_result
"""
        )
        assert str(result_path) == f"{binary_to_test}.func"

    def test_afl_fuzz_script(self, props, binary_to_test: Path):
        script, result_path = afl_fuzz_script(props, binary_to_test)
        subject_dir = "/lifter-eval/subjects/test_subject"
        assert (
            script
            == f"""\
set -ex
cp {binary_to_test.as_posix()} {subject_dir}/subdir/test_binary
cd {subject_dir}
set +e
timeout 2m /lifter-eval/tools/afl-fuzz-smoke.sh {subject_dir}/subdir/test_binary
test_result=$?
set -e

echo $test_result > {binary_to_test.as_posix()}.errno
exit $test_result
"""
        )
        assert str(result_path) == f"{binary_to_test}.errno"


class TestProftpd(Fixture):
    def test_has_patch(self, env):
        class TestProperties(SubjectProperties):
            name = "proftpd"

        assert (
            TestProperties(env).patch
            == "git -C /lifter-eval/subjects/proftpd checkout -- . && git -C /lifter-eval/subjects/proftpd apply /lifter-eval/subjects/proftpd.patch"
        )


class TestSmokeTestWithGrepMixin(Fixture):
    NAME = "test_grep"
    PATTERN = "test_pattern"
    EXPECTED_EXEC = "timeout 5s /lifter-eval/subjects/test_grep/test_grep"
    EXPECTED_REDIRECT = (
        f"/lifter-eval/subjects/{NAME}/{NAME}.smoke-test.stdout"
        f" && cat /lifter-eval/subjects/{NAME}/{NAME}.smoke-test.stdout"
        f" && grep -q '{PATTERN}' /lifter-eval/subjects/{NAME}/{NAME}.smoke-test.stdout"
    )

    @fixture
    def expected_code(self):
        return 0

    @fixture
    def expected_code_check(self, expected_code):
        return f"smoke_exit_code=$?; test $smoke_exit_code == {expected_code} || (exit $smoke_exit_code)"

    @fixture
    def stdin(self):
        return SmokeTestWithGrepMixin.smoke_stdin

    @fixture
    def stderr(self):
        return False

    @fixture
    def mixin(self, env, expected_code, stdin, stderr):
        class Test(SmokeTestWithGrepMixin):
            name = self.NAME
            grep_pattern = self.PATTERN
            smoke_expected_code = expected_code
            smoke_stdin = stdin
            grep_stderr = stderr

        return Test(env)

    def test(self, mixin: SmokeTestWithGrepMixin):
        assert mixin.smoke_test == (f"{self.EXPECTED_EXEC} < /dev/null > {self.EXPECTED_REDIRECT}")

    @mark.parametrize("stdin", [None])
    def test_without_stdin_redirect(self, mixin: SmokeTestWithGrepMixin):
        assert mixin.smoke_test == (
            f"{self.EXPECTED_EXEC} > {self.EXPECTED_REDIRECT}"
        )

    @mark.parametrize("stdin", ["test stdin"])
    def test_with_stdin(self, mixin: SmokeTestWithGrepMixin):
        assert mixin.smoke_test == (
            f"{self.EXPECTED_EXEC} <<< 'test stdin' > {self.EXPECTED_REDIRECT}"
        )

    @mark.parametrize("stderr", [True])
    def test_with_stderr(self, mixin: SmokeTestWithGrepMixin):
        assert mixin.smoke_test == (
            f"{self.EXPECTED_EXEC} < /dev/null 2> " + self.EXPECTED_REDIRECT
        )

    @mark.parametrize("expected_code", [123])
    def test_with_expected_error_code(
        self, mixin: SmokeTestWithGrepMixin, expected_code_check
    ):
        assert mixin.smoke_test == (
            f"{{ {self.EXPECTED_EXEC}; {expected_code_check}; }} < /dev/null > "
            + self.EXPECTED_REDIRECT
        )

    @mark.parametrize("stdin", ["test stdin"])
    @mark.parametrize("expected_code", [123])
    def test_with_stdin_and_expected_error_code(
        self, mixin: SmokeTestWithGrepMixin, expected_code_check
    ):
        assert mixin.smoke_test == (
            f"{{ {self.EXPECTED_EXEC}; {expected_code_check}; }} <<< 'test stdin' > "
            + self.EXPECTED_REDIRECT
        )
