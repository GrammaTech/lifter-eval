import resource
import sys
import time
from unittest.mock import Mock

from lifter_eval.parallelize import (
    Mutex,
    Process,
    ProcessScheduler,
    run_tasks,
    serial_tasks_from_commands,
    Task,
    TaskGroup,
    TaskState,
    wait_for_all,
)


class TestTaskGroup:
    def test_empty(self):
        empty = TaskGroup([])
        assert empty.empty()

    def test_non_empty(self):
        non_empty = TaskGroup([Task(Process(["process"]))])
        assert not non_empty.empty()

    def test_empty_after_popping_single_task(self):
        g = TaskGroup([Task(Process(["process"]))])
        task = g.pop_next()
        assert task
        assert g.empty()

    def test_two_parallel_tasks(self):
        g = TaskGroup([Task(Process(["process1"])), Task(Process(["process2"]))])
        first_task = g.pop_next()
        assert first_task
        second_task = g.pop_next()
        assert second_task
        assert g.empty()

    def test_next_exclusive_task_is_available_only_after_first_one_is_done(self):
        mutex = Mutex()
        g = TaskGroup(
            [
                Task(Process(["process1"]), mutex=mutex),
                Task(Process(["process2"]), mutex=mutex),
            ]
        )
        first_process = g.pop_next()
        assert first_process
        next_process = g.pop_next()
        assert next_process is None
        assert not g.empty()

        assert first_process.handle_exit_code
        first_process.handle_exit_code(0)
        next_process = g.pop_next()
        assert next_process
        assert g.empty()

    def test_next_dependent_task_is_available_only_after_dependency_is_done(self):
        dependency = Task(Process(["process1"]))
        dependant = Task(Process(["process2"]), dependencies=[dependency])
        g = TaskGroup([dependency, dependant])
        dependency_process = g.pop_next()
        assert dependency_process
        assert dependency_process.cmd == ["process1"]
        assert not g.empty()
        assert g.pop_next() is None
        assert not g.empty()

        assert dependency_process.handle_exit_code
        dependency_process.handle_exit_code(0)
        dependant_process = g.pop_next()
        assert dependant_process
        assert dependant_process.cmd == ["process2"]
        assert g.empty()

    def test_dependent_task_fails_if_dependency_failed(self):
        dependency = Task(Process(["process1"]))
        dependant = Task(Process(["process2"]), dependencies=[dependency])
        g = TaskGroup([dependency, dependant])
        dependency_process = g.pop_next()
        assert dependency_process
        dependency_process.cmd == ["process1"]
        assert dependency_process.handle_exit_code
        dependency_process.handle_exit_code(1)
        assert dependency.state() == TaskState.FAILED
        assert g.pop_next() is None
        assert g.empty()
        assert dependant.state() == TaskState.FAILED


class TestRunTasks:
    def test_serial_failure_does_not_run(self):
        scheduler = ProcessScheduler(3)
        handle_exit_code = Mock()
        tasks = serial_tasks_from_commands(
            [_one_second_cmd(1), _one_second_cmd(0)], handle_exit_code=handle_exit_code
        )
        start = time.time()
        run_tasks(tasks, scheduler)

        # Only the first 1-second task should be run
        stop = time.time()
        assert stop - start < 1.7
        assert stop - start >= 1
        handle_exit_code.assert_called_with(1)
        assert handle_exit_code.call_count == 1

    def test_serial_tasks_do_not_run_in_parallel(self):
        scheduler = ProcessScheduler(3)
        handle_exit_code = Mock()
        tasks = serial_tasks_from_commands(
            [_one_second_cmd()] * 3, handle_exit_code=handle_exit_code
        )
        start = time.time()
        run_tasks(tasks, scheduler)

        # all three 1-second tasks should be run sequentially and take 3 seconds
        elapsed = time.time() - start
        assert elapsed < 3.7
        assert elapsed >= 3
        handle_exit_code.assert_called_with(0)
        assert handle_exit_code.call_count == 3

    def test_tasks_run_in_parallel(self):
        scheduler = ProcessScheduler(3)
        handle_exit_code = Mock()
        tasks = [
            Task(Process(cmd, handle_exit_code=handle_exit_code))
            for cmd in [_one_second_cmd()] * 3
        ]
        start = time.time()
        run_tasks(tasks, scheduler)

        # all three 1-second tasks should be run in parallel
        # and take about 1 second to finish
        stop = time.time()
        assert stop - start < 1.5
        assert stop - start >= 1
        handle_exit_code.assert_called_with(0)
        assert handle_exit_code.call_count == 3

    def test_exclusive_and_non_exclusive_tasks_mixed(self):
        scheduler = ProcessScheduler(3)
        handle_exit_code = Mock()
        mutex = Mutex()
        exclusive_tasks = [
            Task(Process(cmd, handle_exit_code=handle_exit_code), mutex=mutex)
            for cmd in [_one_second_cmd()] * 2
        ]
        parallel_tasks = [
            Task(Process(cmd, handle_exit_code=handle_exit_code))
            for cmd in [_one_second_cmd()] * 4
        ]
        start = time.time()
        run_tasks(exclusive_tasks + parallel_tasks, scheduler)

        # while 2 exclusive tasks should take at least 2 seconds
        # to run sequentially, other 4 tasks should take at least 2 seconds
        # to run in parallel using 2 processes left
        elapsed = time.time() - start
        assert elapsed < 2.7
        assert elapsed >= 2
        handle_exit_code.assert_called_with(0)
        assert handle_exit_code.call_count == 6


def test():
    exit_codes = []

    def check_exit_code(expected_exit_code):
        def check(exit_code):
            assert exit_code == expected_exit_code
            exit_codes.append(exit_code)

        return check

    scheduler = ProcessScheduler(3)
    proc_num = 10
    for i in range(proc_num):
        scheduler.add(Process(_one_second_cmd(i), handle_exit_code=check_exit_code(i)))
    wait_for_all(scheduler)
    assert len(exit_codes) == proc_num


def _one_second_cmd(exit_code: int = 0):
    return [sys.executable, __file__, str(exit_code)]


def test_reproduce_too_may_open_files():
    soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
    small_limit = 100
    resource.setrlimit(resource.RLIMIT_NOFILE, (small_limit, hard))
    try:
        ps = ProcessScheduler(small_limit / 2)
        ps.add(Process(_one_second_cmd()))
        for _ in range(small_limit):
            ps.add(Process([sys.executable, "-c", ""]))
        wait_for_all(ps)
    finally:
        resource.setrlimit(resource.RLIMIT_NOFILE, (soft, hard))


def test_outputs_are_ordered():
    ps = ProcessScheduler(3)

    def add_proc(timeout, expected_order):
        ps.add(
            Process(
                [
                    sys.executable,
                    "-c",
                    f"from time import sleep; print('{expected_order}: line1', flush=True); sleep({timeout}); print('{expected_order}: line2')",
                ]
            )
        )

    add_proc(timeout=2, expected_order=1)
    add_proc(timeout=1, expected_order=3)
    add_proc(timeout=0, expected_order=2)
    wait_for_all(ps)
    # The expected output is:
    # line1
    # line2
    # line3


if __name__ == "__main__":
    code = int(sys.argv[1])
    print(f"{code}: stdout", flush=True)
    print(f"{code}: stderr", flush=True, file=sys.stderr)
    time.sleep(1)
    sys.exit(code)
