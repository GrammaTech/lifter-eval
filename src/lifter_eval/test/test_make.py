from typing import List, Optional
from pytest import fixture, mark
from lifter_eval.docker import (
    DOCKER_ROOT,
    HOST_ROOT,
    BuildDockerImage,
    DockerImage,
    DockerOptions,
    PrebuiltDockerImage,
    Runner,
)
from lifter_eval.parallelize import Process, ProcessScheduler
from lifter_eval.subject_properties import SubjectProperties, make_properties
from lifter_eval.target import Target
from lifter_eval.make import (
    MakeOptions,
    Results,
    BINARY_EXT,
    group_targets_by_runner,
    make_targets,
)
from lifter_eval.traits import (
    Compiler,
    ISA,
    LLVMObfuscation,
    Optimization,
    OS,
    Pie,
    Strip,
    Traits,
)


class TestEnv:
    @fixture
    def docker_image(self):
        return PrebuiltDockerImage("test_image")

    @fixture
    def sandbox_volume(self):
        return None

    @fixture
    def runner(self, docker_image, sandbox_volume):
        volumes = {sandbox_volume: DOCKER_ROOT} if sandbox_volume else {}
        return Runner(docker_image, volumes)

    def test_make_cmd(self, runner: Runner, docker_image: DockerImage):
        assert runner.make_cmd(["bash"]) == [
            "docker",
            "run",
            "--rm",
            "-i",
            "-w",
            "/lifter-eval",
            "-e",
            "GIT_DISCOVERY_ACROSS_FILESYSTEM=1",
            docker_image.name,
            "bash",
        ]

    @mark.parametrize("sandbox_volume", ["sandbox_volume"])
    def test_make_cmd_with_sandbox_volume(
        self, runner: Runner, docker_image: DockerImage, sandbox_volume
    ):
        assert runner.make_cmd(["bash"]) == [
            "docker",
            "run",
            "--rm",
            "-i",
            "-w",
            "/lifter-eval",
            "-e",
            "GIT_DISCOVERY_ACROSS_FILESYSTEM=1",
            "-v",
            f"{sandbox_volume}:/lifter-eval",
            docker_image.name,
            "bash",
        ]


class TestBuildDockerImage:
    @fixture
    def traits(self, isa, os, compiler, pie: Pie, flags):
        return Traits(isa, os, compiler, pie, flags)

    @fixture
    def image(self, traits):
        return BuildDockerImage(traits)

    @mark.parametrize(
        "traits",
        [Traits(ISA.x64, OS.ubuntu20, Compiler.clang, flags=LLVMObfuscation.fla)],
    )
    def test_x64_ubuntu20_ollvm_name(self, image: BuildDockerImage):
        assert (
            image.name
            == "registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20.ollvm"
        )

    @mark.parametrize(
        "traits", [Traits(ISA.arm, OS.ubuntu20, Compiler.gcc, flags=Optimization._0)]
    )
    def test_arm_ubuntu20_gcc_name(self, image: BuildDockerImage):
        assert (
            image.name
            == "registry.gitlab.com/grammatech/lifter-eval/build:arm.ubuntu20"
        )

    @mark.parametrize(
        "traits", [Traits(ISA.x64, OS.ubuntu16, Compiler.gcc, flags=Optimization.fast)]
    )
    def test_x64_ubuntu16_gcc_name(self, image: BuildDockerImage):
        assert (
            image.name
            == "registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu16"
        )

    @mark.parametrize(
        "traits",
        [Traits(ISA.arm64, OS.ubuntu20, Compiler.clang, flags=Optimization._3)],
    )
    def test_arm64_ubuntu20_clang_name(self, image: BuildDockerImage):
        assert (
            image.name
            == "registry.gitlab.com/grammatech/lifter-eval/build:aarch64.ubuntu20"
        )


def make_test_props(
    subject: str = "test", traits=Traits(), docker_opts: Optional[DockerOptions] = None
):
    class TestProperties(SubjectProperties):
        name = subject
        evaluate = "test evaluate"

    return make_properties(traits, TestProperties, docker_opts)


class TestGroupTargetsByEnv:
    @fixture
    def props(self, docker_opts):
        return make_test_props(docker_opts=docker_opts)

    @fixture
    def target(self, props):
        return Target(props)

    @fixture
    def targets(self, target):
        return [target]

    @fixture
    def docker_opts(self) -> Optional[DockerOptions]:
        return None

    @fixture
    def batch(self):
        return False

    @fixture
    def targets_by_runner(self, targets, batch):
        return list(group_targets_by_runner(targets, [BINARY_EXT], batch=batch))

    @fixture
    def runner(self, targets_by_runner):
        assert len(targets_by_runner) == 1
        return targets_by_runner[0][0]

    @mark.parametrize(
        "targets",
        [[Target(make_test_props(s)) for s in ["subject1", "subject2"]]],
    )
    def test_two_different_subject_targets(self, targets, targets_by_runner):
        assert len(targets_by_runner) == 2

        runner1, runner1_targets = targets_by_runner[0]
        assert len(runner1_targets) == 1
        assert runner1_targets[0] == (targets[0], BINARY_EXT)

        runner2, runner2_targets = targets_by_runner[1]
        assert len(runner2_targets) == 1
        assert runner2_targets[0] == (targets[1], BINARY_EXT)

        assert runner2.make_cmd([]) == runner1.make_cmd([])

    @mark.parametrize("batch", [True])
    @mark.parametrize(
        "targets", [[Target(make_test_props(s)) for s in ["subject1", "subject2"]]]
    )
    def test_two_different_subject_targets_batched(self, targets, targets_by_runner):
        assert len(targets_by_runner) == 1
        _, runner_targets = targets_by_runner[0]
        assert runner_targets == [(t, BINARY_EXT) for t in targets]

    @mark.parametrize("docker_opts", [None, DockerOptions()])
    def test_runner(self, runner, docker_opts):
        expected = [
            "docker",
            "run",
            "--rm",
            "-i",
            "-w",
            "/lifter-eval",
            "-e",
            "GIT_DISCOVERY_ACROSS_FILESYSTEM=1",
        ]
        expected += ["-v", f"{HOST_ROOT}:/lifter-eval"]
        if docker_opts:
            expected += ["-v", f'{HOST_ROOT / "results"}:/lifter-eval/results']
            expected += ["-v", f"test.x86_64.ubuntu20.gcc:/lifter-eval/subjects/test"]
        expected += ["registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20"]
        assert runner.make_cmd([]) == expected

    @mark.parametrize(
        "targets,batch",
        [
            (
                [
                    Target(make_test_props("subject", Traits(isa)))
                    for isa in [ISA.x64, ISA.x86]
                ],
                True,
            )
        ],
    )
    def test_targets_cannot_be_batched_if_require_different_docker_volumes(
        self, targets_by_runner
    ):
        assert len(targets_by_runner) == 2

    @mark.parametrize(
        "targets",
        [
            [
                Target(make_test_props("subject", Traits(isa), DockerOptions()))
                for isa in [ISA.x64, ISA.x86]
            ]
        ],
    )
    @mark.parametrize("batch", [True])
    def test_targets_cannot_be_batched_if_require_different_sandbox(
        self, targets_by_runner
    ):
        assert len(targets_by_runner) == 2


class ProcessSchedulerStub(ProcessScheduler):
    """
    This scheduler records what processes were run.
    The processes run in parallel are grouped into a single group.
    """

    def __init__(self, process_groups: List[List[str]]):
        self.__processes: List[Process] = []
        self.__result = process_groups

    def add(self, process: Process):
        self.__processes.append(process)

    def wait_for_any_process(self) -> bool:
        group = []
        for p in self.__processes:
            handle_exit_code = p.handle_exit_code
            assert handle_exit_code
            handle_exit_code(0)
            description = p.description
            assert description
            group.append(description)

        self.__processes.clear()
        self.__result.append(group)
        return False


class MakeTargetsFixture:
    @fixture
    def subjects(self):
        return ["test_subject"]

    @fixture
    def traits(self):
        return Traits()

    @fixture
    def strip_opt(self):
        return [Strip.nostrip]

    @fixture
    def build_images(self):
        return False

    @fixture
    def targets(self, subjects, traits, strip_opt):
        return [
            Target(make_test_props(subject, traits), strip=strip)
            for strip in strip_opt
            for subject in subjects
        ]

    @fixture
    def process_groups(self, targets, target_ext, build_images) -> List[List[str]]:
        result: List[List[str]] = []
        make_opts = MakeOptions(build_images=build_images)
        docker_opts = DockerOptions()
        make_targets(
            targets,
            target_ext,
            ProcessSchedulerStub(result),
            make_opts,
            docker_opts,
            Results(),
        )
        return result


@mark.parametrize("target_ext", [[".elf"]])
class TestMakeOriginalBinaries(MakeTargetsFixture):
    def test_build(self, process_groups):
        assert process_groups == [
            ["making x86_64.ubuntu20.test_subject.gcc.O0.nopie.nostrip.elf"],
        ]

    @mark.parametrize("strip_opt", [[Strip.strip]])
    def test_strip(self, process_groups):
        assert process_groups == [
            ["making x86_64.ubuntu20.test_subject.gcc.O0.nopie.strip.elf"],
        ]

    @mark.parametrize("strip_opt", [Strip, reversed(Strip)])
    def test_strip_original_binary_after_building_regardles_of_input_order(
        self, process_groups
    ):
        assert process_groups == [
            ["making x86_64.ubuntu20.test_subject.gcc.O0.nopie.nostrip.elf"],
            ["making x86_64.ubuntu20.test_subject.gcc.O0.nopie.strip.elf"],
        ]

    @mark.parametrize(
        "targets",
        [
            [
                Target(make_test_props("test_subject", Traits(flags=f)))
                for f in [Optimization._0, Optimization._1]
            ]
        ],
    )
    def test_build_sequentially_if_the_same_sandbox(self, process_groups):
        assert process_groups == [
            ["making x86_64.ubuntu20.test_subject.gcc.O0.nopie.nostrip.elf"],
            ["making x86_64.ubuntu20.test_subject.gcc.O1.nopie.nostrip.elf"],
        ]

    @mark.parametrize("subjects", [["test_subject1", "test_subject2"]])
    def test_build_in_parallel_if_different_sandboxes(self, process_groups):
        assert process_groups == [
            [
                "making x86_64.ubuntu20.test_subject1.gcc.O0.nopie.nostrip.elf",
                "making x86_64.ubuntu20.test_subject2.gcc.O0.nopie.nostrip.elf",
            ]
        ]


@mark.parametrize("build_images", [True])
class TestMakeTargetsAndBuildImages(MakeTargetsFixture):
    @mark.parametrize(
        "target_ext", [[".elf"], [".elf.errno"], [".elf.func"], [".afl.errno"]]
    )
    def test_build_image_before_building_original_binary(
        self, target_ext, process_groups
    ):
        assert process_groups == [
            [
                "building registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20"
            ],
            [
                f"making x86_64.ubuntu20.test_subject.gcc.O0.nopie.nostrip{target_ext[0]}"
            ],
        ]

    @mark.parametrize("target_ext", [[".zipr.rewritten"]])
    def test_transform_with_pre_built_image(self, process_groups):
        assert process_groups == [
            ["using existing image: registry.gitlab.com/grammatech/lifter-eval/zipr"],
            [
                f"making x86_64.ubuntu20.test_subject.gcc.O0.nopie.nostrip.zipr.rewritten"
            ],
        ]

    @mark.parametrize("target_ext", [[".elf"]])
    @mark.parametrize("traits", [Traits(compiler=Compiler.go)])
    def test_build_base_image_too(self, process_groups):
        assert process_groups == [
            [
                "building registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20"
            ],
            [
                "building registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20.go"
            ],
            [f"making x86_64.ubuntu20.test_subject.go.O0.nopie.nostrip.elf"],
        ]

    @mark.parametrize("subjects", [["test_subject1", "test_subject2"]])
    @mark.parametrize("target_ext", [[".elf"]])
    @mark.parametrize("traits", [Traits(compiler=Compiler.go)])
    def test_build_base_images_once(self, process_groups):
        assert process_groups == [
            [
                "building registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20"
            ],
            [
                "building registry.gitlab.com/grammatech/lifter-eval/build:x86_64.ubuntu20.go"
            ],
            [
                f"making x86_64.ubuntu20.test_subject1.go.O0.nopie.nostrip.elf",
                f"making x86_64.ubuntu20.test_subject2.go.O0.nopie.nostrip.elf",
            ],
        ]
