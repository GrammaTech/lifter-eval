from typing import List
import lifter_eval.asm.asmToCodeData as asmToCodeData
import lifter_eval.asm.asmToFunctionData as asmToFunctionData

def test_truncate():
    tmp = [1,2,3,4,5,6,7,8,9,10]
    assert asmToCodeData.truncate(tmp) == ['0x1-0xa']
    print("Here")
    tmp = [1,2,3,4,5,6,7,8,9,10,12,13,14]
    assert asmToCodeData.truncate(tmp) == ['0x1-0xa', '0xc-0xe']
    tmp = [1,2,3,4,5,6,7,8,9,10,12,29]
    assert asmToCodeData.truncate(tmp) == ['0x1-0xa', '0xc', '0x1d']
    tmp = [1,10,2,11,3,12,4,56,13,5,14]
    assert asmToCodeData.truncate(tmp) == ['0x1-0x5', '0xa-0xe', '0x38']

def test_ddisasm_code():
   Code: List[int] = []
   Data: List[int] = []
   tmp = ["4012ab:   nop",
          "4012ac:   nop",
          "4012ad:   nop"]
   asmToCodeData.ddisasm(iter(tmp), Data, Code)
   assert Code == [4199083, 4199084, 4199085]
   assert Data == []

   tmp = ["408000: BYTE 000H",
          "408001: BYTE 000H",
          "408002: BYTE 000H"]
   asmToCodeData.ddisasm(iter(tmp), Data, Code)
   assert Code == [4199083, 4199084, 4199085]
   assert Data == [4227072, 4227073, 4227074]

   tmp = ["4012c6:   add RSP,8",
          "402003: .byte 0x0",
          ".cfi_def_cfa_offset 56",
          "4012ca:   pop RBX",
          "402004: .byte 0x1",
          ".cfi_def_cfa_offset 48",
          "402005: .byte 0x0"]
   asmToCodeData.ddisasm(iter(tmp), Data, Code)
   assert Code == [4199083, 4199084, 4199085, 4199110, 4199114]
   assert Data == [4227072, 4227073, 4227074, 4202499, 4202500, 4202501]



def test_retro_function():
   Code = []
   Data = []
   tmp = [".type time, @function",
          "time:",
          ".L1140:",
          ".LC1140:",
          "pushq %rbp",
          ".LC1141:",
          "movq %rsp, %rbp",
          ".LC1144:",
          "subq $0x10, %rsp",
          ".LC1148:",
          "movl %edi, -4(%rbp)",
          ".LC114b:",
          "movl -4(%rbp), %eax",
          ".LC114e:",
          "movl %eax, %ecx",
          ".LC1150:",
          "subl $1, %ecx",
          ".size time,.-time",
          ".text"]
   functions = asmToFunctionData.retroWrite(iter(tmp))
   assert functions == {'time': {
       'Name': 'time',
       'Address': [{'Start_address': '.LC1140:', 'End_address': '.LC1150:'}]}}

def test_retro_code():
   Code: List[int] = []
   Data: List[int] = []
   tmp = [".LC4024:",
          ".byte 0x0",
          ".LC4025:",
          ".byte 0x0",
          ".LC4026:",
          ".byte 0x0",]
   asmToCodeData.retroWrite(iter(tmp), Data, Code)
   assert Code == []
   assert Data == [16420, 16421, 16422]

   tmp = [".LC1144:",
          "subq $0x10, %rsp",
          ".LC1148:",
          "movl %edi, -4(%rbp)",
          ".LC114b:",
          "movl -4(%rbp), %eax"]
   asmToCodeData.retroWrite(iter(tmp), Data, Code)
   assert Code == [4420, 4424, 4427]
   assert Data == [16420, 16421, 16422]

   tmp = [".LC4030:",
	      ".byte 0x0",
          ".LC4031:",
	      ".byte 0x0",
          "time:",
          ".L1140:",
          ".LC1140:",
	      "pushq %rbp"]
   asmToCodeData.retroWrite(iter(tmp), Data, Code)
   assert Code == [4420, 4424, 4427, 4416]
   assert Data == [16420, 16421, 16422, 16432, 16433]

