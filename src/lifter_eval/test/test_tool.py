from pytest import fixture, mark

from lifter_eval.tool import registry

def test_registry():
    assert sorted([tool.name for tool in registry]) == ["dyninst", "zipr"]
