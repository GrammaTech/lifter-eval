from dataclasses import dataclass
from enum import Enum
from pathlib import Path
import shlex
import subprocess
import sys
from tempfile import TemporaryFile
from typing import IO, Callable, Iterable, List, Optional, Sequence, Tuple, cast
import shutil


def format_cmd(cmd: Sequence[str]):
    return " ".join(map(shlex.quote, cmd))


def print_cmd(cmd: Sequence[str], input: Optional[bytes] = None):
    msg = format_cmd(cmd)
    if input:
        msg += ' <<< "' + input.decode() + '"'
    print(msg, flush=True)


class Mutex:
    def __init__(self):
        self.__locked = False

    def try_lock(self):
        if self.__locked:
            return False
        self.__locked = True
        return True

    def release(self):
        assert self.__locked
        self.__locked = False


class TaskState(Enum):
    PENDING = 0
    EXECUTING = 1
    FAILED = 2
    SUCCEEDED = 3


@dataclass
class Process:
    cmd: Sequence[str]
    cwd: Optional[Path] = None
    input: Optional[bytes] = None
    handle_exit_code: Optional[Callable[[int], None]] = None
    description: Optional[str] = None


def hook_process_exit_code(p: Process, hook: Callable[[int], None]) -> Process:
    def handle_exit_code(exit_code: int) -> None:
        hook(exit_code)
        if also_call := p.handle_exit_code:
            also_call(exit_code)

    return Process(p.cmd, p.cwd, p.input, handle_exit_code, p.description)


class Task:
    def __init__(
        self,
        process: Process,
        mutex: Optional[Mutex] = None,
        dependencies: Optional[List["Task"]] = None,
    ):
        self.__process_exit_code_handler = process.handle_exit_code
        self.__process = hook_process_exit_code(process, self.__done)
        self.__mutex = mutex
        self.__dependencies = dependencies or []
        self.__state = TaskState.PENDING

    def state(self):
        return self.__state

    def try_execute(self) -> Optional[Process]:
        assert self.__state == TaskState.PENDING

        for d in self.__dependencies:
            if d.__state in [TaskState.PENDING, TaskState.EXECUTING]:
                return None
            if d.__state == TaskState.FAILED:
                self.__state = TaskState.FAILED
                return None
        if self.__mutex and not self.__mutex.try_lock():
            return None

        self.__state = TaskState.EXECUTING
        return self.__process

    def dry_run(self):
        print_cmd(self.__process.cmd, self.__process.input)
        if handle_exit_code := self.__process_exit_code_handler:
            handle_exit_code(0)

    def __done(self, exit_code: int):
        assert self.__state == TaskState.EXECUTING

        if self.__mutex:
            self.__mutex.release()

        if exit_code == 0:
            self.__state = TaskState.SUCCEEDED
        else:
            self.__state = TaskState.FAILED


class ProcessScheduler:
    def __init__(self, max=1):
        self.__max = max
        self.__processes: List[
            Tuple[subprocess.Popen, Optional[IO[bytes]], Process]
        ] = []
        self.__stdout_captured = False
        self.__pending_stdout: Optional[IO[bytes]] = None

    def add(self, process: Process):
        if len(self.__processes) == self.__max:
            self.wait_for_any_process()

        stdout = None
        if self.__stdout_captured:
            stdout = TemporaryFile()

        log_text = ""
        if description := process.description:
            log_text += description + "\n"
        log_text += f"starting: {format_cmd(process.cmd)}"
        _log_process(None, log_text, stdout)

        proc = subprocess.Popen(
            process.cmd,
            cwd=process.cwd,
            stdin=subprocess.PIPE,
            stdout=stdout,
            stderr=stdout,
        )
        _log_process(proc.pid, "started", stdout)
        stdin = cast(IO[bytes], proc.stdin)
        if process.input:
            stdin.write(process.input)
        stdin.close()
        if stdout is None:
            self.__stdout_captured = True
        self.__processes.append((proc, stdout, process))

    def wait_for_any_process(self) -> bool:
        """
        Return `True` if any of the processes has finished.
        Return `False` if no processes are left for waiting.
        """
        if not self.__processes:
            return False

        while True:
            i = 0
            while i < len(self.__processes):
                p, redirected_stdout, process = self.__processes[i]
                exit_code = p.poll()
                if exit_code is None and i == len(self.__processes) - 1:
                    # wait a little bit avoiding CPU consumption
                    try:
                        exit_code = p.wait(0.1)
                    except subprocess.TimeoutExpired:
                        pass

                if exit_code is None:
                    i += 1
                else:
                    del self.__processes[i]
                    _log_process(p.pid, f"exited with {exit_code}", redirected_stdout)
                    if redirected_stdout is None:
                        self.__stdout_captured = False
                        if self.__pending_stdout:
                            _append_and_close_file(self.__pending_stdout, sys.stdout.buffer)
                            self.__pending_stdout = None
                    elif self.__pending_stdout:
                        _append_and_close_file(redirected_stdout, self.__pending_stdout)
                    else:
                        self.__pending_stdout = redirected_stdout

                    if process.handle_exit_code:
                        process.handle_exit_code(exit_code)
                    return True


def wait_for_all(ps: ProcessScheduler):
    while ps.wait_for_any_process():
        pass


def _log_process(pid: Optional[int], text: str, stdout):
    if pid is not None:
        text = f"PID #{pid}: " + text
    text += "\n"
    if stdout is None:
        stdout = sys.stdout.buffer
    stdout.write(text.encode())
    stdout.flush()


def _append_and_close_file(from_file: IO[bytes], to_file: IO[bytes]):
    try:
        from_file.seek(0)
        shutil.copyfileobj(from_file, to_file)  # type: ignore
    finally:
        from_file.close()


class TaskGroup:
    def __init__(self, tasks: Iterable[Task]):
        self.__tasks = list(tasks)

    def tasks(self):
        return self.__tasks

    def empty(self):
        return len(self.__tasks) == 0

    def pop_next(self) -> Optional[Process]:
        i = 0
        while i < len(self.__tasks):
            t = self.__tasks[i]
            process = t.try_execute()
            if t.state() != TaskState.PENDING:
                del self.__tasks[i]
                if process:
                    return process
            else:
                i += 1
        return None


def serial_tasks_from_commands(
    cmds: Iterable[Sequence[str]],
    handle_exit_code: Optional[Callable[[int], None]] = None,
    description: Optional[str] = None,
):
    result = []
    dependency: Optional[Task] = None
    for cmd in cmds:
        dependencies: List[Task] = [dependency] if dependency else []
        description = description if dependency is None else None
        task = Task(
            Process(cmd, handle_exit_code=handle_exit_code, description=description),
            dependencies=dependencies,
        )
        result.append(task)
        dependency = task
    return result


def run_tasks(tasks: Iterable[Task], scheduler: ProcessScheduler = ProcessScheduler()):
    g = TaskGroup(tasks)
    while True:
        process = g.pop_next()
        if process:
            scheduler.add(process)
        elif not g.empty():
            scheduler.wait_for_any_process()
        else:
            break
    wait_for_all(scheduler)
