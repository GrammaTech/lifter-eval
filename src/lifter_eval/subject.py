from functools import partial
from pathlib import Path
from shlex import quote
from typing import Callable, List, Optional, Tuple

from . import tool
from .docker import DOCKER_ROOT, HOST_ROOT, PrebuiltDockerImage, Runner
from .parallelize import Process
from .plugin import REPO_DIR, load
from .subject_properties import (
    TIMEOUT_EXPECTED_CODE,
    registry,
    SUBJECTS_DIR,
    SubjectProperties,
)


SMOKE_TEST_EXT = ".errno"
FUNCTIONAL_TEST_EXT = ".func"

_all_subjects_registered = False


def all_subjects() -> List[str]:
    global _all_subjects_registered
    if not _all_subjects_registered:
        for f in (REPO_DIR / SUBJECTS_DIR).iterdir():
            if f.suffix == ".py":
                load(f)
        _all_subjects_registered = True

    return sorted(props.name for props in registry)


def build_script(props: SubjectProperties, binary_rel_path: Path) -> Tuple[str, Path]:
    return (
        f"""\
set -ex
{props.init_filesystem}
{props.cleanup}
{props.patch}
cd {props.repo_dir.as_posix()}
{_call_build_script(props)}
{props.check_built_binary}
cp -p {props.binary_path.as_posix()} {_target_posix_path(props, binary_rel_path)}
""",
        binary_rel_path,
    )


def smoke_script(props: SubjectProperties, binary_rel_path: Path) -> Tuple[str, Path]:
    return _run_test_script(
        props,
        binary_rel_path,
        SMOKE_TEST_EXT,
        props.smoke_test,
        props.repo_dir,
    )


class ActionUnavailableError(Exception):
    pass


def evaluate_script(
    props: SubjectProperties, binary_rel_path: Path
) -> Tuple[str, Path]:
    evaluate = props.evaluate
    if not evaluate:
        raise ActionUnavailableError(f"no functional tests exist for '{props.name}'")

    cmd = "bash -e -c " + quote(evaluate)
    target_path = _target_posix_path(props, binary_rel_path)
    evaluate = _wrap_with_timeout(target_path + FUNCTIONAL_TEST_EXT, cmd)

    post_sanity_check = f"""\
if ! diff -q {target_path} {props.binary_path.as_posix()}; then
    echo "the target has been modified (rebuilt?) while running the test suite" >&2 && false
fi
"""
    cwd = props.env.sandbox_path(props.evaluate_subdir)
    return _run_test_script(
        props, binary_rel_path, FUNCTIONAL_TEST_EXT, evaluate, cwd, post_sanity_check
    )


def afl_fuzz_script(
    props: SubjectProperties, binary_rel_path: Path
) -> Tuple[str, Path]:
    afl_fuzz = props.afl_fuzz
    if not afl_fuzz:
        raise ActionUnavailableError(f"no AFL for '{props.name}'")

    if tool := _guess_tool_from_path(binary_rel_path):
        if vars := tool.afl_env:
            afl_fuzz = _bash_vars(**vars) + " " + afl_fuzz

    return _run_test_script(
        props, binary_rel_path, SMOKE_TEST_EXT, afl_fuzz, props.repo_dir
    )


def _guess_tool_from_path(path: Path) -> Optional[tool.Tool]:
    stem = path.stem
    i = stem.rfind(".")
    if i == -1:
        print(
            "Cannot guess what tool to use."
            " The tool name is extracted from the binary name like '*.tool.ext'"
        )
        return None
    tool_name = stem[i + 1 :]
    try:
        return tool.registry.tool_by_name(tool_name)
    except KeyError:
        print(f"unknown tool: {tool_name}")
        return None


def _target_posix_path(props: SubjectProperties, binary_rel_path: Path) -> str:
    return props.env.results_path(binary_rel_path).as_posix()


def _run_test_script(
    props: SubjectProperties,
    binary_rel_path: Path,
    result_ext: str,
    script: str,
    cwd: Path,
    post_sanity_check="",
) -> Tuple[str, Path]:
    target_path = _target_posix_path(props, binary_rel_path)
    return f"""\
set -ex
cp {target_path} {props.binary_path.as_posix()}
cd {cwd.as_posix()}
set +e
{script}
test_result=$?
set -e
{post_sanity_check}
echo $test_result > {target_path}{result_ext}
exit $test_result
""", binary_rel_path.with_suffix(
        binary_rel_path.suffix + result_ext
    )


def _bash_vars(**kw):
    return " ".join(f"{k}={quote(w)}" for k, w in kw.items())


def _call_build_script(props: SubjectProperties):
    if vars := props.build_variables:
        bash_vars = _bash_vars(**vars)
        return "function build(){\n" + props.build + "\n}\n" + bash_vars + " build"
    return props.build


def _wrap_with_timing(target_path: str, cmd: str) -> str:
    return f"/usr/bin/time -v -o {target_path}.time -- {cmd}"


def _wrap_with_timeout(target_path: str, cmd: str, timeout="1h") -> str:
    timeout_cmd = f"timeout {timeout} {cmd}"
    timeout_file = f"{target_path}.timeout"
    return f"""\
rm -f {timeout_file}; \
{_wrap_with_timing(target_path, timeout_cmd)} \
|| (/usr/bin/test $? == {TIMEOUT_EXPECTED_CODE} \
&& echo "timed out: {timeout}" \
&& echo {timeout} > {timeout_file} \
&& exit {TIMEOUT_EXPECTED_CODE})\
"""


def _make_process(
    script_factory: Callable[[SubjectProperties, Path], Tuple[str, Path]],
    props: SubjectProperties,
    binary_rel_path: Path,
) -> Process:
    shell = ["bash"]
    if docker_opts := props.env.docker_options():
        volumes = {str(HOST_ROOT): DOCKER_ROOT}
        volumes.update(props.docker_volumes)
        image = (
            docker_opts.image and PrebuiltDockerImage(docker_opts.image)
        ) or props.building_image
        docker_runner = Runner(image, volumes)
        shell = docker_runner.make_cmd(shell, hostname=props.docker_hostname)
    script, result_path = script_factory(props, binary_rel_path)
    return Process(
        shell,
        input=script.encode(),
        description=f"making {result_path}",
    )


make_build_process = partial(_make_process, build_script)


make_smoke_process = partial(_make_process, smoke_script)


make_eval_process = partial(_make_process, evaluate_script)


make_afl_fuzz_process = partial(_make_process, afl_fuzz_script)
