from typing import Dict, Type
from lifter_eval.plugin import REPO_DIR, load


class Tool:
    name = ""  # to be overridden
    afl_env: Dict[str, str] = {}


class Registry:
    def __init__(self):
        self.__tool_by_name: Dict[str, Tool] = {}

    def __iter__(self):
        for v in self.__tool_by_name.values():
            yield v

    def register(self, tool: Tool):
        self.__tool_by_name[tool.name] = tool

    def tool_by_name(self, name: str):
        return self.__tool_by_name[name]


registry = Registry()


def register(tool: Type[Tool]):
    """Decorator to register `Tool`"""
    registry.register(tool())

def _init_registry():
    for tool_dir in (REPO_DIR / "tools").iterdir():
        if tool_dir.is_dir():
            tool_path = tool_dir / (tool_dir.name + ".py")
            if tool_path.exists():
                load(tool_path)

_init_registry()