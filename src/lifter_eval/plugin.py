from importlib.util import module_from_spec, spec_from_file_location
from pathlib import Path


REPO_DIR = Path(__file__).absolute().parent.parent.parent

def load(f: Path):
    name = ".".join(f.relative_to(REPO_DIR).with_suffix("").parts)
    spec = spec_from_file_location(name, f)
    if spec is None or spec.loader is None:
        raise Exception(f"cannot get python module spec from '{f}'")
    module = module_from_spec(spec)
    spec.loader.exec_module(module)
