from typing import Type
from .cli import parse_subject_args
from .subject_properties import SubjectProperties, properties_hook


@properties_hook
def _subject_properties_as_cli(prop: Type[SubjectProperties]):
    if prop.__module__ == "__main__":
        parse_subject_args(prop)
