from collections import defaultdict
from dataclasses import dataclass
from functools import partial
import itertools
from os import environ
from pathlib import Path
from typing import Callable, Dict, Iterable, List, Optional, Sequence, Tuple

from .docker import (
    DOCKER_RESULTS,
    DOCKER_ROOT,
    HOST_RESULTS,
    HOST_ROOT,
    RESULTS_SUBDIR,
    DockerImage,
    DockerOptions,
    PrebuiltDockerImage,
    Runner,
)
from .parallelize import (
    Mutex,
    Process,
    ProcessScheduler,
    Task,
    hook_process_exit_code,
    run_tasks,
)
from .subject import (
    FUNCTIONAL_TEST_EXT,
    SMOKE_TEST_EXT,
    ActionUnavailableError,
    make_afl_fuzz_process,
    make_build_process,
    make_eval_process,
    make_smoke_process,
)
from .subject_properties import registry, make_properties
from .target import BINARY_EXT, Target, binary_base
from .traits import ISA, OS, Compiler, LLVMObfuscation, Optimization, Pie, Spec, Strip, Traits


MakeTargetT = Tuple[Target, str]


def collect_targets(
    subjects: Sequence[str],
    spec: Optional[Spec] = None,
    docker_opts: Optional[DockerOptions] = None,
    one_per_sandbox=False,
) -> Tuple[List[Target], List[Target]]:
    targets: List[Target] = []
    skipped: List[Target] = []
    spec = spec or Spec()
    for args in itertools.product(
        subjects,
        spec.isas,
        spec.os_list,
        spec.compilers,
        spec.pie_options,
        itertools.chain(spec.optimizations, spec.obfuscations),
        spec.strip_options,
    ):
        traits = Traits(*args[1:-1])
        prop_cls = registry.properties_by_name(args[0])
        props = make_properties(traits, prop_cls, docker_opts)
        t = Target(props, args[-1])
        if (
            t.isa == ISA.x64 or (t.os == OS.ubuntu20 and t.compiler != Compiler.icx)
        ) and (
            t.flags not in LLVMObfuscation
            or (
                t.compiler == Compiler.clang
                and t.os == OS.ubuntu20
                and t.isa == ISA.x64
            )
        ):
            if _should_be_skipped(t):
                res = skipped
            else:
                res = targets
            res.append(t)

    if one_per_sandbox:
        targets = _single_subject_target_per_sandbox(targets, skipped)
    return targets, skipped


_SO_SUBJECTS = [
    "libreoffice",
    "libzmq",
    "poppler",
]


# TODO: This function should be factored out to `Properties.supported_traits`
def _should_be_skipped(t: Target):
    if t.os == OS.ubuntu16:
        if t.subject in [
            "asterisk",  # requires libjansson >= 2.11.
            "bitcoin",  # requires C++17 support
            "filezilla",  # requires C++17 support
            "gnome-calculator",  # new dependencies required (gee-0.8 found: NO found 0.18.0 but need: '>= 0.20.0')
            "libreoffice",  # gcc/clang too old
            "poppler",  # gcc/clang compile errors
            "samba",  # python version >= 3.6.0 required
            "sipwitch",  # gcc/clang compile errors
            "unrealircd",  # libargon2 required
            "vlc",  # gettext-0.19.8 or newer is required
        ]:
            return True

        if t.compiler == Compiler.icx:
            return True

        if t.compiler == Compiler.clang:
            if t.subject in ["mysql"]:
                # compilation error
                return True
            if t.subject == "monero" and t.flags != Optimization._0:
                # linking error
                return True

    elif t.isa == ISA.arm:
        if t.subject in [
            "bind",
            "libreoffice",  # building fails: "workdir/UnpackedTarball/jfreereport_libloader/common_build.xml:1072: Compile failed"
        ]:  # "./gen -s . -t" fails
            return True
        if t.compiler == Compiler.clang:
            if t.subject == "monero":
                # tests/unit_tests/node_server.cpp:408:55: error: non-constant-expression cannot be narrowed from type 'unsigned long long' to 'size_t' (aka 'unsigned int') in initializer list [-Wc++11-narrowing]
                return True
    elif t.isa == ISA.arm64:
        if t.subject in [
            "snort3",  # "libdnet": cannot guess build type
            "filezilla",  # builds but cannot run: "Unable to initialize GTK+"
            "libreoffice",  # builds but cannot run: "terminate called after throwing an instance of 'com::sun::star::uno::DeploymentException'"
        ]:
            return True
        if t.subject == "libzmq" and (
            t.compiler != Compiler.gcc or t.flags != Optimization._1
        ):
            return True  # testsuite fails

    elif t.isa == ISA.x86:
        if t.subject in [
            "gnome-calculator",  # Lib-gee rquires a number of dependecies
            "monero",  # cmake issues
            "poppler",  # cmake issues
            "libreoffice",  # jawt library
            "sipwitch",
        ]:
            return True
        if t.compiler == Compiler.clang:
            if t.subject in [
                "redis",  # link errors: deps/hdr_histogram/hdr_histogram.c:73: undefined reference to `__atomic_fetch_add_8' and others
            ]:
                return True

        if t.subject == "vlc":
            # video_filter/deinterlace/yadif_template.h:134:9: error: 'asm' operand has impossible constraints
            if t.compiler == Compiler.clang and t.flags == Optimization._0:
                return True
            if t.compiler == Compiler.gcc and t.flags in [
                Optimization._0,
                Optimization.s,
            ]:
                return True
    if t.pie != Pie.pie:
        if t.subject in _SO_SUBJECTS + [
            "gnome-calculator",
            "lighttpd",
            "memcached",
            "redis",
        ]:
            return True

    if t.compiler == Compiler.gcc:
        if t.flags == Optimization._1 and t.subject == "libreoffice":
            # linking error
            return True
        if t.flags == Optimization._2 and t.subject == "hello":
            # the same as O3
            return True
        if t.flags == Optimization.fast and t.subject == "qmail":
            # the same as O3
            return True

    if t.compiler == Compiler.clang:
        if t.isa == ISA.x86:
            if t.subject in ["asterisk"]:
                # "libBlocksRuntime" is not available (no "libblocksruntime-dev:i386" package)
                return True
        if t.subject in [
            "bind",  # clang compiler error
            "gnome-calculator",  # gcc only supported
        ]:
            return True
        if t.subject == "hello":
            if t.flags in [Optimization._1, Optimization._2] or (
                t.flags == Optimization.s and t.pie == Pie.pie and t.os == OS.ubuntu16
            ):
                # the same as O3
                return True
            if t.flags == LLVMObfuscation.sub:
                # the same as LLVMObfuscation.fla
                return True
        if t.subject == "qmail" and t.flags == Optimization.fast:
            # the same as O3
            return True

        if t.subject == "vlc" and t.flags == Optimization._3:
            # the same as O2
            return True

        if t.flags in LLVMObfuscation:
            if t.subject in [
                "asterisk",  # "configure" error in "third-party/pjproject/source"
                "bitcoin",  # requires C++17 support
                "filezilla",  # requires C++17 support
                "libreoffice",  # must be at least Clang 5.0.2
                "memcached",  # "libclang_rt.profile-x86_64.a" missed + "-Wno-unused-command-line-argument" needed
                "monero",  # the -fla option: may only occur zero or one times
                "poppler",  # clang compile errors
                "snort3",  # "cannot find libdnet.so.1"
            ]:
                return True

            if t.subject == "samba" and t.pie == Pie.nopie:
                # unknown argument: '-no-pie'
                return True

            if t.flags == LLVMObfuscation.bcf and t.subject in [
                "anope",  # hangs on compiling "anope/include/version.cpp"
                "libzmq",  # hangs on compiling "src/address.cpp"
                "mosh",  # "configure" hangs on "checking whether std::shared_ptr is available..."
                "mysql",  # "configure" hangs on "Performing Test protobuf_HAVE_BUILTIN_ATOMICS"
                "sipwitch",  # hangs on compiling "service.cpp"
                "snort3",  # hangs on compiling "fst/daq_fst.cc"
                "squid",  # "configure" hangs on "checking whether linking without -latomic works..."
                "vlc",  # hangs
            ]:
                return True

    if t.compiler == Compiler.icx:
        if t.subject in [
            "mysql",  # error __FAST_MATH__ defined, refusing to compile
            "libreoffice",  # compilation error
            "gnome-calculator",  # gcc only supported
        ]:
            return True

        if t.subject == "hello" and t.flags in [Optimization._1, Optimization._2]:
            # the same as O3
            return True

        if t.subject == "vlc" and t.flags == Optimization._3:
            # the same as O2
            return True

        if t.flags not in [Optimization._0, Optimization._1]:
            if t.subject in ["bitcoin"]:
                # link errors
                return True

        if t.flags == Optimization.fast and t.subject in [
            "dnsmasq",
            "nginx",
            "qmail",
            "openssh",
            "pks",
            "sendmail",
            "zip",
        ]:
            # the same as O3
            return True

    is_haskell_subject = t.subject in [
        "ImplicitCAD",
        "kmonad",
        "pandoc",
        "shellcheck",
        "xmonad",
    ]
    if is_haskell_subject != (t.compiler == Compiler.ghc):
        return True
    if is_haskell_subject:
        if t.pie == Pie.pie:
            return True
        if t.flags in [Optimization._3, Optimization.s, Optimization.fast]:
            return True
        if t.os == OS.ubuntu16:
            return True
        if t.isa in [ISA.x86, ISA.arm64, ISA.arm]:
            return True

    is_ocaml_subject = t.subject in [
        "dune",
        "frama-c",
        "magic-trace",
        "mirage",
        "unison",
    ]
    if is_ocaml_subject != (t.compiler == Compiler.ocaml):
        return True
    if is_ocaml_subject:
        if t.pie == Pie.nopie:
            return True
        if t.flags not in [Optimization._1]:
            return True
        if t.os == OS.ubuntu16:
            return True
        if t.isa in [ISA.x86, ISA.arm64, ISA.arm]:
            return True

    is_go_subject = t.subject in [
        "crowdsec",
        "compose",
        "cli",
        "nsq",
        "osv-scanner",
    ]
    if is_go_subject != (t.compiler == Compiler.go):
        return True
    if is_go_subject:
        if t.flags not in [Optimization._0]:
            return True
        if t.os == OS.ubuntu16:
            return True
        if t.isa in [ISA.x86, ISA.arm64, ISA.arm]:
            return True

    is_gfortran_subject = t.subject in [
        "alscal",
        "dijkstra",
        "dijkstra_openmp",
        "f77_to_f90",
        "satisfy",
        "satisfy_openmp",
        "wordsnake",
    ]
    if is_gfortran_subject != (t.compiler == Compiler.gfortran):
        return True
    if is_gfortran_subject:
        if t.isa in [ISA.x86, ISA.arm64, ISA.arm]:
            return True
        if (
            t.subject == "satisfy"
            and t.flags == Optimization._3
            and t.os == OS.ubuntu16
        ):
            return True  # the same as O2

    if t.flags == Optimization.fast and t.subject in [
        "gnome-calculator",  # not supported
        "mysql",  # extra/duktape/duktape-2.6.0/src/duk_config.h:2893:2: error: #error __FAST_MATH__ defined, refusing to compile
    ]:
        return True

    return False


def _is_binary_target(ext: str):
    return ext == BINARY_EXT


def _parse_smoke_target(ext: str) -> Optional[str]:
    suffix = SMOKE_TEST_EXT
    return ext[: -len(suffix)] if ext.endswith(suffix) else None


def _parse_eval_target(ext: str):
    suffix = FUNCTIONAL_TEST_EXT
    return ext[: -len(suffix)] if ext.endswith(suffix) else None


def group_targets_by_runner(
    targets: Sequence[Target],
    target_ext: Sequence[str],
    batch: bool = False,
    image: Optional[DockerImage] = None,
) -> Iterable[Tuple[Runner, List[MakeTargetT]]]:
    """Group targets by docker image and (maybe) sandbox volume so they
    could be made with one `make` call
    """

    def volumes(t: Target) -> Dict[str, Path]:
        volumes = {str(HOST_ROOT): DOCKER_ROOT}
        if t.properties.env.docker_options():
            volumes[str(HOST_RESULTS)] = DOCKER_RESULTS
            volumes.update(**t.properties.docker_volumes)
        return volumes

    if not batch:
        return (
            (Runner(image or _image(t, e), volumes(t)), [(t, e)])
            for e in target_ext
            for t in targets
        )

    if image:
        targets_by_image = [(image, list(itertools.product(targets, target_ext)))]
    else:
        targets_by_image = list(_group_targets_by_image(targets, target_ext))

    result: List[Tuple[Runner, List[MakeTargetT]]] = []
    for image, make_targets in targets_by_image:
        target_volumes: Dict[Tuple, List[MakeTargetT]] = defaultdict(list)
        for t, ext in make_targets:
            group_by = _exclusive_tag(t) if t.properties.env.docker_options() else None
            target_volumes[group_by].append((t, ext))
        result.extend(
            (Runner(image, volumes(targets[0][0])), targets)
            for targets in target_volumes.values()
        )
    return result


@dataclass
class MakeOptions:
    always_make: bool = False
    batch: bool = False
    ignore_batch_errors: bool = False
    build_images: bool = False
    one_per_sandbox: bool = False


class ScheduledTasks:
    def __init__(
        self,
        opts: MakeOptions,
        handle_result: Callable[[Iterable[MakeTargetT], int], None],
    ):
        self.__opts = opts
        self.__handle_result = handle_result
        self.__tasks: List[Task] = []
        self.__image_build_tasks: Dict[str, Task] = {}

    def schedule_makefile_task(
        self,
        make_targets: Iterable[MakeTargetT],
        runner: Runner,
        mutex: Optional[Mutex] = None,
        dependencies: Optional[List[Task]] = None,
    ):
        cmd = _make_cmd(
            make_targets,
            runner,
            self.__opts.ignore_batch_errors,
            self.__opts.always_make,
        )
        handle_exit_code = partial(self.__handle_result, make_targets)
        task_dependencies = self.__dependencies(runner.image())
        if dependencies:
            task_dependencies.extend(dependencies)

        description = "making " + " ".join(
            f"{binary_base(t)}{ext}" for t, ext in make_targets
        )
        result = Task(
            Process(cmd, handle_exit_code=handle_exit_code, description=description),
            mutex=mutex,
            dependencies=task_dependencies,
        )
        self.__tasks.append(result)
        return result

    def schedule_python_task(
        self,
        t: Target,
        ext: str,
        mutex: Mutex,
    ) -> Task:
        src_ext: Optional[str] = ext
        if _is_binary_target(ext):
            assert t.strip == Strip.nostrip
            make_process = make_build_process
        elif src_ext := _parse_smoke_target(ext):
            if src_ext.endswith(".afl"):
                make_process = make_afl_fuzz_process
            else:
                make_process = make_smoke_process
        elif src_ext := _parse_eval_target(ext):
            make_process = make_eval_process

        assert src_ext is not None

        try:
            process = make_process(t.properties, Path(binary_base(t) + src_ext))
        except ActionUnavailableError as x:
            process = Process(["false"], description=str(x))

        handle_exit_code = partial(self.__handle_result, [(t, ext)])
        result = Task(
            hook_process_exit_code(process, handle_exit_code),
            mutex=mutex,
            dependencies=self.__dependencies(t.properties.building_image),
        )
        self.__tasks.append(result)
        return result

    def all_scheduled(self):
        return self.__tasks

    def __dependencies(self, image: DockerImage):
        if not self.__opts.build_images:
            return []
        return [self.__build_image(image)]

    def __build_image(self, image: DockerImage) -> Task:
        image_name = image.name
        result = self.__image_build_tasks.get(image_name)
        if result is None:
            if base_image := image.base_image:
                dependencies = [self.__build_image(base_image)]
            else:
                dependencies = []
            result = Task(image.build(), dependencies=dependencies)
            self.__tasks.append(result)
            self.__image_build_tasks[image_name] = result
        return result


class Results:
    def __init__(self, skipped: Sequence[Target] = []):
        self.__ok: List[MakeTargetT] = []
        self.__failed: List[MakeTargetT] = []
        self.__skipped = skipped

    def handle(
        self,
        make_targets: Iterable[MakeTargetT],
        exit_code: int,
    ):
        (self.__failed if exit_code else self.__ok).extend(make_targets)

    def dump_summary(self):
        if self.__ok:
            print("successful targets:\n" + _format_grouped_targets(self.__ok))
        if self.__skipped:
            print("skipped subjects:\n" + "\n".join(map(binary_base, self.__skipped)))
        if self.__failed:
            print("failed targets:\n" + _format_grouped_targets(self.__failed))
        else:
            print("All subjects are OK!")


def _single_subject_target_per_sandbox(
    targets: Sequence[Target], skipped: List[Target]
) -> List[Target]:
    result = []
    unique = set()
    for t in targets:
        skip = False
        for key in (
            t.properties.docker_volumes
            if t.properties.env.docker_options()
            else [t.subject]
        ):
            if key in unique:
                skip = True
            else:
                unique.add(key)
        if skip:
            skipped.append(t)
        else:
            result.append(t)
    return result


def make(
    subjects: Sequence[str],
    spec: Spec,
    target_ext: Sequence[str],
    ps: Optional[ProcessScheduler],
    make_opts: MakeOptions,
    docker_opts: Optional[DockerOptions],
):
    targets, skipped = collect_targets(
        subjects, spec, docker_opts, make_opts.one_per_sandbox
    )
    results = Results(skipped)
    make_targets(targets, target_ext, ps, make_opts, docker_opts, results)
    results.dump_summary()


def _schedule_tasks(
    targets: Sequence[Target],
    target_ext: Sequence[str],
    make_opts: MakeOptions,
    docker_opts: Optional[DockerOptions],
    results: Results,
) -> ScheduledTasks:
    run_targets = group_targets_by_runner(
        targets,
        target_ext,
        make_opts.batch,
        docker_opts
        and docker_opts.image
        and PrebuiltDockerImage(docker_opts.image)
        or None,
    )
    tasks = ScheduledTasks(make_opts, results.handle)

    # Group targets by a subject - the same subject targets (.elf)
    # cannot be built in parallel with different options in the same sandbox
    exclusive_groups: Dict[str, Mutex] = defaultdict(Mutex)

    # split .elf/.func/.errno targets and group them by subject/sandbox
    exclusive_targets: Dict[str, List[MakeTargetT]] = defaultdict(list)

    for runner, grouped_targets in run_targets:
        other_targets: List[MakeTargetT] = []
        for target, ext in grouped_targets:
            if _is_target_exclusive_for_subject_sandbox(ext):
                group = exclusive_targets[_exclusive_tag(target)]
            else:
                group = other_targets
            group.append((target, ext))

        if other_targets:
            tasks.schedule_makefile_task(other_targets, runner)

    for tag, subject_targets in exclusive_targets.items():
        mutex = exclusive_groups[tag]
        _split_python_and_makefile_tasks(subject_targets, runner, mutex, tasks)

    return tasks


def _format_grouped_targets(grouped_targets: Sequence[MakeTargetT]):
    return "\n".join([binary_base(t) + ext for t, ext in grouped_targets])


def make_targets(
    targets: Sequence[Target],
    target_ext: Sequence[str],
    ps: Optional[ProcessScheduler],
    make_opts: MakeOptions,
    docker_opts: Optional[DockerOptions],
    results: Results,
):
    scheduled = _schedule_tasks(targets, target_ext, make_opts, docker_opts, results)
    tasks = scheduled.all_scheduled()
    if ps:
        run_tasks(tasks, ps)
    else:
        for t in tasks:
            t.dry_run()


def _split_python_and_makefile_tasks(
    subject_targets: List[MakeTargetT],
    runner: Runner,
    mutex: Mutex,
    tasks: ScheduledTasks,
):
    assert len(subject_targets)
    subject = next(iter(subject_targets))[0].subject
    assert not any(t.subject != subject for t, _ in subject_targets)

    # Binary stripping is implemented in Makefile so such tasks should go after the binaries are built.
    makefile_targets: List[MakeTargetT] = []
    python_tasks: List[Task] = []
    for t, ext in subject_targets:
        if t.strip == Strip.strip and _is_binary_target(ext):
            makefile_targets.append((t, ext))
        else:
            python_tasks.append(tasks.schedule_python_task(t, ext, mutex))
    if makefile_targets:
        tasks.schedule_makefile_task(
            makefile_targets, runner, dependencies=python_tasks
        )


def _make_cmd(
    make_targets: Iterable[MakeTargetT],
    runner: Runner,
    ignore_batch_errors: bool = False,
    always_make: bool = False,
):
    make_cmd = ["make"]
    if ignore_batch_errors:
        make_cmd += ["-k"]
    if always_make:
        make_cmd += ["--always-make"]
    make_cmd += [_result_target_path(t, ext) for t, ext in make_targets]

    env_vars = {k: v for k, v in environ.items() if k.startswith("LIFTER_EVAL_")}
    return runner.make_cmd(make_cmd, env=env_vars)


def _result_target_path(t: Target, ext: str):
    return f"{RESULTS_SUBDIR}/{binary_base(t)}{ext}"


def _exclusive_tag(t: Target):
    result = [t.subject]
    props = t.properties
    if props.env.docker_options():
        result.extend(props.docker_volumes.keys())
    return ".".join(result)


def _group_targets_by_image(
    targets, exts
) -> Iterable[Tuple[DockerImage, List[MakeTargetT]]]:
    group_by_image: Dict[str, Tuple[DockerImage, List[MakeTargetT]]] = {}
    for t in targets:
        for ext in exts:
            image = _image(t, ext)
            group = group_by_image.get(image.name)
            if group is None:
                group = (image, [])
                group_by_image[image.name] = group
            make_targets = group[1]
            make_targets.append((t, ext))

    return group_by_image.values()


def _is_target_exclusive_for_subject_sandbox(ext: str):
    return any(
        f(ext) for f in [_is_binary_target, _parse_smoke_target, _parse_eval_target]
    )


def _image(target: Target, ext: str) -> DockerImage:
    if ext in [".dyninst.rewritten", ".dyninst.afl"]:
        suffix = "dyninst"
    elif ext in [".e9patch.rewritten", ".e9patch.afl"]:
        suffix = "e9patch"
    elif ext in [".egalito.rewritten", ".egalito.afl"]:
        suffix = "egalito"
    elif ext in [".retrowrite.S"]:
        suffix = "retrowrite"
    elif ext in [".mctoll.rewritten", ".mctoll.afl"]:
        suffix = "mctoll"
    elif ext in [".reopt.ll", ".reopt.rewritten"]:
        suffix = "reopt"
    elif ext in [".revng.rewritten"]:
        suffix = "revng"
    elif ext == ".zipr.rewritten":
        suffix = "zipr"
    elif ext == ".zipr.afl":
        suffix = "zafl"
    else:
        return target.properties.building_image

    result = DockerImage.ROOT_IMAGE
    if suffix:
        result += "/" + suffix
    return PrebuiltDockerImage(result)
