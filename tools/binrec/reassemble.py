#!/usr/bin/env python3
from build import ContainerScope, container_binrec_dir, exec_with_bash, image

import argparse

def _reassebmle(input_bc, input_bin, output):
    wd = f"{container_binrec_dir}/temp"
    bash_cmds = [f"mkdir {wd}",
                 f"cd {wd}",
                 f"mv {container_binrec_dir}/input.bc"
                   f" {container_binrec_dir}/binary"
                   " .",

                 # See "binrec/scripts/lift2.sh"
                 f"{container_binrec_dir}/build/bin/binrec_lift --compile -o recovered input.bc",
                 f"make -f {container_binrec_dir}/scripts/s2eout_makefile -sr recovered.o",
                 f"{container_binrec_dir}/build/bin/binrec_link"
                    f" -b binary -r recovered.o"
                    f" -l {container_binrec_dir}/build/lib/libbinrec_rt.a"
                    f" -o recovered"
                    f" -t {container_binrec_dir}/binrec_link/ld/i386.ld"
                 ]

    with ContainerScope(image, exec_with_bash(bash_cmds)) as c:
        c.copy_from_host(input_bc, f"{container_binrec_dir}/input.bc")
        c.copy_from_host(input_bin, f"{container_binrec_dir}/binary")
        c.start()
        c.copy_to_host(f"{wd}/recovered", output)

def main():
    parser = argparse.ArgumentParser(description="Reassebmble a binary from LLVM bitcode (.bc)")
    parser.add_argument("input", help="input LLVM bitcode")
    parser.add_argument("output", help="output binary (ELF x86-32)")
    parser.add_argument(
        "--original-binary", dest="original_binary", required=True,
        help="original binary from which LLVM bitcode was produced")
    options = parser.parse_args()
    _reassebmle(options.input, options.original_binary, options.output)

if __name__ == "__main__":
    main()
