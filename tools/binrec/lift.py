#!/usr/bin/env python3
from build import ContainerScope, container_binrec_dir, exec_with_bash, image

import argparse
import os

def _lift(input, output):
    input_file_name = os.path.basename(input)
    bash_cmds = [f"qemu/cmd-debian.sh {input_file_name}",
                 f"scripts/double_merge.sh {input_file_name}",
                 f"cd s2e-out-{input_file_name}",
                 
                 # See "binrec/scripts/lift2.sh"
                 "make -f ../scripts/s2eout_makefile symbols",
                 "../build/bin/binrec_lift --clean -o cleaned captured.bc",
                 "llvm-link -o linked.bc cleaned.bc ../runlib/custom-helpers.bc",
                 "../build/bin/binrec_lift --lift -o lifted linked.bc --clean-names",
                 "../build/bin/binrec_lift --optimize -o optimized lifted.bc --memssa-check-limit=100000"
                 ]

    with ContainerScope(image, exec_with_bash(bash_cmds)) as c:
        c.copy_from_host(input, f"{container_binrec_dir}/build/test")
        c.start()
        c.copy_to_host(
            f"{container_binrec_dir}/s2e-out-{input_file_name}/optimized.bc",
            output)

def main():
    parser = argparse.ArgumentParser(description="Lift a binary to LLVM")
    parser.add_argument("input", help="input binary (ELF x86-32)")
    parser.add_argument("output", help="output LLVM bitcode (.bc)")
    options = parser.parse_args()
    _lift(options.input, options.output)

if __name__ == "__main__":
    main()
