#!/usr/bin/env python3
import argparse
import itertools
import os
import shlex
import subprocess
import sys
import tempfile

image = "registry.gitlab.com/metis/lifter-eval/binrec"

def run_cmd(cmd, capture_stdout=False):
    print(" ".join(map(shlex.quote, cmd)))
    sys.stdout.flush()

    f = subprocess.check_output if capture_stdout else subprocess.check_call
    return f(cmd)

_base_image = "binrec_base"
_volume = "binrec"
container_binrec_dir = "/binrec"

def exec_with_bash(cmds):
    return ["bash", "-c", " && ".join(cmds)]

class ContainerScope:
    def __init__(self, image, cmd, volumes=None):
        args = ["docker", "create"]
        if volumes:
            args.extend(itertools.chain(*(
                ["-v", f"{k}:{v}"] for k, v in volumes.items())))
        #args += ["-e", "S2EDIR=/binrec"]
        args += [image] + cmd
        self.__args = args
        self.__container_id = None

    def __enter__(self, *_):
        self.__container_id = run_cmd(
            self.__args, capture_stdout=True).decode().strip()
        return self

    def __exit__(self, *_):
        run_cmd(["docker", "rm", "-f", self.__container_id])

    def copy_from_host(self, host_path, container_path):
        run_cmd(["docker", "cp", host_path,
                                 f"{self.__container_id}:{container_path}"])

    def copy_to_host(self, container_path, host_path):
        run_cmd(["docker", "cp", f"{self.__container_id}:{container_path}",
                                 host_path])

    def start(self):
        run_cmd(["docker", "start", "-a", self.__container_id])

    def commit(self, image):
        run_cmd(["docker", "commit", self.__container_id, image])


def _build_image(use_volume):
    this_dir = os.path.dirname(os.path.abspath(__file__))
    run_cmd(["docker", "build",
             "--build-arg", f"S2EDIR={container_binrec_dir}",
             "-t", _base_image, this_dir])

    with tempfile.TemporaryDirectory(prefix = "gt_binrec_") as tmp:
        run_cmd(["git", "clone",
                 "-b", "vfolts/eval",
                 "git@gitlab.com:METIS/binrec.git", tmp])

        container_repo_dir = "/binrec_mount"
        build_cmds = [
            f"git clone {container_repo_dir} {container_binrec_dir}",
            f"cd {container_binrec_dir}",
            f"mkdir build",
            f"cd build",
            "cmake ..",
            "make",
            "curl https://www.ics.uci.edu/~fparzefa/binrec/debian10.raw.xz"
                f" | xz -d > {container_binrec_dir}/qemu/debian.raw"
            ]
        volumes = {f"{tmp}": f"{container_repo_dir}"}
        if use_volume:
            volumes[f"{_volume}"] = f"{container_binrec_dir}"
        with ContainerScope(_base_image, exec_with_bash(build_cmds), volumes) as c:
            c.start()
            if not use_volume:
                c.commit(_base_image)

def main():
    parser = argparse.ArgumentParser(
        description=f"Build BinRec docker image '{_base_image}'")
    parser.add_argument(
        "--use-volume", dest="use_volume", action="store_true")
    options = parser.parse_args()

    _build_image(options.use_volume)

if __name__ == "__main__":
    main()
