# Build docker image

Image building requires manual steps.

## Build base docker image "binrec_base"

1. Build "binrec_base" image with BinRec built in "/binrec":
    ```shell
    ./build.py
    ```

## Add S2E mode snapshot to the resulting image "registry.gitlab.com/metis/lifter-eval/binrec"

1. Boot VM and run VNC server:
    ```shell
    docker run --name binrec -w /binrec -p 5900:5900 binrec_base qemu/debian.sh -vnc :0
    ```
1. Connect to the server using VNC client (port 5900). Log in (user "user" and password "password") and run the command `getrun-cmd`:
    ```shell
    getrun-cmd
    ```
1. Still being in VNC session switch to the QEMU shell by pressing `ctrl+alt+2` and save a snapshot with the name "cmd":
    ```shell
    savevm cmd
    quit
    ```

    The snapshot will be saved to "/binrec/qemu/debian.s2e.cmd" and VNC session should terminate. 
    
1. Commit the resulting docker image:
    ```shell
    docker commit binrec registry.gitlab.com/metis/lifter-eval/binrec
    docker rm binrec
    ```

## Push "registry.gitlab.com/metis/lifter-eval/binrec"
    
```shell
docker push registry.gitlab.com/metis/lifter-eval/binrec
```

# Lifting binaries

Check if the image is OK:
```shell
docker run --rm registry.gitlab.com/metis/lifter-eval/binrec bash -c "qemu/cmd-debian.sh hello && scripts/double_merge.sh hello && cd s2e-out-hello && ../scripts/lift2.sh && ./recovered"
```
This will lift and reassemble "hello" binary from the image itself. Look for "Hello world!" in the very end of output.

## Getting LLVM bitcode

```shell
./lift.py <input binary> <output .bc>
```    

## Reassemble from LLVM bitcode

```shell
./reassemble.py --original-binary <input binary> <input .bc> <output binary>
```    

# Limitations

- dynamic lifting expects that a binary has a good code coverage and will actually run in specific environment
- it is possible to run the binary of interest multiple times with different inputs (to cover different execution paths) but no option for running a whole test suite once
- x86-32 binaries only
- gcc9/debin10 only (the address of `main` has to be patched, see "Linking" section in "binrec/README.md")
- fails for non-pie binaries ("gcc -m32 -no-pie /binrec/test/hello.c")
- fails for static binaries
