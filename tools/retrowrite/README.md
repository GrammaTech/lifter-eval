# Limitations

- `pie` binaries only
- non-stripped binaries only
- `.so` are not supported (fails with no "main" symbol found)
- Windows binaries are not supported.
- Claims ARM64 support but fails to process even "hello world", the latest version in https://github.com/HexHive/retrowrite is broken (the latest merge is committed with unresolved conflicts).
- ARM32 fails: "struct.error: unpack requires a buffer of 8 bytes"

# Building

Use [Dockerfile](Dockerfile) to build "registry.gitlab.com/metis/lifter-eval/retrowrite".

## Ubuntu 20:

Run docker with "registry.gitlab.com/metis/lifter-eval" image:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Disassemble the binaries (produce "*.retrowrite.S"):
```shell: 
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt compilers/list-targets-icx.txt | grep \\.pie | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.S|')
```

Rewrite gcc/clang binaries and run the smoke tests:
```shell: 
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | grep \\.pie | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.rewritten.errno|')
```

Produce gcc/clang binaries with AFL instrumentation and smoke-test them:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | grep \\.pie | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.afl.errno|')
```

Run docker with icx image ("registry.gitlab.com/metis/lifter-eval/retrowrite"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/icx bash
```

Rewrite icx binaries and run the smoke tests:
```shell: 
make -k $(cat compilers/list-targets-icx.txt | grep \\.pie | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.rewritten.errno|')
```

Produce icx binaries with AFL instrumentation and smoke-test them:
```shell
make -k $(cat compilers/list-targets-icx.txt | grep \\.pie | sed -E 's|(.+).elf|results/\1.nostrip.retrowrite.afl.errno|')
```

## Ubuntu 16:

Run docker with host's "results-16" mapped to container's "results". Use "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval bash
```

Produce assembler files ("*.retrowrite.S"):
```shell
make -k $(grep \\.pie compilers/list-targets-16.txt | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.S|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Rewrite the binaries and run the smoke tests:
```shell: 
make -k $(grep \\.pie compilers/list-targets-16.txt | grep \\.pie | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.rewritten.errno|')
```

Produce binaries with AFL instrumentation and smoke-test them:
```shell
make -k $(grep \\.pie compilers/list-targets-16.txt | grep \\.nostrip | sed -E 's|(.+).elf|results/\1.retrowrite.afl.errno|')
```

# Results

Ubuntu 20 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .S                | 24.20% | 1751     |
| .rewritten        | 8.87%  | 2105     |
| .rewritten.errno  | 8.05%  | 2124     |
| .afl              | 8.70%  | 2109     |
| .afl.errno        | 6.84%  | 2152     |

Ubuntu 16 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .S                | 24.85% | 777      |
| .rewritten        | 12.48% | 905      |
| .rewritten.errno  | 11.90% | 911      |
| .afl              | 12.48% | 905      |
| .afl.errno        | 9.28%  | 938      |
