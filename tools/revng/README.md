# Ubuntu 20 x86_64 binaries

```shell
bin/alternate_make.py --isa x86_64 --os ubuntu20 -- .revng.rewritten .revng.rewritten.errno
```

# Results

| transform                             | rate    | failures |
| ---                                   | ---     | ---      |
| x86_64.ubuntu20.revng.rewritten       | 19.48%  | 1918     |
| x86_64.ubuntu20.revng.rewritten.errno | 16.62%  | 1986     |
| x86_64.ubuntu16.revng.rewritten       | 40.72%  | 613      |
| x86_64.ubuntu16.revng.rewritten.errno | 37.72%  | 644      |