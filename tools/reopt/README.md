# Limitations

- ARM64 is not supported: "Recovery does not support EM_AARCH64 binaries."
- ARM32 is not supported: "32-bit elf files are not yet supported."

# Environment

Build "registry.gitlab.com/grammatech/lifter-eval/reopt" docker image:
```shell
docker build -t registry.gitlab.com/grammatech/lifter-eval/reopt -f tools/reopt/Dockerfile tools/reopt
```

# Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval/reopt
```

Produce LLVM assembler files and rewritten binaries (`.ll` and `.rewritten` files):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.reopt.ll|')
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.reopt.rewritten|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/grammatech/lifter-eval"):
```shell
docker run --rm -it --hostname test.com -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval bash
```

Run smoke tests for the rewritten gcc/clang binaries:
```
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.reopt.rewritten.errno|')
```

Exit docker container and run "icx" image ("registry.gitlab.com/grammatech/lifter-eval/icx"):
```shell
docker run --rm -it --hostname test.com -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval/icx bash
```

Run smoke tests for the rewritten icx binaries:
```
make -k $(cat compilers/list-targets-icx.txt | sed -E 's|(.+).elf|results/\1.reopt.rewritten.errno|')
```

## Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/grammatech/lifter-eval/reopt
```

Produce LLVM assembler files and rewritten binaries (`.ll` and `.rewritten` files):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.reopt.ll|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/grammatech/lifter-eval:16") with host's "results-16" mapped to container's "results":
```shell
docker run --rm -it --hostname test.com -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/grammatech/lifter-eval:16 bash
```

Run smoke tests for the rewritten binaries:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.reopt.rewritten.errno|')
```

# Results

Ubuntu 20 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .ll               | 91.86% | 188      |
| .rewritten        | 88.66% | 262      |
| .rewritten.errno  | 34.50% | 1513     |

Ubuntu 16 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .ll               | 95.65% | 45       |
| .rewritten        | 59.19% | 422      |
| .rewritten.errno  | 32.59% | 697      |
