# Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Dissassemble the binaries (produce "*.ddisasm.S" and "*.ddisasm.att.S"):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.ddisasm.S\nresults/\1.ddisasm.att.S|')
```

Rewrite the gcc/clang binaries and run the smoke tests:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.ddisasm.rewritten.errno|')
```

Run AFL transform and smoke test AFL instrumentation (make sure to produce "*.ddisasm.att.S" first):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.ddisasm.afl.errno|')
```

Exit docker container and run "icx" image ("registry.gitlab.com/metis/lifter-eval/icx"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/icx bash
```

Rewrite the icx binaries and run the smoke tests:
```shell
make -k $(cat compilers/list-targets-icx.txt | sed -E 's|(.+).elf|results/\1.ddisasm.rewritten.errno|')
```

Run AFL transform and smoke test AFL instrumentation (make sure to produce "*.ddisasm.att.S" first):
```shell
make -k $(cat compilers/list-targets-icx.txt | sed -E 's|(.+).elf|results/\1.ddisasm.afl.errno|')
```

# Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results". Use "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval bash
```

Dissassemble the binaries (produce "*.ddisasm.S" and "*.ddisasm.att.S"):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.ddisasm.S\nresults/\1.ddisasm.att.S|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Rewrite the binaries and run the smoke tests:

```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.ddisasm.rewritten.errno|')
```

Run AFL transform and smoke test AFL instrumentation (make sure to produce "*.ddisasm.att.S" first):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.ddisasm.afl.errno|')
```

# Results

x64 ubuntu20 binaries
| transform                               | rate    | failures |
| ---                                     | ---     | ---      |
| x86_64.ubuntu20.ddisasm.S               | 97.44%  | 61       |
| x86_64.ubuntu20.ddisasm.rewritten       | 89.67%  | 246      |
| x86_64.ubuntu20.ddisasm.rewritten.errno | 86.27%  | 327      |
| x86_64.ubuntu20.ddisasm.att.S           | 97.73%  | 54       |
| x86_64.ubuntu20.ddisasm.afl             | 90.64%  | 223      |
| x86_64.ubuntu20.ddisasm.afl.errno       | 80.02%  | 476      |

x64 ubuntu16 binaries
| transform                               | rate    | failures |
| ---                                     | ---     | ---      |
| x86_64.ubuntu16.ddisasm.S               | 97.00%  | 31       |
| x86_64.ubuntu16.ddisasm.rewritten       | 92.07%  | 82       |
| x86_64.ubuntu16.ddisasm.rewritten.errno | 90.62%  | 97       |
| x86_64.ubuntu16.ddisasm.att.S           | 97.20%  | 29       |
| x86_64.ubuntu16.ddisasm.afl             | 92.75%  | 75       |
| x86_64.ubuntu16.ddisasm.afl.errno       | 64.80%  | 364      |

aarch64 ubuntu20 binaries
| transform                                | rate    | failures |
| ---                                      | ---     | ---      |
| aarch64.ubuntu20.ddisasm.S               | 100.00% | 0        |
| aarch64.ubuntu20.ddisasm.rewritten       | 38.29%  | 817      |
| aarch64.ubuntu20.ddisasm.rewritten.errno | 36.48%  | 841      |
