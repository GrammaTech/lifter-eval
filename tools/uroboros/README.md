# Limitations

- `non-pie` binaries only ("Uroboros doesn't support shared library")
- AFL binaries can be produced but seem non-working ("No instrumentation detected")
- Windows binaries are not supported.
- ARM (both 32 and 64) is not supported: "objdump: can't disassemble for architecture UNKNOWN!"

# Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/uroboros bash
```

Produce `*.uroboros.S`:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.S|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Rewrite the binaries and run the smoke tests:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.rewritten.errno|')
```

Produce binaries with AFL instrumentation and smoke-test them:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.afl.errno|')
```

# Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval/uroboros bash
```

Produce `*.uroboros.S`:
```shell
make -k $(cat compilers/list-targets-16.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.S|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval bash
```

Rewrite the binaries:
```
make -k $(cat compilers/list-targets-16.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.rewritten|')
```

Produce binaries with AFL instrumentation (`*.uroboros.afl`):
```
make -k $(cat compilers/list-targets-16.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.afl|')


Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Run the smoke tests for rewritten binaries:
```
make -k $(cat compilers/list-targets-16.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.rewritten.errno|')
```

Smoke-test AFL binaries:
```
make -k $(cat compilers/list-targets-16.txt | grep \\.nopie | sed -E 's|(.+).elf|results/\1.uroboros.afl.errno|')
```

# Results

Ubuntu 20 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .S                | 8.05%  | 2124     |
| .rewritten        | 3.98%  | 2218     |
| .rewritten.errno  | 2.16%  | 2260     |
| .afl              | 3.98%  | 2218     |

Ubuntu 16 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .S                | 17.21% | 856      |
| .rewritten        | 11.99% | 910      |
| .rewritten.errno  | 4.45%  | 988      |
| .afl              | 11.41% | 916      |
