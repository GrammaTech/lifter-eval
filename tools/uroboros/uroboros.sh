#!/bin/bash
#
# Run Uroboros on $1 moving results to $2
#
set -e
SOURCE=$(readlink -f $1)
TARGET=$(readlink -f $2)

cd /uroboros/src
python2 ./uroboros.py $SOURCE
[ -f final.s ] || (echo "Failed to disassemble $SOURCE" && exit 1)
cp final.s $TARGET
