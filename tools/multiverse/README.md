# Limitations

- Ubuntu 20 binaries are not supported yet (https://github.com/utds3lab/multiverse/issues/10)
- All "nopie" binaries failed the smoke tests (crashed)
- Windows binaries are not supported.
- Currently supports 32-bit and 64-bit x86 binaries only. But we don't have 32-bit bainaries for Ubuntu 16.

# Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval/multiverse bash
```

Rewrite the binaries (produce `*.multiverse.rewritten`):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.multiverse.rewritten|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Run smoke tests for the rewritten binaries:
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.multiverse.rewritten.errno|')
```

# Results

Ubuntu 20 binaries
| transform          | rate   | failures | 
| ---                | ---    | ---      |
| .rewritten         | 0%     | 2310     |
| .rewritten.errno   | 0%     | 2310     |


Ubuntu 16 binaries
| transform          | rate   | failures | 
| ---                | ---    | ---      |
| .rewritten         | 85.11% | 154      |
| .rewritten.errno   | 35.01% | 672      |
