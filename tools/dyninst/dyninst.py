from lifter_eval.tool import Tool, register


@register
class DyninstTool(Tool):
    name = "dyninst"
    afl_env = {"AFL_SKIP_BIN_CHECK": "1"}
