# Limitations
- x86 (32- and 64-bits) only
- x86 "pie" binaries can be rewritten but most of them fail the smoke-test (only 6 binaries pass)
- x86 AFL transform using "afl-dyninst" does not work: "error while loading shared libraries: unexpected reloc type 0xe3"

# Source code
- "null_rewrite" is a tool created specifically for this evaluation, it is based on "dyninst" and just loads a binary and writes it back.
- "docker/Dockerfile" is a slightly modified version of "https://github.com/dyninst/dyninst.git", it builds "dyninst" itself + "null_rewrite"

# Build docker image

```shell
docker build -f tools/dyninst/docker/Dockerfile -t registry.gitlab.com/grammatech/lifter-eval/dyninst tools/dyninst
```
# Results
- no AFL transform out of the box

| transform                               | rate    | failures |
| ---                                     | ---     | ---      |
| x86_64.ubuntu20.dyninst.rewritten       | 99.75%  | 6        |
| x86_64.ubuntu20.dyninst.rewritten.errno | 75.02%  | 595      |
| x86_64.ubuntu20.dyninst.afl             | 47.44%  | 1252     |
| x86_64.ubuntu20.dyninst.afl.errno       | 31.36%  | 1635     |
| x86_64.ubuntu16.dyninst.rewritten       | 100.00% | 0        |
| x86_64.ubuntu16.dyninst.rewritten.errno | 88.88%  | 115      |
| x86_64.ubuntu16.dyninst.afl             | 97.29%  | 28       |
| x86_64.ubuntu16.dyninst.afl.errno       | 73.69%  | 272      |
| x86.ubuntu20.dyninst.rewritten          | 96.04%  | 50       |
| x86.ubuntu20.dyninst.rewritten.errno    | 44.30%  | 704      |
| x86.ubuntu20.dyninst.afl                | 0.00%   | 1264     |
| x86.ubuntu20.dyninst.afl.errno          | 0.00%   | 1264     |