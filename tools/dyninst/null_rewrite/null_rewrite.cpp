#include <iostream>

#include "BPatch.h"

BPatch bpatch;

int main(int argc, char const* argv[]) {
  if (argc != 3) {
	  std::cerr << "usage: program <input binary path> <output binary path>" << std::endl;
	  return -1;
  }

  auto* input_path = argv[1];
  auto* output_path = argv[2];
  auto* app = bpatch.openBinary(input_path, true);
  if (!app) {
	  std::cerr << "cannot open binary: " << input_path << std::endl;
	  return -2;
  }

  if (!app->writeFile(output_path)) {
    std::cerr << "cannot write binary: " << output_path << std::endl;
    return -3;
  }    

  return 0;
}
