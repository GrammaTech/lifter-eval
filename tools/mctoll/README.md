# Limitations

- Requires external function prototypes to be known. That means it will work for "Hello World" with `--include-files="/usr/include/stdio.h"` option but other binaries will require specific headers.
- Windows binaries are not supported.
- ARM64 is not supported: https://github.com/microsoft/llvm-mctoll/issues/150
- ARM32 fails with "Failed to decode instruction"

# Bootstrap

- Build docker image:
    ```shell
    docker build --progress plain -t registry.gitlab.com/metis/lifter-eval/mctoll .
    ```

- Lift a binary to LLVM text bitcode:
    ```shell
    docker run --rm -v $(pwd):/gt -w /gt registry.gitlab.com/metis/lifter-eval/mctoll llvm-mctoll -d --include-files="/usr/include/stdio.h" -o <output>.ll <input binary>
    ```

- Reassemble LLVM text bitcode:
    ```shell
    docker run --rm -v $(pwd):/gt -w /gt registry.gitlab.com/metis/lifter-eval/mctoll clang -o <output binary> <input>.ll
    ```

# Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/mctoll bash
```

Produce `*.mctoll.ll`:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.mctoll.ll|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Rewrite the binaries and run the smoke tests:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.mctoll.rewritten.errno|')
```

Produce binaries with AFL instrumentation and smoke-test them:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.mctoll.afl.errno|')
```

# Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval/mctoll bash
```

Produce `*.mctoll.ll`:
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.mctoll.ll|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Rewrite the binaries and run the smoke tests:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.mctoll.rewritten.errno|')
```

Produce binaries with AFL instrumentation and smoke-test them:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.mctoll.afl.errno|')
```

# Results

Ubuntu 20 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .ll               | 0.74%  | 2293     |
| .rewritten        | 0.74%  | 2293     |
| .rewritten.errno  | 0.74%  | 2293     |
| .afl              | 0.74%  | 2293     |
| .afl.errno        | 0.74%  | 2293     |

Ubuntu 16 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .ll               | 1.26%  | 1021     |
| .rewritten        | 1.26%  | 1021     |
| .rewritten.errno  | 1.26%  | 1021     |
| .afl              | 1.26%  | 1021     |
| .afl.errno        | 1.26%  | 1021     |
