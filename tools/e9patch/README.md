# Limitations

- x86_64 only

# Ubuntu 20 x86_64 binaries

```shell
bin/alternate_make.py --isa x86_64 --os ubuntu20 -- .e9patch.rewritten .e9patch.rewritten.errno .e9patch.afl .e9patch.afl.errno
```

# Results

"e9patch.rewritten" transform rewrites without actual patching (no matched instruction to patch): "e9tool -M 'asm=/non-exinstent-instruction/' -P empty"

| transform                               | rate    | failures |
| ---                                     | ---     | ---      |
| x86_64.ubuntu20.e9patch.rewritten       | 100.00% | 0        |
| x86_64.ubuntu20.e9patch.rewritten.errno | 76.83%  | 552      |
| x86_64.ubuntu20.e9patch.afl             | 100.00% | 0        |
| x86_64.ubuntu20.e9patch.afl.errno       | 34.59%  | 1558     |
| x86_64.ubuntu16.e9patch.rewritten       | 100.00% | 0        |
| x86_64.ubuntu16.e9patch.rewritten.errno | 88.97%  | 114      |
| x86_64.ubuntu16.e9patch.afl             | 100.00% | 0        |
| x86_64.ubuntu16.e9patch.afl.errno       | 40.81%  | 612      |