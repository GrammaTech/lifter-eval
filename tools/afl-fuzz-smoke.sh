#!/bin/bash
#
# Fuzz a binary with just a single input
#
set -ex

binary_path=$(readlink -f $1)
shift

temp_dir=$(mktemp -d)
pushd $temp_dir
mkdir input && echo test > input/data
mkdir output
AFL_BENCH_JUST_ONE=1 AFL_SKIP_CPUFREQ=1 AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1 afl-fuzz -t 2000 -i input -o output -- $binary_path $@
popd
rm -r $temp_dir
