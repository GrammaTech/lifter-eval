import os
import sys
import argparse
import subprocess

DEFAULT_MAX_CONTAINERS = 1
DEFAULT_TIMEOUT_S = 10 * 60
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
LIFTER_EVAL_DIR = os.path.abspath(SCRIPT_DIR + "/..")

# Import bin scripts
sys.path.insert(1, LIFTER_EVAL_DIR + "/src")
from lifter_eval.cli import add_options_no_target, combined_subjects
from lifter_eval.make import collect_targets
from lifter_eval.results import stat
from lifter_eval.target import binary_name
from lifter_eval.traits import Spec
from lifter_eval.parallelize import ProcessScheduler, run_tasks, serial_tasks_from_commands

FAIL_COLOR = "\033[91m"
DEFAULT_COLOR = "\033[0m"

LIFTER_EVAL_ROOT = "/lifter-eval"
DEFAULT_TARGET_DIR = "results"
SUBJECT_DIR = "subjects"
DOCKER_CONTAINER_NAME = "generic-lifter-container"
DOCKER_HOST_CONTAINER_NAME = "generic-lifter-container-host"
TEST_HOST_URL = "test.com"
DEFAULT_DOCKER_ARGS = [
    "docker",
    "exec",
    "-w",
    LIFTER_EVAL_ROOT,
]
UNIQUE_DOCKER_ARGS = [
    "docker",
    "run",
    "--rm",
    "-w",
    LIFTER_EVAL_ROOT,
    "-v",
    LIFTER_EVAL_DIR + ":" + LIFTER_EVAL_ROOT,
]
LIFTER_EVAL_DOCKER_IMAGE = "registry.gitlab.com/grammatech/lifter-eval/build"
LIFT_OUTPUT_EXT = ".generic.rewritten"
FAIL_RECORD_EXT = ".failed"
TEST_OUTPUT_EXT = LIFT_OUTPUT_EXT + ".errno"


def clean_test_artifacts(target_dir):
    for file in os.listdir(target_dir):
        if file.endswith((LIFT_OUTPUT_EXT, FAIL_RECORD_EXT, TEST_OUTPUT_EXT)):
            os.remove(os.path.join(target_dir, file))


def compile_target_list(subjects, spec):
    targets = collect_targets(subjects, spec)[0]
    output = []
    for target in targets:
        output.append(binary_name(target))
    return output


def make_shared_target_cmd(docker_name, make_args, timeout_s):
    lifter_args = DEFAULT_DOCKER_ARGS.copy()
    lifter_args.extend(
        [
            "--env",
            f"LIFTER_EVAL_GENERIC_TIMEOUT={timeout_s}s",
            docker_name,
            "make",
        ]
    )
    lifter_args.extend(make_args)
    return lifter_args


def make_unique_target_cmd(docker_image, make_args, timeout_s, additional_volume, hostname=None):
    lifter_args = UNIQUE_DOCKER_ARGS.copy()
    if additional_volume:
        lifter_args.extend(["-v", additional_volume])
    lifter_args.extend(["--env", f"GENERIC_TIMEOUT={timeout_s}s"])
    if hostname:
        lifter_args.extend(["--hostname", hostname])
    lifter_args.extend([docker_image, "make"])
    lifter_args.extend(make_args)
    return lifter_args


def stop_docker_container(container_name):
    try:
        subprocess.check_call(["docker", "rm", "-f", container_name])
    except:
        print(f"Warning: Failed to kill docker container {container_name}")


def stop_docker():
    stop_docker_container(DOCKER_CONTAINER_NAME)
    stop_docker_container(DOCKER_HOST_CONTAINER_NAME)


def start_docker_container(docker_image, container_name, hostname, additional_volume):
    lifter_args = [
        "docker",
        "run",
        "-dt",
        "-v",
        LIFTER_EVAL_DIR + ":" + LIFTER_EVAL_ROOT,
        "--name",
        container_name,
    ]
    if hostname:
        lifter_args.extend(["--network=host"])
        lifter_args.extend(["--hostname", hostname])
    if additional_volume:
        lifter_args.extend(["-v", additional_volume])
    lifter_args.append(docker_image)
    subprocess.check_call(lifter_args)


def start_docker(docker_image, additional_volume):
    # Make sure we aren't already running these docker containers
    stop_docker()
    start_docker_container(docker_image, DOCKER_CONTAINER_NAME, None, additional_volume)
    start_docker_container(docker_image, DOCKER_HOST_CONTAINER_NAME, TEST_HOST_URL, additional_volume)


def lift_target_cmd(
    lift_command, target_dir, target, timeout_s, unique_containers, docker_image, additional_volume
):
    lift_target = target.replace(".elf", LIFT_OUTPUT_EXT)
    make_args = ["LIFT_COMMAND=" + lift_command, os.path.join(target_dir, lift_target)]
    if unique_containers:
        return make_unique_target_cmd(docker_image, make_args, timeout_s, additional_volume)
    return make_shared_target_cmd(
        DOCKER_CONTAINER_NAME,
        make_args,
        timeout_s,
    )


def smoke_test_target_cmd(
    target_dir, target, timeout_s, unique_containers, docker_image, additional_volume
):
    test_target = target.replace(".elf", TEST_OUTPUT_EXT)
    make_args = [os.path.join(target_dir, test_target)]
    host_required = "sendmail" in target
    if unique_containers:
        hostname = TEST_HOST_URL if host_required else None
        return make_unique_target_cmd(docker_image, make_args, timeout_s, hostname, additional_volume)

    docker_name = DOCKER_HOST_CONTAINER_NAME if host_required else DOCKER_CONTAINER_NAME
    return make_shared_target_cmd(docker_name, make_args, timeout_s)


def lift_and_test_targets(
    lift_command,
    target_dir,
    targets,
    timeout_s,
    max_containers,
    unique_containers,
    docker_image,
    additional_volume,
):
    scheduler = ProcessScheduler(max_containers)
    tasks = []
    for target in targets:
        lift_task = lift_target_cmd(
            lift_command, target_dir, target, timeout_s, unique_containers, docker_image, additional_volume
        )
        test_task = smoke_test_target_cmd(
            target_dir, target, timeout_s, unique_containers, docker_image, additional_volume
        )
        tasks.extend(serial_tasks_from_commands([lift_task, test_task]))

    run_tasks(tasks, scheduler)


def generate_report(subjects, spec):
    stat(subjects, [LIFT_OUTPUT_EXT, TEST_OUTPUT_EXT], spec)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Test a generic lifter against a list of subject binaries"
    )
    parser.add_argument(
        "-i",
        dest="docker_image",
        action="store",
        required=True,
        help="docker image to use for running the lifter",
    )
    parser.add_argument(
        "-l",
        dest="lift_command",
        action="store",
        required=True,
        help="command used to invoke the lifter (to be called in the format: `lift_command input_elf output_elf`)",
    )
    parser.add_argument(
        "-d",
        dest="target_directory",
        action="store",
        required=False,
        help="path to the subject binaries",
        default=DEFAULT_TARGET_DIR,
    )
    parser.add_argument(
        "-c",
        dest="clean",
        action="store_true",
        required=False,
        help="clean test artifacts before running",
    )
    parser.add_argument(
        "--timeout",
        type=int,
        action="store",
        required=False,
        help="Lift timeout in seconds",
        default=DEFAULT_TIMEOUT_S,
    )
    parser.add_argument(
        "--max-containers",
        type=int,
        metavar="N",
        help="run up to N docker containers in parallel",
        default=DEFAULT_MAX_CONTAINERS,
    )
    parser.add_argument(
        "--unique-containers",
        action="store_true",
        help="use a unique docker container for each test process",
    )
    parser.add_argument(
        "--additional-volume",
        action="store",
        help="additional volume to mount to docker containers",
    )
    add_options_no_target(parser)
    args = parser.parse_args()
    if not args.unique_containers:
        start_docker(args.docker_image, args.additional_volume)
    ret_val = 0
    try:
        results_spec = Spec(
            args.isa,
            args.os,
            args.compiler,
            args.pie,
            args.optimize,
            args.obfuscate,
            args.strip,
        )

        if args.clean:
            clean_test_artifacts(args.target_directory)

        subjects = combined_subjects(args)
        targets = compile_target_list(subjects, results_spec)
        lift_and_test_targets(
            args.lift_command,
            args.target_directory,
            targets,
            args.timeout,
            args.max_containers,
            args.unique_containers,
            args.docker_image,
            args.additional_volume
        )
        generate_report(subjects, results_spec)
        print("Lifter Eval Done!")
    except Exception as ex:
        print(
            "%slifter test failed with exception: %s%s"
            % (FAIL_COLOR, str(ex), DEFAULT_COLOR)
        )
        ret_val = 1
    finally:
        if not args.unique_containers:
            stop_docker()
    exit(ret_val)
