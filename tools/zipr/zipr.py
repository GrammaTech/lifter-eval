from lifter_eval.tool import Tool, register


@register
class ZiprTool(Tool):
    name = "zipr"
    afl_env = {"AFL_SKIP_BIN_CHECK": "1"}
