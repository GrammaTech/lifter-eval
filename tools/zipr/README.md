# Limitations

- Windows binaries (x64) are not supported.
- ARM (32 and 64) binaries are dependent on IDA.

# Prepare local environment

If host filesystem is mapped to zipr/zafl docker containers make sure it has write access for other users (docker imagese are set up to run under "zuser")"

```shell
chmod o+w .
chmod o+w . results
``

# Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval/zipr bash
```

Produce `*.zipr.rewritten`:
```
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.zipr.rewritten|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Run smoke tests for the rewritten gcc/clang binaries:
```
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.zipr.rewritten.errno|')
```

Exit docker container and run "icx" image ("registry.gitlab.com/metis/lifter-eval/icx"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/icx bash
```

Run smoke tests for the rewritten icx binaries:
```
make -k $(cat compilers/list-targets-icx.txt | sed -E 's|(.+).elf|results/\1.zipr.rewritten.errno|')
```

# Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/grammatech/lifter-eval/zipr bash
```

Produce `*.zipr.rewritten`:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.zipr.rewritten|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Run smoke tests for the rewritten binaries:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.zipr.rewritten.errno|')
```

# AFL instrumentation

## Ubuntu20

Run docker:
```shell
docker run --rm -it --entrypoint bash -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/zafl
```

Rewrite binaries (produce *.zipr.afl):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.zipr.afl|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Run smoke tests for the gcc/clang AFL instrumented binaries:
```
make -k $(cat compilers/list-targets.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.zipr.afl.errno|')
```

Exit docker container and run "icx" image ("registry.gitlab.com/metis/lifter-eval/icx"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/icx bash
```

Run smoke tests for the icx AFL instrumented binaries:
```
make -k $(cat compilers/list-targets-icx.txt | sed -E 's|(.+).elf|results/\1.zipr.afl.errno|')
```

## Ubuntu16

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it --entrypoint bash -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval/zafl
```

Rewrite binaries (produce *.zipr.afl):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.zipr.afl|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Run smoke tests for the rewritten binaries:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.zipr.afl.errno|')
```

# Results

Ubuntu 20 binaries
| transform             | rate   | failures | 
| ---                   | ---    | ---      |
| .zipr.rewritten       | 93.72% | 145      |
| .zipr.rewritten.errno | 79.96% | 463      |
| .zipr.afl             | 73.07% | 622      |
| .zipr.afl.errno       | 36.54% | 1466     |

Ubuntu 16 binaries
| transform             | rate   | failures | 
| ---                   | ---    | ---      |
| .zipr.rewritten       | 86.46% | 140      |
| .zipr.rewritten.errno | 84.33% | 162      |
| .zipr.afl             | 98.74% | 17       |
| .zipr.afl.errno       | 72.34% | 286      |
