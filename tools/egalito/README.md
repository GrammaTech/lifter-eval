# Limitations

- Seems like AFL binaries require patched `afl-fuzz` version: https://github.com/columbia/egalito-artefact/blob/master/README-afl.txt
- Windows binaries are not supported.
- It seems like egalito does not convey `RUNPATH` into rewritten binaries, this might prevent them from running (observed rfor "vlc" subject).

## ARM

- It seems like egalito supported ARM64 binaries at some point but that was broken around 2018.
- ARM32: "Exception: file is unsupported"

## Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval/egalito bash
```

Rewrite the binaries (produce `*.egalito.rewritten`):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.egalito.rewritten|')
```

Produce binaries with AFL instrumentation (`*.egalito.afl`):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.egalito.afl|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval bash
```

Run smoke tests for the rewritten binaries:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.egalito.rewritten.errno|')
```

## Ubuntu 16 binaries

Run docker with host's "results-16" mapped to container's "results".
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval/egalito bash
```

Run rewriting (produce `*.egalito.rewritten`):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.egalito.rewritten|')
```

Produce binaries with AFL instrumentation (`*.egalito.afl`):
```shell
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.egalito.afl|')
```

Exit docker container and run "Ubuntu 16" image ("registry.gitlab.com/metis/lifter-eval:16"):

```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval results-16:/lifter-eval/results registry.gitlab.com/metis/lifter-eval:16 bash
```

Run smoke tests for the rewritten binaries:
```
make -k $(cat compilers/list-targets-16.txt | sed -E 's|(.+).elf|results/\1.egalito.rewritten.errno|')
```

## Results

Ubuntu 20 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .rewritten        | 98.27% | 40       |
| .rewritten.errno  | 22.68% | 1786     |
| .afl              | 66.02% | 785      |
| .afl.errno        | 0%     | 2310     |

Ubuntu 16 binaries
| transform         | rate   | failures | 
| ---               | ---    | ---      |
| .rewritten        | 99.03% | 10       |
| .rewritten.errno  | 44.39% | 575      |
| .afl              | 93.62% | 66       |
| .afl.errno        | 0%     | 1034     |
