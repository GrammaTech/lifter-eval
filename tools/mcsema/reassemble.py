#!/usr/bin/env python3
import argparse

from lift import ida_rebase, run_cmd

def main():
    parser = argparse.ArgumentParser(description="Lift a binary to LLVM")
    parser.add_argument("input", help="input LLVM bitcode")
    parser.add_argument("output", help="output binary (ELF x86-64)")
    parser.add_argument("--pie", action="store_true")
    options = parser.parse_args()

    cmd = ['remill-clang-11', '-o', options.output, options.input]
    if options.pie:
        cmd += [f'-Wl,--section-start=.section_{ida_rebase:x}=0x{ida_rebase:x}']
    run_cmd(cmd)

if __name__ == "__main__":
    main()
