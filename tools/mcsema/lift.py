#!/usr/bin/env python3
import argparse
import os
import shlex
import subprocess
import sys

# Re-position the binary and pretend to load it at this address.
# See https://github.com/lifting-bits/mcsema/blob/master/docs/McSemaWalkthrough.md#control-flow-recovery
ida_rebase = 0x1ff00000

def run_cmd(cmd, env: dict=None):
    if env:
        env = env.copy()
        env.update(os.environ)
    print(' '.join(map(shlex.quote, cmd)))
    sys.stdout.flush()
    subprocess.check_call(cmd, env=env)

def _make_cfg(input, pie):
    bin_name = os.path.splitext(os.path.basename(input))[0]
    output = f'{bin_name}.cfg'
    cmd = ['/usr/local/bin/mcsema-disass',
           '--disassembler', '/opt/ida-7.0/idat64',
           '--arch', 'amd64',
           '--os', 'linux',
           '--entrypoint', 'main',
           '--output', output,
           '--binary', input,
           '--log_file', 'log.txt'
    ]
    if pie:
           cmd += ['--pie-mode', '--rebase', str(ida_rebase)]
    run_cmd(cmd, env={'IDALOG': os.path.abspath('ida_log.txt')})
    return output 

def _lift(cfg, output, pie):
    cmd = ['mcsema-lift-11.0',
           '--arch', 'amd64',
           '--os', 'linux',
           '--cfg', cfg,
           '--explicit_args', '--merge_segments',
           '--output', output
    ]
    if pie:
           cmd += ['--name_lifted_sections']
    run_cmd(cmd)

def main():
    parser = argparse.ArgumentParser(description="Lift a binary to LLVM")
    parser.add_argument("input", help="input binary (ELF x86-64)")
    parser.add_argument("output", help="output LLVM bitcode")
    parser.add_argument("--pie", action="store_true")
    options = parser.parse_args()

    cfg = _make_cfg(options.input, options.pie)
    _lift(cfg, options.output, options.pie)

if __name__ == "__main__":
    main()
