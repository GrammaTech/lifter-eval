- Build docker image:
    ```shell
    docker build --progress plain -t registry.gitlab.com/metis/lifter-eval/mcsema .
    ```

- Lift a binary to LLVM bitcode:
    ```shell
    docker run --rm -v $(pwd):/gt -w /gt registry.gitlab.com/metis/lifter-eval/mcsema lift.py <input binary> <output>.bc
    ```

    Add `--pie` option for pie binaries.


- Reassemble LLVM bitcode:
    ```shell
     docker run --rm -v $(pwd):/gt -w /gt registry.gitlab.com/metis/lifter-eval/mcsema reassemble.py <input>.bc <output binary>
    ```

    Add `--pie` option for pie binaries.


- Batch Run McSema:
    ```shell
      python3 ./lift_directory.py --llvm_version 12 --disassembler /Applications/IDA/ida64.app/Contents/MacOS/ --workspace_dir /tmp --clang clang --binary_dir ./results --num_workers 5
    ``` 
