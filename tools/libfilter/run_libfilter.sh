set -e

RELATIVE_LIB_DIR="_libfilter/$1.lib"
ABSOLUTE_LIB_DIR="$(dirname "${1}")/$RELATIVE_LIB_DIR"
INTERMEDIATE_FN="$1.int"

rm -rf $ABSOLUTE_LIB_DIR
cp $1 $INTERMEDIATE_FN
/libfilter/app/libfilter_main -l $RELATIVE_LIB_DIR $INTERMEDIATE_FN
mv $INTERMEDIATE_FN $2
