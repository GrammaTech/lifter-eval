# Limitations

- Windows binaries are not supported.
- Ubuntu 16 binaries are not supported.

# Environment

Build "registry.gitlab.com/grammatech/lifter-eval/libfilter" docker image:
```shell
docker build -t registry.gitlab.com/grammatech/lifter-eval/libfilter -f tools/libfilter/Dockerfile compilers/
```

## Ubuntu 20 binaries

Run docker:
```shell
docker run --rm -it -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval/libfilter bash
```

Filter the binaries and their libraries (produce `*.libfilter.rewritten`):
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.libfilter.rewritten|')
```

Exit docker container and run "Ubuntu 20" image ("registry.gitlab.com/metis/lifter-eval"):
```shell
docker run --rm -it --hostname test.com -w /lifter-eval -v $(pwd):/lifter-eval registry.gitlab.com/grammatech/lifter-eval bash
```

Run smoke tests for the rewritten binaries:
```shell
make -k $(cat compilers/list-targets.txt compilers/list-targets-icx.txt compilers/list-targets-ollvm.txt | sed -E 's|(.+).elf|results/\1.libfilter.rewritten.errno|')
```

## Results

Ubuntu 20 binaries
| transform         | rate   | failures |
| ---               | ---    | ---      |
| .rewritten        | --%    | --       |
| .rewritten.errno  | --%    | --       |
