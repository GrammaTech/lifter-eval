########################################################################
# Top level driver.
ISA_X64 = x86_64
ISA_X32 = x86
ISA_ARM64 = aarch64
ISA_ARM32 = arm
ISAS := $(ISA_X64) $(ISA_ARM64) $(ISA_X32) $(ISA_ARM32)

OS_UBUNTU20 := ubuntu20
OS_UBUNTU16 := ubuntu16
OS_LIST := $(OS_UBUNTU20) $(OS_UBUNTU16)

COMPILERS := clang gcc icx ghc ocaml go gfortran
OPT_FLAGS := O0 O1 O2 O3 Os Ofast
EXE_FLAGS := pie nopie static

OBF_FLAGS := fla sub bcf

########################################################################
# Optional environment variables, must start with "LIFTER_EVAL_" prefix

# Timeouts
LIFTER_EVAL_DDISASM_TIMEOUT ?= 4h
LIFTER_EVAL_DYNINST_TIMEOUT ?= 4h
LIFTER_EVAL_E9PATCH_TIMEOUT ?= 15m # expected to be fast
LIFTER_EVAL_EGALITO_TIMEOUT ?= 4h
LIFTER_EVAL_GENERIC_TIMEOUT ?= 4h
LIFTER_EVAL_LIBFILTER_TIMEOUT ?= 4h
LIFTER_EVAL_REOPT_TIMEOUT ?= 4h
LIFTER_EVAL_REVNG_TIMEOUT ?= 4h
LIFTER_EVAL_ZIPR_TIMEOUT ?= 4h

LIFTER_EVAL_FUNC_TIMEOUT ?= 1h

LIFTER_EVAL_DDISASM_OPTIONS ?= # custom ddisasm options, e.g. "-j 16"

with_timings = /usr/bin/time -v -o $(1).time -- $(2)
with_timeout = rm -f $(1).timeout; \
			   $(call with_timings,$(1),timeout $(3) $(2)) \
			   		|| (/usr/bin/test $$? == 124 \
					   	&& echo "timed out: $(3)" \
						&& echo $(3) > $(1).timeout \
						&& exit 124)

########################################################################
# Build binaries
arch_flag = $(shell echo $(1)|grep -q 'results/$(ISA_X32)\.' && echo "-m32" ||echo "")

subject_deps_dir = $(shell echo $(1) | sed -E 's/.(gcc|clang|ollvm).*.elf/.gcc-clang.deps/;s/.icx.*.elf/.icx.deps/')

check_isa = case "$(1)" in $(ISA_X64)|$(ISA_X32)) uname -m | grep -q "x86_64" || (echo "please use linux/amd64 base image to build the binary" >&2 && false);; \
                           $(ISA_ARM64)) uname -m | grep -q "aarch64" || (echo "please use linux/arm64 base image to build the binary" >&2 && false);; \
                           $(ISA_ARM32)) uname -m | grep -q "armv7l" || (echo "please use linux/arm base image to build the binary" >&2 && false);; \
                           *) echo "unknown isa: $(1)" >&2 && false;; \
            esac

check_os = case "$(1)" in $(OS_UBUNTU20)) grep -q "Ubuntu 20.04" /etc/issue || (echo "please use Ubuntu 20.04 base image to build the binary" >&2 && false);; \
                          $(OS_UBUNTU16)) grep -q "Ubuntu 16.04" /etc/issue || (echo "please use Ubuntu 16.04 base image to build the binary" >&2 && false);; \
                          *) echo "unknown os: $(1)" >&2 && false;; \
           esac

# collect and update dependency .so so they would not point to the build directory
collect_and_update_dependency = \
	set -e; \
	subject=$(call subject_name,$(1)); \
	so_deps=$$(ldd $(1) | grep subjects/$$subject/ | sed -E 's|.*(subjects/'$$subject'/.*\.so.*)\s+.+|\1|') ; \
	if test ! -z "$$so_deps"; then \
		so_deps_dir=$$(readlink -f $(call subject_deps_dir,$(1))); \
		mkdir -p $$so_deps_dir; \
		cp $$so_deps $$so_deps_dir; \
		patchelf --set-rpath $$so_deps_dir $(1); \
		for x in $$so_deps; \
			do patchelf --set-rpath $$so_deps_dir $$so_deps_dir/$$(basename $$x); \
		done; \
	fi

results/%.nostrip.elf:
	@set -e;\
	isa=$$(echo $@ | cut -d . -f 1 | sed 's|results/||');\
	$(call check_isa,$$isa);\
	os=$$(echo $@ | cut -d . -f 2);\
	$(call check_os,$$os);\
	compiler=$$(echo $@ | cut -d . -f 4 | sed 's/ollvm/clang/') ;\
	opt_flag=$$(echo $@ | cut -d . -f 5 | sed 's/fla/-mllvm -fla/;s/sub/-mllvm -sub/;s/bcf/-mllvm -bcf -mllvm -bcf_prob=100/;s/O/-O/');\
	pie_flag=$$(echo $@ | cut -d . -f 6);\
	shell_script=subjects/$(call subject_name,$@).sh;\
	test -f $$shell_script || (echo "please build '$@' first using 'PYTHONPATH=src subjects/$(call subject_name,$@).py build --isa $$isa --os $$os --compiler $$compiler $$(echo $@ | cut -d . -f 4 | (grep -q ollvm && echo --obfuscate || echo --optimize)) $$(echo $@ | cut -d . -f 5) --pie $$pie_flag'" >&2 && false);\
	arch_flag=$$(echo $$isa | grep -q '$(ISA_X32)$$' && echo "-m32" || echo "");\
	pie_cc_flag=$$(echo $$pie_flag | grep -q nopie && echo "-fno-pie" || echo -fpie);\
	pie_ld_flag=$$(echo $$pie_flag | grep -q nopie && ((echo $$compiler | grep -q clang && echo "-nopie") \
                                                                               		    || echo "-no-pie") \
                                                   || echo "-pie");\
	env CC=$$compiler\
		CFLAGS="$$opt_flag $$pie_cc_flag $$arch_flag"\
		LDFLAGS="$$pie_ld_flag $$arch_flag"\
		$$shell_script compile $@
	@$(call collect_and_update_dependency,$@)

results/%.strip.elf: results/%.nostrip.elf
	strip -s $< -o $@

check_prerequisite = test -f $(1) || (echo "required '$(1)' is missing" && false)

with_error_ignored = (set +e; $(1); rc=$$?; /usr/bin/test $$rc == $(2) || exit $$rc)

check_failed_cache = ([ ! -f $(1).failed ] || (echo "skipping previously failed target because '$(1).failed' is present" && false))
update_failed_cache = (rm -f $(1) && touch $(1).failed && false)
with_failures_cache = $(call check_failed_cache,$(1)) && (($(2)) || $(call update_failed_cache,$(1)))

smoke_test = timeout 1m $(2) ./subjects/$(call subject_name,$(1)).sh smoke-test $(1)

evaluate = $(call with_timeout,$(2),$(3) ./subjects/$(call subject_name,$(1)).sh evaluate $(1),$(LIFTER_EVAL_FUNC_TIMEOUT))

check_errno = /usr/bin/test $$(cat $(1)) "==" 0 || (echo "skipping evaluation because non-zero '$(1)' is present" && false)

########################################################################
# Lift IR
results/%.ddisasm.asm:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,ddisasm $(LIFTER_EVAL_DDISASM_OPTIONS) --asm $@ results/$*.exe,$(LIFTER_EVAL_DDISASM_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.retrowrite.S:
	@$(call check_failed_cache,$@)
	[ -f results/$*.elf ] && (/usr/bin/time -v -o $@.time -- env PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin LD_LIBRARY_PATH=/usr/local/lib /retrowrite/retrowrite results/$*.elf $@ || $(call update_failed_cache,$@))

results/%.uroboros.S:
	@$(call check_failed_cache,$@)
	[ -f results/$*.elf ] && (/usr/bin/time -v -o $@.time -- uroboros results/$*.elf $@ || $(call update_failed_cache,$@))

results/%.dyninst.rewritten:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,/null_rewrite/null_rewrite results/$*.elf $@,$(LIFTER_EVAL_DYNINST_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.dyninst.afl:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,/opt/dyninst-env/afl-dyninst/afl-dyninst -i results/$*.elf -o $@,$(LIFTER_EVAL_DYNINST_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.e9patch.rewritten:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,e9tool -M 'asm=/non-exinstent-instruction/' -P empty -o $@ $$(readlink -f results/$*.elf),$(LIFTER_EVAL_E9PATCH_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.e9patch.afl:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,e9afl -o $@ results/$*.elf,$(LIFTER_EVAL_E9PATCH_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.multiverse.rewritten:
	@$(call check_failed_cache,$@)
	[ -f results/$*.elf ] && (cp results/$*.elf /multiverse/target.elf && /usr/bin/time -v -o $@.time -- bash -c "cd /multiverse && python2 multiverse.py --execonly --arch x86-64 target.elf" || $(call update_failed_cache,$@))
	mv -f /multiverse/target.elf-r $@

results/%.multiverse.rewritten.errno: results/%.multiverse.rewritten
	$(call dump_errno,$(call smoke_test,$<,env LD_BIND_NOW=1),$@)

results/%.multiverse.rewritten.func: results/%.multiverse.rewritten results/%.multiverse.rewritten.errno
	@$(call check_errno,$(word 2,$^))
	$(call dump_errno,$(call evaluate,$<,$@,env LD_BIND_NOW=1),$@)

results/%.mctoll.ll:
	[ -f results/$*.elf ] && /usr/bin/time -v -o $@.time -- /llvm-project/build/bin/llvm-mctoll -d --include-files="/usr/include/stdio.h" -o $@ results/$*.elf

results/%.reopt.ll:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,reopt --export-llvm $@ results/$*.elf,$(LIFTER_EVAL_REOPT_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.reopt.rewritten:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,reopt -o $@ results/$*.elf,$(LIFTER_EVAL_REOPT_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.revng.rewritten:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,bash -c "set -ex; input=$$(readlink -f results/$*.elf); output=$$(readlink -f $@); cd /orchestra; orc shell -c revng bin/revng translate -o \$$output \$$input",$(LIFTER_EVAL_REVNG_TIMEOUT)) || $(call update_failed_cache,$@)

results/%.mcsema.bc:
	[ -f results/$*.elf ] && /usr/bin/time -v -o $@.time -- mcsema-lift results/$*.elf $@

results/%.zipr.rewritten:
	@$(call check_prerequisite,results/$*.elf)
	@$(call with_failures_cache,\
		$@,\
		$(call with_timeout,\
			$@,\
			bash -c 'set -ex; input=$$(readlink -f results/$*.elf); output=$$(readlink -f $@); cd ~; $(call with_error_ignored,/opt/ps_zipr/tools/pszr $$input $$output,1)',\
			$(LIFTER_EVAL_ZIPR_TIMEOUT)))

results/%.zipr.afl:
	@$(call check_prerequisite,results/$*.elf)
	@$(call with_failures_cache,\
		$@,\
		$(call with_timeout,\
			$@,\
			bash /run_zafl.sh $(shell pwd)/results/$*.elf $(shell pwd)/$@,\
			$(LIFTER_EVAL_ZIPR_TIMEOUT)))

results/%.egalito.rewritten:
	@$(call check_prerequisite,results/$*.elf)
	@$(call with_failures_cache,\
		$@,\
		$(call with_timeout,$@,/egalito/app/etelf -m results/$*.elf $@,$(LIFTER_EVAL_EGALITO_TIMEOUT))\
			&& test -f $@) # egalto can report 0-exit code while not producing the resulting file ("Exception: error useReg")

results/%.egalito.afl:
	@$(call check_prerequisite,results/$*.elf)
	@$(call with_failures_cache,\
		$@,\
		$(call with_timeout,$@,bash -c "cd /egalito/app && ./etcoverage $(shell pwd)/results/$*.elf $(shell pwd)/$@",$(LIFTER_EVAL_EGALITO_TIMEOUT))\
			&& test -f $@) # egalto can report 0-exit code while not producing the resulting file ("Exception: error useReg")

results/%.libfilter.rewritten:
	@$(call check_failed_cache,$@)
	cp results/$*.elf $@.int
	$(call with_timeout,$@,/libfilter/app/libfilter_main -l _libfilter/$@.lib $@.int,$(LIFTER_EVAL_LIBFILTER_TIMEOUT)) || $(call update_failed_cache,$@)
	mv $@.int $@

results/%.generic.rewritten:
	@$(call check_failed_cache,$@)
	$(call with_timeout,$@,$(LIFT_COMMAND) results/$*.elf $@,$(LIFTER_EVAL_GENERIC_TIMEOUT)) || $(call update_failed_cache,$@)

dump_errno=($(1) && echo $$? > $(2)) || (echo $$? > $(2) && false)

# Extract dependency libs and RUNPATH
libs = $(shell ldd $(1) \
	|sed 's/^\s.*=>\s//;s/(0x[a-f0-9]*)$$//' \
	|grep "^\s*/" \
	|tr '\n' ' ' \
    && readelf -d $(1) | grep -E "RUNPATH|RPATH" | sed 's/^.*\[/-Wl,-rpath=/;s/\]$$//')

base = $(basename $(basename $(1))).elf
pie = $(shell file $(1)|grep -q "LSB executable" && echo -no-pie)

results/%.rewritten: results/%.S
	@$(call check_failed_cache,$@)
	[ -f $< ] && (gcc $< $(call arch_flag,$<) $(call libs,$(call base,$<)) $(call pie,$(call base,$<)) -o $@ -Wl,--export-dynamic || $(call update_failed_cache,$@))
	echo $<|grep -q \\.strip\\. && strip -s $@ || echo "no need to strip"

results/%.rewritten: results/%.ll
	[ -f $< ] && clang -o $@ $<
	echo $<|grep -q \\.strip\\. && strip -s $@ || echo "no need to strip"

results/%.rewritten: results/%.bc
	[ -f $< ] && mcsema-reassemble $< $@
	echo $<|grep -q \\.strip\\. && strip -s $@ || echo "no need to strip"

results/%.ddisasm.gtirb:
	$(call with_failures_cache,\
		$@,\
		$(call with_timeout,\
			$@,ddisasm $(LIFTER_EVAL_DDISASM_OPTIONS) --ir results/$*.ddisasm.gtirb results/$*.elf,$(LIFTER_EVAL_DDISASM_TIMEOUT)))

results/%.ddisasm.rewritten: results/%.ddisasm.gtirb
	$(call with_failures_cache,\
		$@,\
		$(call with_timeout,\
			$@,gtirb-pprinter $< --binary $@,$(LIFTER_EVAL_DDISASM_TIMEOUT)))

results/%.ddisasm.afl: results/%.ddisasm.gtirb
	$(call with_failures_cache,\
		$@,\
		$(call with_timeout,\
			$@,bash -c "PATH=$$PATH:$$(pwd)/compilers/afl-as gtirb-pprinter $< --use-gcc afl-gcc-wrapper.sh --binary $@",$(LIFTER_EVAL_DDISASM_TIMEOUT)))

results/%.afl: results/%.S
	@$(call check_failed_cache,$@)
	[ -f $< ] && (compilers/afl-as/afl-ins-as.sh $< $@ $(call libs,$(call base,$<)) $(call pie,$(call base,$<)) || $(call update_failed_cache,$@))
	echo $<|grep -q \\.strip\\. && strip -s $@ || echo "no need to strip"

results/%.afl: results/%.ll
	@$(call check_failed_cache,$@)
	afl-clang -o $@ $< || $(call update_failed_cache,$@)

results/hello%.map: results/hello%.afl
	afl-showmap -o $@ -- $<

results/sqlite%.map: results/sqlite%.afl
	afl-showmap -o $@ -- $< --version

results/nginx%.map: results/nginx%.afl
	afl-showmap -o $@ -- $< -h

include subjects/chromium/Makefile.chrome
include subjects/firefox/Makefile.firefox

########################################################################
# Metrics

results/%.size: results/%
	bloaty --csv $< > $@

subject_name = $(shell basename $(1) | sed -E 's/[^.]+\.[^.]+\.([^.]+).*/\1/')

results/%.func: results/% results/%.errno
	@$(call check_errno,$(word 2,$^))
	$(call dump_errno,$(call evaluate,$<,$@),$@)

results/%.errno:
	$(call dump_errno,$(call smoke_test, $(shell echo $@ | sed 's/\.errno//')),$@)

########################################################################
# Default rule. Without it the default will be the next one - `real-clean`.
# You surely don't want `real-clean` your results by default.
all:
	@echo "Use 'make <results/subject.compiler.optimize.pie.*>' to build specific target binary"
	@exit 1

########################################################################
# Maintenance
real-clean:
	rm results/*

.PRECIOUS: results/%pie.elf results/%.strip.elf results/%.nostrip.elf results/%.errno results/%.multiverse.rewritten.errno results/%.ddisasm.gtirb results/%.ddisasm.afl results/%.egalito.afl results/%.retrowrite.S results/%.afl results/%.zipr.afl results/%.uroboros.S results/%.uroboros.afl results/%.multiverse.S results/%.multiverse.afl results/%.mctoll.ll results/%.mcsema.bc results/%.mcsema.bc results/%.rewritten results/%.zipr.rewritten
