#!/bin/bash
#
# Usage: check.sh [options]
#  Check evaluation binaries.
#
# OPTIONS:
#   -k ----------------- keep going after finding an error
#   -d NAME ------------ directory name
#                        (default: results)
#   -c CHECKS ---------- ,-delimited list of checks
#                        (default: pie,no-pie,errno,dups)
#
SCRIPT="$0"
help(){
    local HELP_TEXT=$(cat "$SCRIPT" \
        |sed '/^[^#]/q' \
        |tail -n +3 \
        |sed -e :a -e '/^\n*$/{$d;N;ba' -e '}' \
        |sed '$ d' \
        |cut -c3-)
    echo "$HELP_TEXT"
    exit 1
}

EXIT=0
unset KEEP
DIRECTORY=results
CHECKS=pie,no-pie,errno

while [ $# -gt 0 ];do
    case $1 in
        -h|--help) help;;
        -k) KEEP=yes;;
        -d) shift; DIRECTORY=$1;;
        -c) shift; CHECKS=$1;;
        (--) shift; break;;
        (-*) echo "unrecognized option $1";exit 1;;
        (*)  break;;
    esac
    shift
done

function report(){
    echo "$1"
    EXIT=2
    if [ -z $KEEP ];then exit $EXIT;fi
}

function check_pie(){
    echo "========================================================================"
    echo "== PIE"
    echo "========================================================================"
    for elf in ${DIRECTORY}/*.pie.*elf;do
        if ! $(readelf -h $elf|grep Type|grep -qw DYN);then
            report "$elf is not PIE"
        fi
    done
}

function check_nopie(){
    echo "========================================================================"
    echo "== no-PIE"
    echo "========================================================================"
    for elf in ${DIRECTORY}/*.nopie.*elf;do
        if ! $(readelf -h $elf|grep Type|grep -qw EXEC);then
            report "$elf is PIE"
        fi
    done
}

function check_errno(){
    echo "========================================================================"
    echo "== Functionality of the originals"
    echo "========================================================================"
    for elf in ${DIRECTORY}/*.elf;do
        BASE=${DIRECTORY}/$(basename $elf .elf)
        if [ -f $BASE.errno ];then
            if ! $(grep -q "^0$" $BASE.errno);then
                report "$BASE.elf has bad exit $(cat $BASE.errno)"
            fi
        else
            report "$BASE.errno does not exist"
        fi;
    done
}

function check_dups(){
    echo "========================================================================"
    echo "== Duplicates"
    echo "========================================================================"
    duplicates=$(md5sum ${DIRECTORY}/*|awk '{ if(byhash[$1]){ byhash[$1] = byhash[$1] "," $2 } else { byhash[$1] = $2}} END {for (key in byhash) { print key, byhash[key] }}')
    if $(echo "$duplicates"|grep -q ,);then
        report "$(echo "$duplicates"|grep ,|sed 's/,/ /')"
    fi
}

for check in $(echo "$CHECKS"|tr ',' ' ');do
    case $check in
        no-pie) check_nopie;;
        pie) check_pie;;
        errno) check_errno;;
        dups) check_dups;;
    esac
done

exit $EXIT
