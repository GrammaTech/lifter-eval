#!/usr/bin/env python3
import argparse
from bisect import bisect_right
import glob
import os
import sys
from datetime import date
from enum import Enum
from pathlib import Path
from subprocess import PIPE, Popen
from typing import List, Tuple, Type

sys.path.append(str(Path(__file__).absolute().parent.parent / "src"))
from lifter_eval.subject import all_subjects
from lifter_eval.cli import enum_help
from lifter_eval.make import collect_targets
from lifter_eval.parallelize import print_cmd
from lifter_eval.target import binary_base, binary_name
from lifter_eval.traits import ISA, OS, Spec


def _enum_option_kw(type: Type[Enum]):
    return dict(default=type, nargs="*", help=enum_help(type), type=type)


def _parse_args():
    opts = argparse.ArgumentParser(description="Archive to a tarball")
    opts.add_argument("--isa", **_enum_option_kw(ISA))
    opts.add_argument("--os", **_enum_option_kw(OS))
    opts.add_argument(
        "--from-dir", default="results", help="from directory ('%(default)s')"
    )
    opts.add_argument(
        "--to-dir",
        default="/u4/TARBALLS/lifter-eval",
        help="to directory ('%(default)s')",
    )
    opts.add_argument(
        "tool",
        nargs="?",
        help="Tool results to archive (e.g. 'ddisasm'). If not specified "
        "then archive the original binaries",
    )
    return opts.parse_args()


def archive(opts):
    for isa in opts.isa:
        for os in opts.os:
            _archive_isa_os(isa, os, opts)


def _find_by_prefixes(dir: Path, prefixes: List[str]) -> Tuple[List[Path], List[str]]:
    prefixes = sorted(prefixes)
    files_per_prefix: List[List[Path]] = [[] for i in range(len(prefixes))]
    assert len(prefixes) == len(files_per_prefix)
    for p in dir.iterdir():
        name = p.name
        i = bisect_right(prefixes, name)
        if i != 0:
            prefix = prefixes[i - 1]
            if name.startswith(prefix):
                files_per_prefix[i - 1].append(p)

    files = []
    missed = []
    for i, pfiles in enumerate(files_per_prefix):
        if pfiles:
            files.extend(pfiles)
        else:
            missed.append(prefixes[i])
    return files, missed


def _find_by_patterns(from_dir: Path, patterns: List[str]):
    files: List[Path] = []
    missed: List[str] = []
    for p in [str(from_dir / p) for p in patterns]:
        matched = glob.glob(p)
        if matched:
            files.extend(map(Path, matched))
        else:
            missed.append(p)
    return files, missed


def _archive_isa_os(isa: ISA, os_: OS, opts):
    spec = Spec([isa], [os_])
    targets = collect_targets(all_subjects(), spec)[0]
    if opts.tool:
        prefixes = [f"{binary_base(t)}.{opts.tool}." for t in targets]
        patterns: List[str] = []
    else:
        prefixes = [binary_name(t) for t in targets]
        patterns = [f"{isa.value}.{os_.value}.*.deps"]
        if isa == ISA.x64 and os == OS.ubuntu20:
            patterns += [
                f"{d}/**/*"
                for d in [
                    "chromium.clang.Ostar.pie.nostrip",
                    "firefox.gcc.Ostar.nopie.nostrip",
                ]
            ]

    print(f"collecting files...", flush=True)
    from_dir = Path(opts.from_dir)
    prefix_files, missed_prefixes = _find_by_prefixes(from_dir, prefixes)
    pattern_files, missed_patterns = _find_by_patterns(from_dir, patterns)
    files = prefix_files + pattern_files
    missed = [x + "*" for x in missed_prefixes] + missed_patterns
    if missed:
        print("some of the expected files were not found:")
        for p in missed:
            print(p)
        raise Exception(f"please make sure all the files are present")
    if len(files) == 0:
        raise Exception(f"cannot find any files to archive")
    print(f"found {len(files)} files")

    archive_path = Path(opts.to_dir) / _arch_name(isa, os_, opts.tool, date.today())
    print(f"archiving to {archive_path}")
    _archive_files(isa, files, archive_path)
    _update_current_link(isa, os_, opts.tool, archive_path)


def _archive_files(isa, files, archive):
    with open(archive, "wb") as tar_xz:
        with _run_tar(files) as tar, _run_xz(isa, tar.stdout, tar_xz) as xz:
            _wait_for_exit_code(tar, "tar")
            _wait_for_exit_code(xz, "xz")


def _arch_name(isa: ISA, os_: OS, tool, timestamp=None):
    result = f"results.{isa.value}.{os_.value}"
    if tool:
        result += f".{tool}"
    if timestamp:
        result += f".{timestamp}"
    return result + ".tar.xz"


def _update_current_link(isa: ISA, os_: OS, tool, archive):
    link = os.path.join(os.path.dirname(archive), _arch_name(isa, os_, tool))
    if os.path.islink(link):
        print(f"the link already exists, updating it: {link}")
        os.unlink(link)

    print(f"link {archive} -> {link}")
    os.symlink(os.path.basename(archive), link)


def _wait_for_exit_code(proc, desc):
    exit_code = proc.wait()
    if exit_code:
        raise Exception(f"{desc} exited with non-zero exit code: {exit_code}")


def _run_tar(files):
    tar_common_args = ["tar", "-cvf", "-"]
    print_cmd(tar_common_args + [f"<{len(files)} files>"])
    return Popen(tar_common_args + files, stdout=PIPE)


def _run_xz(isa: ISA, input, output):
    args = ["xz", "-T0"]
    if isa in [ISA.x64, ISA.x86]:
        args += ["--x86", "--lzma2"]
    elif isa in [ISA.arm64, ISA.arm]:
        args += ["--arm", "--lzma2"]
    print_cmd(args)
    return Popen(args, stdin=input, stdout=output)


if __name__ == "__main__":
    opts = _parse_args()
    archive(opts)
    print("OK!")
