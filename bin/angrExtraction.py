import angr, monkeyhex, cle
import argparse, json, sys, os
from Ghidra.ghidraExtraction import dumpCodeDataResult

def angrSymbolExtract(file_name):
    symbol_info= {}
    _cle = cle.loader.Loader(file_name)
    symbols = _cle.symbols
    
    proj = angr.Project(file_name)
    obj = proj.loader.main_object
    
    for symbol in symbols:        
        symbol_info[symbol.rebased_addr] = {"Address": hex(symbol.rebased_addr),
                                       "References": [],
                                        "Names": [symbol.name]
                                    }
                                    
    for i in range(obj.min_addr, obj.max_addr):
        
        try:
            block = proj.factory.block(i)
        except angr.errors.SimEngineError:
            continue 

        for j in block.disassembly.insns:
            operands = j.op_str.split(" ")
            for operand in operands:
                if(operand in symbol_info):
                    if(hex(i.address) not in symbol_info[operand]["References"]):
                        symbol_info[operand]["References"].append(hex(i.address))
    return symbol_info;

def angrFunctionBounds(file_name):
    proj = angr.Project(file_name)
    idfer = proj.analyses.CFGFast()
    function_bounds = {}
    symbol_info = []
    
    for key in idfer.kb.functions:
        func = idfer.kb.functions[key]
        addresses = []
        for block in func.blocks:
            try:
                addresses.append({"Start_address" : block.instruction_addrs[0],
                    "End_address" : block.instruction_addrs[-1]})
            except angr.errors.SimEngineError:
                continue
        function_bounds[func.name] = {
            "Name" : func.name,
            "Addresses" : addresses 
        }
    return function_bounds

def dataBlock(block):
    #Return true if block is a data block
    for i in block.disassembly.insns:
        if("byte" in i.op_str):
            return True
    return False
        
def angrCodeDataExtract(file_name, data, code):
    proj = angr.Project(file_name)
    obj = proj.loader.main_object
    for i in range(obj.min_addr, obj.max_addr):
        try:
            block = proj.factory.block(i)
        except angr.errors.SimEngineError:
            continue 
        if(dataBlock(block)):
            data.extend(block.instruction_addrs)
        else:
            code.extend(block.instruction_addrs)
                    
def main():
    data = []
    code = []

    parser = argparse.ArgumentParser(description='Given the dissambeler and assembly file get a file with addresses.')
                                       
    parser.add_argument('binary_file_name', type=str,
                    help='Binary File Path')
                    
    parser.add_argument('mode', type=str,
                    help='function or code mode where function gives function boundaries, code gives code Data info, symbols gives symbol info')

    parser.add_argument('txt_file_name', type=str, nargs='?',
                    help='Text File to store information',
                    default = None)
                    
    args = parser.parse_args()
    if(not os.path.exists(args.binary_file_name[0])):
        raise FileNotFoundError("Binary file not found") 
           
    if((args.mode).lower()=="function"):
        function_bounds = angrFunctionBounds(args.binary_file_name)
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                file.write(json.dumps(function_bounds))
        else:
            sys.stdout.write(json.dumps(function_bounds))
    if((args.mode).lower()=="code"):
        angrCodeDataExtract(args.binary_file_name, data, code)
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                dumpCodeDataResult(data, code, file);
        else:
            dumpCodeDataResult(data, code, sys.stdout);
    elif((args.mode).lower()=="symbols"):
        symbol_info = angrSymbolExtract(args.binary_file_name) 
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                file.write(json.dumps(symbol_info))
        else:
            sys.stdout.write(json.dumps(symbol_info))
    else:
        raise ValueError("Mode not supported")

if __name__ == '__main__':
    main()   
