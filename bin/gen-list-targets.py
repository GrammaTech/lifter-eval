#!/usr/bin/env python3
from lifter_eval.make import collect_targets
from lifter_eval.subject import all_subjects
from lifter_eval.target import binary_name
from lifter_eval.traits import Compiler, ISA, LLVMObfuscation, Optimization, OS, Spec


def dump_targets(file_name, isa: ISA, os: OS, compilers, obfuscations=None):
    optimizations = [o for o in Optimization] if obfuscations is None else None
    spec = Spec(
        [isa], [os], compilers, optimizations=optimizations, obfuscations=obfuscations
    )
    targets = collect_targets(all_subjects(), spec)[0]
    print(f"generating '{file_name}'...")
    with open(file_name, "w") as f:
        for t in targets:
            print(binary_name(t), file=f)


dump_targets("list-targets.txt", ISA.x64, OS.ubuntu20, [Compiler.gcc, Compiler.clang])
dump_targets("list-targets-icx.txt", ISA.x64, OS.ubuntu20, [Compiler.icx])
dump_targets(
    "list-targets-16.txt", ISA.x64, OS.ubuntu16, [Compiler.gcc, Compiler.clang]
)
dump_targets(
    "list-targets-ollvm.txt", ISA.x64, OS.ubuntu20, [Compiler.clang], LLVMObfuscation
)
dump_targets(
    "list-targets-arm64.txt", ISA.arm64, OS.ubuntu20, [Compiler.gcc, Compiler.clang]
)
dump_targets(
    "list-targets-x86.txt", ISA.x86, OS.ubuntu20, [Compiler.gcc, Compiler.clang]
)
dump_targets("list-targets-gfortran.txt", ISA.x64, OS.ubuntu20, [Compiler.gfortran])
print("OK!")
