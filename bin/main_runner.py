import os
import subprocess
import sys

repo_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def path_in_repo(*path_components):
    return os.path.join(repo_dir, *path_components)

binaries_path = path_in_repo("binaries")

result_dir = path_in_repo("results")
storage_dir = path_in_repo("storage")

angr_script = path_in_repo("bin", "angrExtraction.py")

ghidra_script_dir = path_in_repo("bin", "Ghidra")
analyzeHeadlessDir = "/ghidra_10.1.1_PUBLIC/support/analyzeHeadless"

retroCommand = "/retrowrite/retrowrite"
retro_code_data_script = path_in_repo("bin", "asm", "asmToCodeData.py")
retro_symbol_script = path_in_repo("bin", "asm", "asmToSymbol.py")
retro_function_script = path_in_repo("bin", "asm", "asmToFunctionData.py")

ddisasm_code_data_script = path_in_repo("bin", "asm", "asmToCodeData.py")
ddisasm_symbol_script = path_in_repo("bin", "asm", "asmToSymbol.py")
gtirb_script = path_in_repo("bin", "gtirbExtraction.py")

import os

def run_python(args):    
    subprocess.check_call([sys.executable] + args)

def runAngrCommand(bin_location, mode, output_file_name):    
    run_python([angr_script, bin_location, mode, output_file_name])
    
def angrInformation(_file):
    #Get Symbol Information
    runAngrCommand( _file, "symbols", os.path.join(result_dir, os.path.basename(_file)+".angr.symbol.json"))
    #Get Code Data Information
    runAngrCommand( _file, "code", os.path.join(result_dir, os.path.basename(_file)+".angr.code_data.json"))
    #Get Function Bound Information
    runAngrCommand( _file, "function", os.path.join(result_dir, os.path.basename(_file)+".angr.function.json"))

def runGhidraCommand(bin_location, mode, file_name):    
    subprocess.check_call([
        analyzeHeadlessDir, "/ghidra_project", "ghidra_project_name",
        "-scriptPath", ghidra_script_dir,
        "-import", bin_location,
        "-overwrite", "-postScript", "ghidraExtraction.py", mode, file_name])
    
def ghidraInformation(_file):
    #Get Symbol Information
    runGhidraCommand(_file, "symbols", os.path.join(result_dir, os.path.basename(_file)+".ghidra.symbols.json"))
    #Get Code Data Information
    runGhidraCommand(_file, "code", os.path.join(result_dir, os.path.basename(_file)+".ghidra.code_data.json"))
    #Get Function Bound Information
    runGhidraCommand(_file, "function", os.path.join(result_dir, os.path.basename(_file)+".ghidra.function.json"))
    
def getRetroAsm(_file):
    #Get the asm via retrowrite
    asm_file = os.path.join(storage_dir, os.path.basename(_file)+".retro.s")
    subprocess.check_call([retroCommand, "--ignore-no-pie", "-s", _file, asm_file])
    return asm_file
  
def retroInformation(_file):
    asm_file = getRetroAsm(_file)
    base_path = os.path.join(result_dir, os.path.basename(_file))
    #Get Code Data Information
    run_python([retro_code_data_script, "retro", asm_file, base_path + ".retro.code_data.json"])
    #Get Symbol Data
    run_python([retro_symbol_script, "retro", asm_file, base_path + ".retro.symbol.json"])
    #Get Function Data
    run_python([retro_function_script, "retro", asm_file, base_path + ".retro.symbol.json"])

def getDdisasmAsm(_file):
    #Get the ddisasm and gtirb files via ddisasm
    gtirb_file = os.path.join(storage_dir, os.path.basename(_file) + ".gtirb")
    asm_file = os.path.join(storage_dir, os.path.basename(_file) + ".ddisasm.s")
    subprocess.check_call(["ddisasm", _file, "--ir", gtirb_file, "--asm", asm_file, "--debug"])
    return (gtirb_file, asm_file)
    
def ddisasmInformation(_file):
    (gtirb_file, asm_file) = getDdisasmAsm(_file)
    base_path = os.path.join(result_dir, os.path.basename(_file))
    #Get Code Data Information
    run_python([ddisasm_code_data_script, "ddisasm", asm_file, base_path + ".ddisasm.code_data.json"])
    #Get Symbol Data
    run_python([ddisasm_symbol_script, "ddisasm", asm_file, base_path + ".ddisasm.symbol.json"])
    #Use Gtirb to find function bound information
    gtirbInformation(gtirb_file, _file)

def runGtirbCommand(bin_location, mode, output_file_name):    
    run_python([gtirb_script, bin_location, mode, output_file_name]) 
    
def gtirbInformation(gtirb_file, _file):
    #Get Function Data
    runGtirbCommand(gtirb_file, "function", result_dir +"/"+os.path.basename(_file)+".gtirb.function.json")
    #Get Symbol Data
    runGtirbCommand(gtirb_file, "symbol", result_dir +"/"+os.path.basename(_file)+".gtirb.symbol.json")
    
def runner(_file):
    angrInformation(_file)
    ghidraInformation(_file)
    retroInformation(_file)
    ddisasmInformation(_file)


def setup():
    os.makedirs(result_dir, exist_ok=True)
    os.makedirs("/ghidra_project", exist_ok=True)
    os.makedirs(storage_dir, exist_ok=True)
    
def main():    
    setup()
    files = []
    for (dirpath, dirnames, filenames) in os.walk(binaries_path):
        for f in filenames:
            files.append(dirpath + "/" + f)
    
    for _file in files:
        runner(_file)
    return 0
    
if __name__ == '__main__':
    main()
