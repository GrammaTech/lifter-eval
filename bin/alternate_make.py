#!/usr/bin/env python3
import argparse
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent / "src"))
from lifter_eval.cli import (
    add_target_options,
    add_docker_options,
    combined_subjects,
    docker_options,
)
from lifter_eval.make import MakeOptions, make
from lifter_eval.traits import Spec
from lifter_eval.parallelize import ProcessScheduler


def _parse_args():
    parser = argparse.ArgumentParser(description="Run make")
    add_docker_options(parser)
    parser.add_argument(
        "--max-containers",
        type=int,
        metavar="N",
        default=1,
        help="run up to N docker containers in parallel, " "default: %(default)s",
    )
    parser.add_argument(
        "--batch",
        action="store_true",
        default=False,
        help="build multiple binaries within the same" " docker container",
    )
    parser.add_argument(
        "--ignore-batch-errors",
        action="store_true",
        default=False,
        help="keep going even if a particular target in --batch fails",
    )
    parser.add_argument(
        "--build-images",
        action="store_true",
        default=False,
        help="rebbuild required docker images",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="only print what is going to be run",
    )
    parser.add_argument(
        "--always-make",
        action="store_true",
        default=False,
        help="tell make to disregard timestamps and make the specified target",
    )
    parser.add_argument(
        "--single-target",
        action="store_true",
        default=False,
        help="make only one target per subject in a sandbox (useful for initializing sandbox volumes)",
    )
    add_target_options(parser)

    result = parser.parse_args()
    if result.ignore_batch_errors and not result.batch:
        parser.error('"--ignore-batch-errors" can be used only with "--batch".')
    return result


def main():
    args = _parse_args()
    spec = Spec(
        args.isa,
        args.os,
        args.compiler,
        args.pie,
        args.optimize,
        args.obfuscate,
        args.strip,
    )
    docker_opts = docker_options(args)
    ps = ProcessScheduler(args.max_containers) if not args.dry_run else None
    make_opts = MakeOptions(
        args.always_make,
        args.batch,
        args.ignore_batch_errors,
        args.build_images,
        args.single_target,
    )
    make(combined_subjects(args), spec, args.target_ext, ps, make_opts, docker_opts)


if __name__ == "__main__":
    main()
