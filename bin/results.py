#!/usr/bin/env python3
import argparse
from pathlib import Path
import sys

sys.path.append(str(Path(__file__).absolute().parent.parent / "src"))
from lifter_eval.cli import add_target_options, combined_subjects
from lifter_eval.docker import HOST_RESULTS
from lifter_eval.results import csv, stat, summary
from lifter_eval.traits import Spec


def _parse_cmd_line(args=None):
    parser = argparse.ArgumentParser(description="Collect results")
    parser.add_argument("--csv", action="store_true", help="generate CSV")
    parser.add_argument(
        "--isa-os-summary", action="store_true", help="summary per ISA/OS"
    )
    parser.add_argument(
        "--results-dir",
        default=HOST_RESULTS,
        help="look for the results in this directory, default is '%(default)s'",
    )
    add_target_options(parser)
    result = parser.parse_args(args)
    if result.isa_os_summary and result.csv:
        parser.error('"--isa-os-summary" cannot be combined with "--csv".')
    if len(result.target_ext) == 0 and not result.csv:
        parser.error('"target_ext" is required when --csv is not set.')
    return result


if __name__ == "__main__":
    opts = _parse_cmd_line()
    spec = Spec(
        opts.isa,
        opts.os,
        opts.compiler,
        opts.pie,
        opts.optimize,
        opts.obfuscate,
        opts.strip,
    )
    results_dir = Path(opts.results_dir)
    subjects = combined_subjects(opts)
    if opts.csv:
        csv(subjects, opts.target_ext, spec, results_dir)
    elif opts.isa_os_summary:
        summary(subjects, opts.target_ext, spec, results_dir)
    else:
        stat(subjects, opts.target_ext, spec, results_dir)
