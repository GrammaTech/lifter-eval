#!/bin/bash
#
# Usage: list.sh [OPTIONS] TOOL [TOOL...]
#  List the results of TOOLs against lifted binaries
#
# OPTIONS:
#   -a ----------------- count successful lifts to asm
#   -e ----------------- count successful new executables
#   -E ----------------- count successful new AFL executables
#   -s ----------------- change in file size
#   -d ----------------- count of different instructions
#   -p ----------------- tool runtime and max memory
#   -f ----------------- count fully functional binaries
#   -F ----------------- count fully functional AFL binaries
#   -d NAME ------------ directory name
#                        (default: results)
#
SCRIPT="$0"
help(){
    local HELP_TEXT=$(cat "$SCRIPT" \
        |sed '/^[^#]/q' \
        |tail -n +3 \
        |sed -e :a -e '/^\n*$/{$d;N;ba' -e '}' \
        |sed '$ d' \
        |cut -c3-)
    echo "$HELP_TEXT"
    exit 1
}

declare -a TYPE
DIRECTORY=results

eval set -- $(getopt -o hieEsapfFd: -- "$@") || help

while [ $# -gt 0 ];do
    case $1 in
        -h) help;;
        -a) TYPE+=( asm );;
        -e) TYPE+=( exe );;
        -E) TYPE+=( afl_exe );;
        -s) TYPE+=( size );;
        -d) TYPE+=( diff );;
        -p) TYPE+=( perf );;
        -f) TYPE+=( functional );;
        -F) TYPE+=( afl_functional );;
        -d) shift; DIRECTORY=$1;;
        (--) shift; break;;
        (-*) error "unrecognized option $1";;
        (*)  break;;
    esac
    shift
done
if [ -z $1 ];then
    help
fi

if [ -z $TYPE ];then
    TYPE+=asm
fi

declare -a TOOLS
TOOLS=$@

function exists(){
    [ -f $1 ] && echo "✓"
}

function size(){
    [ -f $1 ] || return 1
    [ -f $2 ] || return 1
    local original_size=$(du -L $1|awk '{print $1}')
    local rewritten_size=$(du -L $2|awk '{print $1}')
    echo "($rewritten_size / $original_size)*100"|bc -l|cut -c-8
}

function diff(){
    [ -f $1 ] || return 1
    [ -f $2 ] || return 1
    # Get difference, remove colors, only diff lines, remove nops, count
    ./asm-diff -n -d " " -j .text -C $1 $2 -- \
        |grep "^\(<\|>\)" \
        |grep -vw "nop.*"|wc -l
}

function perf(){
    [ -f $1 ] || return 1
    local time=$1
    local memory=$(grep -i "Maximum resident set size" $time |awk '{print $6}')
    local user=$(grep -i "User time" $time|awk '{print $4}')
    local system=$(grep -i "System time" $time|awk '{print $4}')
    echo "$(echo "$user + $system"|bc -l)|$memory"
}

function functional(){
    [ -f $1 ] || return 1
    cat $1
}

echo "Binary ${TOOLS}"|tr ' ' '\t'
for original in ${DIRECTORY}/*.{nostrip,strip}.elf;do
    base=$(basename $original .elf)
    echo -ne "$base\t"
    for tool in $TOOLS;do
        rewritten=${DIRECTORY}/$base.$tool.rewritten
        LINE=
        for TEST in ${TYPE[@]};do
            case $TEST in
                asm) VALUE=$(exists ${DIRECTORY}/$base.$tool.S || exists ${DIRECTORY}/$base.$tool.ll || echo "∅");;
                exe) VALUE=$(exists $rewritten || echo "∅");;
                afl_exe) VALUE=$(exists ${DIRECTORY}/$base.$tool.afl || echo "∅");;
                size) VALUE=$(size $original $rewritten || echo "∅");;
                diff) VALUE=$(diff $original $rewritten || echo "∅");;
                perf) VALUE=$(perf ${DIRECTORY}/$base.$tool.*.time || echo "∅");;
                functional) VALUE=$(functional ${DIRECTORY}/$base.$tool.rewritten.errno || echo "∅");;
                afl_functional) VALUE=$(functional ${DIRECTORY}/$base.$tool.afl.errno || echo "∅");;
                *) echo "BAD VALUE: $TEST" >&2
            esac
            if [ -z $LINE ];then
                LINE=$VALUE
            else
                LINE="$LINE,$VALUE"
            fi
        done
        echo -ne "$LINE\t"
    done
    echo ""
done
