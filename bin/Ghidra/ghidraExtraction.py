import argparse
import json
import sys

def truncate(og_data):
    if(len(og_data) <= 1):
        return og_data
    data = sorted(og_data)
    start = data[0]
    next_address = start+1
    tmp = []
    for i in range(1,len(data)):
        if(data[i] != next_address):
            if(start != data[i-1]):
                tmp.append(hex(start) + "-" + hex(data[i-1]))
            else:
                tmp.append(hex(data[i-1]))
            start = data[i]
        next_address = data[i]+1
    if(start != data[-1]):
        tmp.append(hex(start) + "-" + hex(data[-1]))
    else:
        tmp.append(hex(data[-1]))
    return tmp

def argumentSetup():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('mode', type=str,
                    help='function or code mode where function gives function boundaries, code gives code Data info, symbols gives symbol info')
     
    parser.add_argument('txt_file_name', type=str, nargs="?",
            help='Text file to store information',
            default=None)

    args = parser.parse_args(args = getScriptArgs())
    return args

def dumpFuncResult(data, file = sys.stdout):
    for key in data:
        storage = {
        "Name: " : key,
        "Address_Range: " : data[key]
        }
        file.write(json.dumps(storage))
        
def dumpCodeDataResult(data, code, file = sys.stdout):
    storage = {
        "Data_Addresses" : truncate(data),
        "Code_Addresses": truncate(code)
    }
    file.write(json.dumps(storage))

def sortBlock(start_addr, end_addr, funcs, code, data ):
    addr = start_addr
    
    while(addr <= end_addr):
        try:
            address = int(addr.toString(), 16)
        except:
            break
            
        #Function Data    
        func = getFunctionContaining(addr)
        if func:
            funcs[func.getName()].append(address)
           
        #Code Data
        if(getInstructionAt(addr)):
            code.append(address)
        if(getDataAt(addr)):
            data.append(address)
            
        addr = addr.next() 

def getSymbolInfo():
    prog = getCurrentProgram()
    symbolTable = prog.getSymbolTable()
    symbols = symbolTable.getAllSymbols(True)
    
    storage = {}
     
    for symbol in symbols:
        tmp = []
        if(symbol.hasReferences()):
            for ref in symbol.getReferences():
                tmp.append(ref.getFromAddress().toString())

        storage[symbol.getName()] = {
            "Name" : symbol.getName(),
            "Address" : symbol.getAddress().toString(),
            "References" : tmp
        }
    return storage
        

def main():
    args = argumentSetup()

    func = getFirstFunction()
    funcs = {}
    
    data = []
    code = []
    
   # Create a dictionary where the name of the function is they key
    while func:
        funcs[func.getName()] = []
        func = getFunctionAfter(func)

    # Get addresses from memeoryblocks and determine where which function they belong to
    # Done this way b/c there is no explicit way to determine the end address of a function
    for block in getMemoryBlocks():
        sortBlock(block.getStart(), block.getEnd(), funcs, data, code)

    for key in funcs:
        funcs[key] = truncate(funcs[key])

    if((args.mode).lower()=="function"):
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                dumpFuncResult(funcs, file);
        else:
            dumpFuncResult(funcs, sys.stdout);
    elif((args.mode).lower()=="code"):
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                dumpCodeDataResult(data, code, file);
        else:
            dumpCodeDataResult(data, code, sys.stdout);
            
    elif((args.mode).lower()=="symbols"):
        symbol_info = dict(getSymbolInfo()) 
        if(args.txt_file_name):
            with open(args.txt_file_name, "w") as file:
                file.write(json.dumps(symbol_info))
        else:
            sys.stdout.write(json.dumps(symbol_info))            

if __name__ == '__main__':
    main()   
