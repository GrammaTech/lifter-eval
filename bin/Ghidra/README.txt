Ghidra ReadMe Notes

To run the script and get the necessary data from the binary the command 
$ ./analyzeHeadless ${Project-directory} ${Project-name} -scriptPath ${script-dir} -import ${Binary-location} -overwrite -postScript ghidraExtraction.py ${mode} ${file_name}

Where:
${Project-directory} is the directory where the "project" for your ghidra analyzing can go
${Project-name} is the name of the project
${script-dir} Directory of the script (something along the lines lifter-eval/bin/... )
${Binary-location} the path including the name of the binary
${mode} Either function or code (Function gets function boundaries, code gets code/data information)
${file_name} is the place/name where you want the results of the script to be stored

Command is ran from within the ghidra directory ghidra-directory/support/


More information of the analyzeHeadless command can be found within the ghidra-directory/support/analyzeREADME 
