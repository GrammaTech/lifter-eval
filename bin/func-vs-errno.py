#!/usr/bin/env python3
from lifter_eval.results import subject_stat
from lifter_eval.traits import ISA, Compiler, Spec

subjects = ["lighttpd", "nginx", "redis"]

tools = [
    ".elf",
    ".ddisasm.rewritten",
    ".dyninst.rewritten",
    ".egalito.rewritten",
    ".e9patch.rewritten",
    ".multiverse.rewritten",
    ".reopt.rewritten",
    ".retrowrite.rewritten",
    ".uroboros.rewritten",
    ".zipr.rewritten",
]

exts = [".func", ".errno"]

spec = Spec(isas=[ISA.x64], compilers=[Compiler.clang, Compiler.gcc])


def format_rate(rates):
    return "/".join(rates)


def print_row(tool, rates):
    print(f"| {tool:<{30}}| {rates:<13}|")


def main():
    print_row("tool", format_rate(exts))
    print_row("---", "---")
    for s in subjects:
        for t in tools:
            stats = (subject_stat([s], t + r, spec) for r in exts)
            print_row(f"{s}{t}", format_rate(str(len(s[0])) for s in stats))


if __name__ == "__main__":
    main()
