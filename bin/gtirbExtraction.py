import argparse
import json, sys, os
import gtirb
from gtirb_capstone.instructions import (
            GtirbInstructionDecoder,
                AccessType,
                )
                
#Function Boundaries
def functionBoundaries(ir):
    function_bounds = {}
    for mod in ir.modules:
        function_block_info = mod.aux_data["functionBlocks"]
        function_block_name = mod.aux_data["functionNames"]
        for function_block_uuid in function_block_info.data:
            addresses = []
            function_name = function_block_name.data[function_block_uuid].name
            for block in function_block_info.data[function_block_uuid] :
                addresses.append({"Start_address": block.address,
                    "End_address" : block.address+block.size})
            function_bounds[function_name] = {
                "Name": function_name,
                "Addresses": addresses
            }
    return function_bounds

#Symbols
def symbolInfo(ir):
    symbol_info = {}
    for symbol in ir.symbols:
        if(symbol.referent):
            try:
                symbol_address = hex(symbol.referent.address)
                if(symbol_address in symbol_info):
                    symbol_info[symbol_address]["Names"].append(symbol.name)
                else:
                    symbol_info[symbol_address] = {"Address": symbol_address,
                        "References": [],
                        "Names": [symbol.name]
                    }
            except AttributeError:
                continue
    decoder = GtirbInstructionDecoder(gtirb.Module.ISA.X64)
    for code_block in ir.code_blocks:
        insns = list(decoder.get_instructions(code_block))
        for insn in insns:
            operands = insn.op_str.split(" ")
            for operand in operands:
                if(operand in symbol_info):
                    symbol_info[operand]["References"].append(hex(insn.address))
    return symbol_info

def main():
    parser = argparse.ArgumentParser(description='Given the gtirb file and the mode get the information.')
                                    
    parser.add_argument('gtirb_file_name', type=str,
                    help='Gtirb File Path')
                    
    parser.add_argument('mode', type=str,
                    help='Function or Symbol information')
                    
    parser.add_argument('txt_file_name', type=str, nargs= "?",
                    help='Text File to store information',
                    default = None)
                    
    args = parser.parse_args()
    if(not os.path.exists(args.gtirb_file_name)):
        raise FileNotFoundError("Gtirb file not found")
        
    ir = gtirb.ir.IR.load_protobuf(args.gtirb_file_name)
    
    if((args.mode).lower()=="function"):
        storage_info = functionBoundaries(ir)
    elif((args.mode).lower()=="symbol"):
        storage_info = symbolInfo(ir)
    else:
        raise Exception("Mode not supported")
        
    if(args.txt_file_name):
        with open(args.txt_file_name, "w") as file:
            file.write(json.dumps(storage_info))
    else:
        sys.stdout.write(json.dumps(storage_info))


if __name__ == '__main__':
    main()
                  
