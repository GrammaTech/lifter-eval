#!/bin/bash
#
# Print summary results.
#

function mean(){
    jq -s add/length
}

function stdev(){
    jq -s '(add/length)as$a|map(pow(.-$a;2))|add/length|sqrt'
}

function system_time(){
    local tool=$1
    grep -i "System time" results/*$tool.S.time|awk '{print $5}'
}

function user_time(){
    local tool=$1
    grep -i "User time" results/*$tool.S.time|awk '{print $5}'
}

function memory(){
    local tool=$1
    grep -i "Maximum resident set size" results/*$tool.S.time|awk '{print $7}'
}

function success(){
    local tool=$1
    grep "Exit status:" results/*$tool.S.time|awk '{print $4}'|sort|uniq -c\
        |grep " 0$"|awk '{print $1}'
}

function code_size(){
    local tool=$1
    cat results/*.$tool.rewritten.size|grep ".text"|cut -d, -f3
}

function data_size(){
    local tool=$1
    cat results/*.$tool.rewritten.size|grep ".data"|cut -d, -f3
}

function summary_header(){
    echo -e "tool\tsuccess\tuser_time\tsystem_time\tmax_memory\tcode_size\tdata_size"
}
function summary(){
    local tool=$1
    echo -ne "$tool\t"
    echo -ne "$(success $tool)\t"
    echo -ne "$(user_time $tool|mean)±$(user_time $tool|stdev)\t"
    echo -ne "$(system_time $tool|mean)±$(system_time $tool|stdev)\t"
    echo -ne "$(memory $tool|mean)±$(memory $tool|stdev)\t"
    echo -ne "$(code_size $tool|mean)±$(code_size $tool|stdev)\t"
    echo -ne "$(data_size $tool|mean)±$(data_size $tool|stdev)\t"
    echo ""
}

(summary_header
 summary ddisasm
 summary retrowrite)|column -t
