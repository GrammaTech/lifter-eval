#!/usr/bin/env python3
from pathlib import Path
from pipes import quote

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        result = ["libdb-dev"]
        if self.traits.isa == ISA.x86:
            result = [p + ":i386" for p in result]
        return result


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "postfix"
    build_subdir = Path("bin")

    smoke_args = ["--version"]
    smoke_expected_code = 1
    grep_pattern = "invalid option"
    grep_stderr = True

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        opt = quote(" ".join(self.cflags + self.ldflags))
        return f"make tidy && make makefiles CC={self.cc} OPT={opt} && make"
