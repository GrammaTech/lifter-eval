# Installing Pidgin for Windows
The tar archive contains the Pidgin installer and a folder with Pidgin binaries. The Pidgin installer is needed to install all dependecies like `MSYS2` and `GTK+`.   
After installation the binaries will be located at `C:\Program Files (x86)\Pidgin\`   
## Content of the binaries folder
The `binaries` folder contains `pidgin.exe` and number of libraries files.
When any of these binaries is updated/modified it should be copied to the Pidgin location.
# Building Pidgin for Windows
Information how to build Pidgin for Windows located here - https://pidgin.im/development/building/2.x.y/windows/ but it is outdated. It refers to the old version of Mingw and other packages. So the first steps should be to define all outdated packages and replace them with modern alternatives. For example, `mingw32` should be replaced with `mingw-w64`. The Makefile and makefile variables should be updated accordingly.

