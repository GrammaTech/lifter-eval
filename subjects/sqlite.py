#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        isa_dep = ["libz-dev", "tcl-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "sqlite"
    binary_path_in_repo = Path("sqlite3")

    smoke_expected_code = 1
    smoke_args = ["--help"]
    grep_pattern = "SQLite"
    grep_stderr = True

    evaluate = "make test"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self):
        opts = ""
        if self.traits.isa == ISA.x86:
            opts += "--with-tcl=/usr/lib/i386-linux-gnu"
        return (
        f"./configure {opts} && make sqlite3.c"
        " && $CC $CFLAGS shell.c sqlite3.c $LDFLAGS -lpthread -ldl -lm -o sqlite3"
    )
