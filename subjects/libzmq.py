#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["libtool"]
        isa_dep = [
            "libbsd-dev", "libunwind-dev"
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties):
    name = "libzmq"
    binary_path_in_repo = Path("src") / ".libs" / "libzmq.so.5.2.5"

    # first test that uses libzmq .so
    smoke_test_binary = Path("tests") / "test_abstract_ipc"

    build = (
        "./autogen.sh && ./configure --disable-Werror"
        " && make AM_DEFAULT_VERBOSITY=1 && make check"
    )

    evaluate = "make check-TESTS"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
