# Building Chromium in the Lifter Docker container
Unfortunately, the Chromium project is too complicated and cannot be prepared and built by the Lifter common way.
Here are the steps how to build Chromium
## Run the Docker container:
```
docker run --rm -it --network host -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval /bin/bash
```
## Install the Depot Tool:
```
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git /lifter-eval/subjects/depot_tools
```
## Add Depot Tools to the Path:
```
export PATH="$PATH:/lifter-eval/subjects/depot_tools"
```
## Get the code
```
mkdir /lifter-eval/subjects/chromium && cd /lifter-eval/subjects/chromium
fetch --nohooks chromium
cd src
```
## Install additional build dependencies
Before running the following command remove `sudo` from the script.
```
./build/install-build-deps.sh
```
When you are asked what to do with `snapcraft` select `3. Skip`.
## Run the hooks
```
gclient runhooks
```
## Setting up the build
Chromium uses Ninja as its main build tool along with a tool called GN to generate .ninja files. You can create any number of build directories with different configurations. To create a build directory, run:
```
gn gen out/Default
```
## Build Chromium
```
autoninja -C out/Default chrome
```
## Run Chromium
Once it is built, you can simply run the browser:
```
out/Default/chrome
```
But the X Terminal should be connected to the Docker container.   
Also the quick test can be run:
```
out/Default/chrome --version
```
## Moving Chrome binary to other location
The `chrome` binary cannot start alone. So all `lib*.so` files should be copied with the main binary.
## Setting the build options
The list of all build options can be displayed using the following command:
```
gn args --list out/Default
```
The build options can be changed in the `out/Default/args.gn` file.   
The build chain support only 2 compilers - `clang` and `gcc` and `clang` is used by default.   
Optimization level cannot be set directy and calculated for each component automatically.   
The `-no-pie` option cannot be set via build arguments.   
The stripping can be set via the `enable_stripping` argument.   
So only 2 options can be selected - the compiler and the stripping using following arguments:
```
# Set build arguments here. See `gn help buildargs`.
enable_stripping=true
is_clang=false
```
Unfortunately, setting `is_clang=false` causes compilation error. So, only 2 binaries can be built - clang with and without stripping.
## Additional information
Additional information can be found here:
- https://chromium.googlesource.com/chromium/src/+/HEAD/docs/linux/build_instructions.md
- https://gn.googlesource.com/gn/+/main/docs/quick_start.md
## Build Chromium for Windows
Detailed instructions here - https://chromium.googlesource.com/chromium/src/+/refs/heads/main/docs/windows_build_instructions.md   
GT-issued Windows VM with MSVS 2019 Pro used for building Chromium for Windows. The build took enormous amount of time - more than 24 hrs.
