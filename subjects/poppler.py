#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "libboost-dev",
        "libcairo-dev",
        "liblcms2-dev",
        "libjpeg-dev",
        "libfontconfig-dev",
        "libfreetype6-dev",
        "libgtk-3-dev",
        "libopenjp2-7-dev",
        "libtiff-dev",
        "qt5-default",
    ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "poppler"
    build_subdir = Path("build")
    binary_name = "libpoppler.so.112.0.0"
    test_repo_subdir = Path("poppler_test")

    evaluate_subdir = Path(name) / build_subdir
    evaluate = "make test"

    afl_fuzz = ""
    grep_pattern = "pdftest"

    cflags_pie = ""
    ldflags_pie = ""

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def git_submodules(self):
        return super().git_submodules + [self.test_repo_subdir]

    @property
    def build(self) -> str:
        build_dir = self.build_subdir.as_posix()
        test_dir = self.env.sandbox_path(self.test_repo_subdir).as_posix()
        return f"""\
mkdir -p {build_dir}
cd {build_dir}
cmake -DTESTDATADIR={test_dir} -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON ..
make
"""

    @property
    def smoke_test_binary(self) -> Path:
        return self.repo_dir / self.build_subdir / "test" / "perf-test"
