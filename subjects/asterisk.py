#!/usr/bin/env python3
from pathlib import Path
from typing import Dict

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler, Pie


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        isa_dep = [
            "libblocksruntime-dev",
            "libedit-dev",
            "libxml2-dev",
            "libsqlite3-dev",
            "libjansson-dev",
            "libsystemd-dev",
            "uuid-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "asterisk"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "sbin" / "asterisk"

    smoke_args = ["-V"]
    grep_pattern = "Asterisk"

    cflags_pie = ""
    ldflags_pie = ""

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build_variables(self) -> Dict[str, str]:
        result = super().build_variables
        result["MAIN_PIE_CFLAGS"] = super().cflags_pie
        result["MAIN_PIE_LDFLAGS"] = super().ldflags_pie
        if self.traits.compiler == Compiler.icx:
            result["AST_ICX_BLOCKS_LIBS"] = "-lBlocksRuntime"
        return result

    @property
    def build(self) -> str:
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        return f"./configure --prefix={prefix} NOISY_BUILD=yes && make && make install"
