#!/usr/bin/env python3
from lifter_eval.docker import OcamlDockerImage
from lifter_eval.subject_properties import OcamlProperties, SmokeTestWithGrepMixin, register


class DockerImageImpl(OcamlDockerImage):
    apt_packages = OcamlDockerImage.apt_packages + ["pkg-config"]


@register
class Properties(OcamlProperties, SmokeTestWithGrepMixin):
    name = "magic-trace"
    smoke_args = ["--help"]
    grep_pattern = "magic-trace"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
