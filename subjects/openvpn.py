#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        isa_dep = ["liblz4-dev", "liblzo2-dev", "libpam-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "openvpn"
    binary_path_in_repo = Path("src") / name / name

    smoke_args = ["--version"]
    grep_pattern = "OpenVPN"

    build = "autoreconf -fi && ./configure && make"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
