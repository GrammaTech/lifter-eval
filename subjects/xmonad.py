#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, HaskellProperties, register
from lifter_eval.docker import HaskellDockerImage


class DockerImageImpl(HaskellDockerImage):
    apt_packages = HaskellDockerImage.apt_packages + [
        "libxss1",
        "libxss-dev",
        "libxrandr-dev",
    ]


@register
class Properties(HaskellProperties, SmokeTestWithGrepMixin):
    name = "xmonad"
    smoke_args = ["--version"]
    grep_pattern = "xmonad"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
