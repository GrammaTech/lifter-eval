## Leafnode filter configuration file.
## The file format is described on the applyfilter(8) manual page.
##
## This file is only effective if included by leafnode's main
## configuration file. (Use the filterfile option.)
##
## Note: comments MUST start in the leftmost column with a "#" letter.
## You cannot add a comment at the end of a line.
##
## When using any example from below, be sure to remove the leading "# ".
##

## The following regexp matches crossposts to alt.flame:
# ^Newsgroups:.*[, ]alt.flame($|,)

## The following regexp matches messages that contain "income", "inCoMe",
## "INCOME" (and any other lower-/uppercase combination) in the Subject
## header. (?i) switches the PCRE_CASELESS option on for the remainder
## of the regular expression, which causes case insensitive matching:
# ^Subject:.*(?i)income

##{ The examples below have courteously been provided by Michael R. McCarrey.
##  Again, to enable them, remove the leading "# ".

## These two entries refer to programs that are very popular
## with spammers. Filtering them will reduce a very large number
## of spam posts.
#
# ^X-Newsreader.*NNTP.SCRIBBLER
# ^X-Newsreader:.*BNews2004v20

## Filter "User-Agent" header
# ^User-Agent:.*G2/0.2

## Filter "Path" header
# ^Path:.*bigpond.com
# ^Path:.*jaring.my

## Filter "Content" headers
## This filter will eliminate those annoying HTML posts
# ^Content-Type:.*text/html

## Filter "Subject" headers
##
## You have to "train" your filters through trial and error. Spammers and
## nasty posters have developed clever ways of circumventing filters
## which require a level of creativity on your part in construction of
## filters designed to stop their messages from reaching you.
#
## Exercize care in their constriction, because it is possible to filter
## out legitimate and useful postings. It's best to check your syslog often
## to see if you're not eliminating posts you really want to see.
#
# ^Subject:.*\$30\,000.*INCOME EVERY DAY  GUARANTEED FOR ONE YEAR
# ^Subject:.*\$30,000(?i).*GUARANTEED.*
#
# ^Subject:.*(?i)income.*
# ^Subject:.*(?i)money.*
# ^Subject:.*(?i)profit.*
# ^Subject:.*(?i)always.*wanted.*
# ^Subject:.*(?i)wanted.*always.*
# ^Subject:.*(?i)wealth.*
# ^Subject:.*(?i)money.*
# ^Subject:.*(?i)look.*at.*this.*
# ^Subject:.*(?i)stamina.*
# ^Subject:.*(?i)staying.*power.*
# ^Subject:.*(?i)Pre.*Baby Making Practice.*

## Filter "From" headers
# ^From:.*([!-@])kensmithiscool([!-@])<kensmithiscool@excite\.com>

## Filter "Organization" headers
## Filtering on this header allows one to eliminate offensive
## postings that originate from problematic organizations.
#
# ^Organization:.*http://groups\.google\.com
# ^Organization:.*UFMCC
# ^Organization:.*The Gurkian Way
# ^Organization:.*Back OFF!

## If you dislike postings that have been cross-posted to
## groups that you have no interest in, you can write
## filters to deal with that situation.
#
## Each entry begins with the "^" character, indicating the
## keyword to be matched, "Newsgroups:", is anchored at
## the left of the line.
#
## the "." and "*" match any character or series of characters
## occuring before any normal letters. These additional
## characters may or may not be present which is why the
## listed group names are pre and postfixed by ".*".
#
## Pay particular attention to the "escapes" in the different
## group names. The "." period has special meaning in regex, so
## it must be escaped when it is to be taken literally. The
## backslash, "\" preceeding it serves this purpose.
#
# ^Newsgroups:.*alt\.atheism\.holysmoke.*
# ^Newsgroups:.*alt\.religion\.scientology.*
# ^Newsgroups:.*talk\.politics\.misc.*
##}
