#ifndef MYSIGACT_H
#define MYSIGACT_H

#include "config.h"

int mysigact(int sig, int flags, void (* func)(int), int blockthis);

#endif
