.TH newsq 1 "@VERSION@" Leafnode "Leafnode user's manual" \" -*- nroff -*-
.SH NAME
newsq \- view the leafnode news queue
.SH SYNOPSIS
.B newsq
[\fB\-f\fR]
.SH "DESCRIPTION"
.B newsq
outputs a summary of news that is in leafnode's out.going queue (or
failed.postings directory, with \fB\-f\fR). The out.going queue
contains news that has been posted locally on your site and has not yet
been uploaded to the upstream news server. The failed.postings directory
contains news articles that have been rejected by upstream servers \[en]
these will not be retried automatically. You can hoewver move them to
the out.going queue manually \[en] with the \fBmv\fR(1) command \[en] to have
fetchnews retry them the next time. This is useful if the posting
failed because the upstream server had temporary difficulties.

The queue is printed in the order it is read from the disk. The output
is not explicitly sorted. (This matches GNU "ls \-U" behaviour.)
.SH "OPTIONS"
.TP
.B \-f
print the contents of the failed.postings directory instead. This option
was introduced with leafnode v1.9.20.

.SH AUTHOR
This manual page was written by Joey Hess,
for the Debian GNU/Linux system.

It was amended to by Matthias Andree.
