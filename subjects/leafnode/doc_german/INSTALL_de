0.  Seit Version 1.9.17.ma1 verwendet leafnode GNU automake.  Die
    �blichen make-targets existieren.  F�r Anpassungen am Quellcode
    wird --enable-maintainer-mode empfohlen.

1.  Seit Version 1.9 verwendet leafnode GNU autoconf, um herauszufinden,
    auf was f�r einem Rechner es installiert werden soll. Wenn man

        sh ./configure

    ausf�hrt, werden ein rechnerspezifisches Makefile und ein passendes
    config.h erstellt.
    Wenn Sie IPV6 haben und verwenden m�chten, rufen sie configure mit
    dem Parameter --with-ipv6 auf.

    Wenn Sie eine bestehende installierte Version von Leafnode updaten,
    konfigurieren Sie die neue Version so, da� sie die alte �berschreibt.
    Dies kann mit der Option --prefix von configure eingestellt werden.
    Wenn Leafnode zum Beispiel vorher in /opt installiert war, dann
    geben Sie "./configure --prefix=/opt" ein.  Mit den weiteren Optionen
    --with-spooldir, --sysconfdir und --with-lockfile k�nnen die Pfade
    zur News-Hierarchie (per default /var/spool/news), zu den Konfigura-
    tionsdateien (/etc/leafnode, wenn kein --prefix angegeben wurde,
    sonst PREFIX/etc) und zum Lockfile (per default
    leaf.node/lock.file unterhalb des "spooldir") angepa�t werden.

2.  Durch Eingabe von

        make

    (als normaler User) wird der Quelltext compiliert. Es sollten keine
    Fehler auftreten.  Auf Solaris k�nnten Warnungen der Form "function
    declaration isn't a prototype" auftreten, die getrost ignoriert
    werden k�nnen

    Durch Eingabe von

        make check

    werden einige Tests angesto�en.  Bitte melden Sie sich auf der
    Leafnode Mailingliste, falls nicht alle Tests positiv verlaufen
    (PASS).

3.a Legen Sie (als root) einen User "news" an, wenn es einen solchen
    bisher nicht gibt.

3.b Richten Sie (als root) einen Alias im Mailsystem an, das Mails,
    die an den User "news" gerichtet sind, an einen existierenden
    Benutzer weiterleitet, der sich um Leafnode k�mmert.

    Wenn Sie qmail verwenden: installieren Sie das fastforward Paket,
    das von den qmail-Seiten erh�ltlich ist.

    F�gen Sie dazu zu Ihrer Aliases-Datei (/etc/aliases oder
    /etc/mail/aliases) die Zeile "news: joe" hinzu (falls joe der
    f�r Leafnode zust�ndige User ist) und geben Sie
        newaliases
    ein.

4.  Die Eingabe von
        make install
    (als root) wird Leafnode installiert.

5.  Wenn Sie von einer alten leafnode-Version vor 1.9.3 (einschlie�lich
    aller Betaversionen bis incl. 1.9.3b5) updaten, m�ssen Sie (als
    root) "make update" eingeben.  Dadurch wird das beiliegende Shellscript
    update.sh abgearbeitet.  Es formatiert die groupinfo-Datei um und
    verschiebt einige Dateien in andere Ordner.  Falls dabei etwas
    schiefgehen sollte, finden Sie die alte groupinfo-Datei unter
    /var/spool/news/leaf.node/groupinfo.old . Wenn "make update" fehlerfrei
    funktioniert hat, kann diese Datei gel�scht werden; sie wird nicht mehr
    ben�tigt.

6.  Passen Sie die $(sysconfdir)/config mit einem Editor an Ihre
    Verh�ltnisse an ($(sysconfdir) ist gew�hnlich /etc/leafnode). Die
    Syntax der Datei ist in config.example und leafnode(8) dokumentiert.

6a. Es ist unbedingt notwendig, beim Wert f�r "server" den eigenen
    Upstream-Newsserver (in der Regel ist das der Newsserver Ihres
    Providers) einzutragen.

6b. In die Environment-Variable $NNTPSERVER oder die Datei
    /etc/nntpserver mu� der Name des eigenen Rechners eingetragen werden,
    damit Newsreader auch wirklich auf Leafnode zugreifen und nicht auf den
    Upstream-Server.  Wenn Sie die eingehenden News filtern m�chten, finden
    Sie Einzelheiten dazu weiter unten unter FILTER-DATEI.

6c. Setzen Sie "initialfetch = 200" oder �hnlich in der Konfigurations-
    datei, um zu verhindern, da� neue Gruppen mit hohem Volumen die
    Platten f�llen.

7.  Falls Ihr System keinen "fully qualified domain name" (FQDN) hat,
    besorgen Sie sich bitte einen von Ihrem Newsprovider (oder einer der
    unten angegeben Stellen) und tragen Sie ihn in Ihre /etc/hosts Datei
    ein.   Beispiel:  Falls Ihr FQDN debian.example.com ist und in
    /etc/hosts eine Zeile der Form

    192.168.0.1 debian

    steht, �ndern Sie diese bitte in

    192.168.0.1 debian.example.com debian

    Zum Thema FQDN und Message-IDs (und M�glichkeiten, FQDNs zu bekommen),
    gibt es weitere Informationen in der Datei README-FQDN sowie hier:

    http://www.hanau.net/faq_message-id_ueberschreiben.php
    http://fqdn.th-h.de/
    http://www.rumil.de/faq/kommentar.html

8.  Richten Sie (als root) f�r den User "news" einen cron-Job ein, der
    jede Nacht (oder wenigstens einmal pro Woche) texpire ausf�hrt. Meine
    entsprechende crontab-Zeile sieht folgenderma�en aus (texpire l�uft
    jede Nacht um 4:00):

0 4 * * * /usr/local/sbin/texpire

    Um die Crontab-Datei zu editieren, habe ich als root "crontab -u news -e"
    eingegeben und obige Zeile eingef�gt. Wenn man den dritten "*" durch
    eine "1" ersetzt,

0 4 * * 1 /usr/local/sbin/texpire

    dann wird texpire nur jeden Montag um 4:00 ausgef�hrt. Weitere
    Informationen �ber das Generieren von Crontab-Zeilen finden Sie
    in der manpage zu crontab(5).

9.  Stellen Sie sicher, da� fetchnews zu einer geeigneten Zeit
    aufgerufen wird. Wenn Sie �ber eine Standleitung verf�gen, ist es
    am besten, fetchnews von cron ausf�hren zu lassen (wieder als User
    "news"); anderenfalls mu� fetchnews ausgef�hrt werden, w�hrend
    Sie online sind. Wenn fetchnews vom User "root" gestartet wird,
    wird es zum User "news" wechseln. Falls Sie PPP benutzen, k�nnen
    Sie fetchnews aus /etc/ppp/ip-up starten. 

10. (Als root) F�gen Sie zu /etc/hosts.deny die Zeile

    leafnode: ALL

    hinzu.  F�gen Sie zu /etc/hosts.allow die Zeile

    leafnode: 127.0.0.1

    hinzu, um Zugriff vom lokalen Rechner zu erlauben.  Falls Sie Zugriff
    vom LAN ebenfalls erlauben m�chten, k�nnte die Zeile folgenderma�en
    aussehen (bitte die Netznummern anpassen):

    leafnode: 127.0.0.1 192.168.0.0/255.255.255.0

    Damit sch�tzen Sie ihren Server vor Mi�brauch durch andere Leute.
    Weitere Informationen gibt es in den manpages zu hosts_access(5)
    und hosts_options(5).

11. Es h�ngt vom lokalen System ab, wie leafnode letztlich gestartet
    wird.  VERWENDEN SIE (NUR) GENAU EINE DER DREI FOLGENDEN M�GLICH-
    KEITEN, IM ZWEIFELSFALL 11c mit direkter Zugriffskontrolle durch
    tcpserver (die Zeile mit -x und tcprules).

    WARNUNG:  Unabh�ngig von der Methode, mit der der Zugriff beschr�nkt
    wird, verwenden Sie keine Namenbasierte (DNS) Zugriffsregeln, sondern
    IP-basierte (wie 1.2.3.4).  Verwenden Sie keine Namen, die zu
    dynamischen IP-Adressen aufl�sen oder auf dynamische IP-Adressen
    zeigen, zur Zugangskontrolle.

    Die Alternativen sind (w�hlen Sie nur eine):

    a) Urspr�nglich wurde die Funktionalit�t, Netzwerk-Server zu starten,
    lediglich durch inetd bereitgestellt, auf HP-UX 10, Solaris 8, *BSD
    und einigen anderen ist das immer noch der Fall; siehe dazu Abschnitt
    11a.  Da jedoch die meisten inetd-Implementationen Designschw�chen
    aufweisen, ist dies nur f�r FreeBSD empfehlenswert.  Die Konfiguration
    von inetd erfolgt �blicherweise in der Datei /etc/inetd.conf oder
    /etc/inet/inetd.conf.

    b) Mit xinetd wurde diese Funktionalit�t mit verbessertem Design
    neu implementiert; xinetd wird in Red Hat und SUSE Linux eingesetzt.
    Siehe dazu Abschnitt 11b.

    c) Eine weitere M�glichkeit existiert mit daemontools und ucspi-tcp
    von Dan J. Bernstein.  Die Installation dieser Pakete ist einfach,
    unterscheidet sich aber von �blichen Softwarepaketen, wie andere
    DJB-Software.  Sie wird in Abschnitt 11c (automatisch) und 11d
    (manuell) beschrieben.

11a.NUR FALLS SIE INETD BENUTZEN: (die meisten aktuellen Distributionen
    verwenden xinetd, siehe 11b. unten)

    Editieren Sie (als root) /etc/inetd.conf so, da� leafnode bei
    ankommenden NNTP-Verbindungen gestartet wird. Bei mir sieht die
    entsprechende Zeile folgenderma�en aus:

    nntp stream  tcp nowait news /usr/sbin/tcpd /usr/local/sbin/leafnode

    Dadurch wird leafnode f�r alle Verbindungen auf dem nntp-Port
    gestartet, wenn der Inhalt der Dateien /etc/hosts.allow und
    /etc/hosts.deny nicht dagegen sprechen.  Wenn auf Ihrem System kein
    /usr/sbin/tcpd installiert ist, besorgen Sie sich bitte das Paket
    tcp_wrappers und installieren es.   Benutzung von Leafnode ohne
    tcpd wird nicht unterst�tzt und ist aufgrund der Gefahr durch
    Mi�brauch dringendst abzuraten!

    Wenn Sie /etc/inetd.conf editiert haben, m�ssen Sie dem inetd diese
    Ver�nderungen mitteilen. Der inetd wertet sein Konfigurationsfile
    erneut aus, wenn Sie ihm ein HANGUP-Signal schicken. Dies tun Sie,
    indem Sie als root eingeben:

    kill -HUP `cat /var/run/inetd.pid`

    Weiter mit Abschnitt 12.

11b.FALLS SIE XINETD benutzen:

    xinetd Versionen vor 2.3.3 werden nicht unterst�tzt.  Es ist
    unbekannt, ob Leafnode mit �lteren Versionen funktioniert.

    F�r Informationen zu xinetd konsultieren Sie bitte die manpages
    zu xinetd und xinetd.conf.

    a. �berpr�fen Sie, ob /etc/xinetd.conf eine Zeile der Form
       "includedir /etc/xinetd.d" enth�lt.  Falls ja, so f�hren Sie
       bitte die folgenden �nderungen in /etc/xinetd.d/leafnode durch,
       falls nicht, so f�gen Sie die �nderungen bitte an die Datei
       /etc/xinetd.conf an.

    b. Editieren Sie die gew�hlte Datei, indem Sie folgenden Eintrag
       hinzuf�gen:

	service nntp
	{
	    flags           = NAMEINARGS NOLIBWRAP
	    socket_type     = stream
	    protocol        = tcp
	    wait            = no
	    user            = news
	    server          = /usr/sbin/tcpd
	    server_args     = /usr/local/sbin/leafnode
	    instances       = 7
	    per_source      = 3
	}

    Dieser Eintrag erlaubt maximal 7 Verbindungen zu Leafnode gleich-
    zeitig, davon maximal 3 vom selben Rechner.  Sie k�nnen diese Werte
    anpassen.

    Um xinetd die �nderungen mitzuteilen, senden Sie ein USR2 Signal.
    Finden Sie dazu die PID des laufenden xinetd mit
        ps ax | egrep '[x]inetd'
    auf Linux oder *BSD, oder mit
        ps -ef | egrep '[x]inetd'
    auf SysV-Systemen (Solaris) heraus.  Dann geben Sie
        kill -s USR2 12345
    (als root) ein, wobei Sie 12345 mit der PID ersetzen.

    Weiter mit Abschnitt 12.

11c.AUTOMATISCHE INSTALLATION VON DAEMONTOOLS UND UCSPI-TCP (als root)

    Diese Installationsmethode konfiguriert den leafnode-Start aus
    tcpserver, mit nativer ("tcprules") Zugriffskontrolle, die in der
    Voreinstellung Verbindungen von 127.0.0.1 und nur auf dieser IP
    annimmt. M�chten Sie tcpd, hosts.allow und hosts.deny benutzen,
    lesen Sie bitte stattdessen Abschnitt 11d.

    a. Zuerst besorgen Sie sich Dan J. Bernsteins daemontools und ucspi-tcp
    Pakete von http://cr.yp.to/daemontools.html und
    http://cr.yp.to/ucspi-tcp.html -- f�r IPv6 wird noch der IPv6-patch
    von Felix von Leitner ben�tigt: http://www.fefe.de/ucspi/ .

    b. Starten Sie setup-daemontools.sh. Dieses Skript nimmt die
    Konfiguration vor. Leafnode nimmt nun Verbindungen von localhost
    (127.0.0.1) an.

    c. Wenn Sie die Konfiguration �ndern wollen (die IP, auf der
    Verbindungen akzeptiert werden, Zugriffsregeln, H�chstzahl der
    Newsreader), lesen Sie bitte LIESMICH-daemontools.

11d.MANUELLE INSTALLATION VON DAEMONTOOLS UND UCSPI-TCP (als root)
    a. Zuerst besorgen Sie sich Dan J. Bernsteins daemontools und ucspi-tcp
    Pakete von http://cr.yp.to/daemontools.html und
    http://cr.yp.to/ucspi-tcp.html -- f�r IPv6 wird noch der IPv6-patch
    von Felix von Leitner ben�tigt: http://www.fefe.de/ucspi/ .

    b. Erstellen Sie ein "log" Verzeichnis im Leafnode-Konfigurations-
    Verzeichnis:
        mkdir /etc/leafnode/log

    c. Erstellen Sie eine "run"-Datei in diesem Verzeichnis mit Ihrem
    Lieblingseditor, Inhalt:
        #! /bin/sh
	exec logger -p daemon.notice -t leafnode

    d.  chmod 755 log/run

    e. Erstellen Sie eine "run"-Datei im Leafnode-Konfig-Verzeichnis.
    Es gibt zwei M�glichkeiten:  entweder wird der Zugriffsschutz per
    hosts.allow und hosts.deny geregelt, oder aber mit dem tcpserver-
    eigenen Zugriffsschutz:

    F�R hosts.allow, sollte die Datei in etwa folgendes enthalten:
    #! /bin/sh
    exec 2>&1
    exec /usr/local/bin/tcpserver -c10 -l0 -H -v 127.0.0.1 119 \
    /usr/local/bin/setuidgid news /usr/local/bin/argv0 /usr/sbin/tcpd \
    /usr/local/sbin/leafnode

    Gegebenenfalls m�ssen Sie hier die Pfade anpassen, falls die Software
    in andere Verzeichnisse installiert wurde.  Au�erdem mu� die IP
    127.0.0.1 mit der IP des Computers ersetzt werden, auf dem leafnode
    im LAN erreichbar sein soll.

    Die Optionen werden weiter unten erkl�rt.

    F�R den tcpserver-eigenen Zugriffsschutz:
    #! /bin/sh
    exec 2>&1
    exec /usr/local/bin/tcpserver -c10 -l0 -H -v -x nntp.cdb \
    127.0.0.1 119 \
    /usr/local/bin/setuidgid news /usr/local/sbin/leafnode

    Analog m�ssen ggfs. Pfade und IP angepa�t werden.

    Erl�uterung der oben angegebenen Optionen:
    -c10        h�chstens 10 Verbindungen gleichzeitig.
    -l0         lokalen Hostnamen nicht aufl�sen, sondern "0" verwenden
                (leafnode l�st den lokalen Hostnamen selbst auf).
    -H          client Hostnamen nicht aufl�sen (macht leafnode selbst).
    -v          Verbindungen aufzeichnen (log).
    -x          Zugriffskontrolle auf Basis von tcprules verwenden,
                Zugriff verweigern, falls keine Regel existiert.
    Mehr Informationen dazu auf http://cr.yp.to/ucspi-tcp/tcpserver.html

    f. NUR F�R tcpserver-eigenen Zugriffsschutz:
    Legen Sie eine Datei "nntp.rules" f�r den Zugriff an, Beispiel:
    joe@1.2.3.4:allow
    fool@192.168.0.4:deny
    192.168.0.:allow
    :deny

    g. NUR F�R tcpserver-eigenen Zugriffsschutz:
    Compilieren Sie nntp.rules zu nntp.cdb:
    tcprules nntp.cdb nntp.tmp <nntp.rules
    DIESER SCHRITT MUSS NACH JEDER �NDERUNG DER nntp.rules DATEI
    WIEDERHOLT WERDEN!
    (tcpserver bemerkt -- analog zu tcpd -- die �nderungen dann selbst,
    ein SIGHUP ist nicht notwendig).

    h.  chmod 755 run

    i. Erstellen Sie einen Symlink in das /service Verzeichnis:
    ln -s `pwd` /service

    svscan sollte den neuen leafnode-Dienst innerhalb von 5 Sekunden
    erkennen und ihn starten.

12. F�hren Sie fetchnews aus. Der erste Aufruf von fetchnews wird
    einige Zeit dauern,  weil fetchnews eine Liste aller auf dem
    Upstream-Server vorhanden Gruppen einliest. Mit einem 28.8 Modem
    kann dies bis zu 60 Minuten dauern (abh�ngig davon, wie viele
    Gruppen Ihr Provider anbietet). Um zu sehen, was fetchnews gerade
    tut, k�nnen Sie es mit der Option -vvv starten. 
    Wenn Sie leafnode von einer Version vor 1.6 updaten, rufen Sie
    fetchnews bitte mit dem Parameter -f auf, da sich das Format der
    groupinfo-Datei ge�ndert hat. 
   
13. Lesen Sie (als normaler Benutzer) News mit einem NNTP-f�higen
    Newsreader (in $NNTPSERVER oder /etc/nntpserver mu� dazu Ihr eigener
    Rechner eingetragen sein).  W�hlen Sie die Gruppen aus, die Sie in
    Zukunft lesen m�chten. Diese sind zun�chst einmal leer, abgesehen von
    einem Platzhalterartikel.  Bei einigen Newsreadern ist es notwendig,
    da� Sie diesen Artikel in jeder gew�nschten Gruppe lesen, damit die
    Gruppe in Zukunft abonniert wird.

    Danach sollten Sie f�r jede selektierte Gruppe eine leere Datei
    in /var/spool/news/interesting.groups/ vorfinden.

14. Lassen Sie fetchnews noch einmal laufen; nun sollten f�r alle
    ausgew�hlten Gruppen Artikel geholt werden. Es empfiehlt sich
    dabei, initialfetch auf einen sinnvollen Wert zu setzen, da
    leafnode sonst alle Artikel holt, die upstream vorhanden sind (und
    das kann dauern).  

15. Falls Sie sich von au�erhalb Ihres LAN (au�erhalb der Subnetze der
    lokalen Interfaces) zu Leafnode verbinden m�chten, lesen Sie bitte
    die Dateien README_de und config.example f�r die seit Version 1.9.23
    notwendigen Einstellungen.

Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>
Matthias Andree <matthias.andree@gmx.de>
Deutsche �bersetzung: Alexander Stielau <aleks@zedat.fu-berlin.de>
                       Alexander Reinwarth <a.reinwarth@gmx.de>
		       Klaus Fischer <fischer@reutlingen.netsurf.de>
                       Ralf Wildenhues <Ralf.Wildenhues@gmx.de>
