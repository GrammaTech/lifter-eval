#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, HaskellProperties, register


@register
class Properties(HaskellProperties, SmokeTestWithGrepMixin):
    name = "kmonad"
    smoke_args = ["--version"]
    grep_pattern = "commit"
