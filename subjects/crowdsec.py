#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, GoProperties, register
from pathlib import Path


@register
class Properties(GoProperties, SmokeTestWithGrepMixin):
    name = "crowdsec"
    binary_path_in_repo = Path("cmd") / "crowdsec" / "crowdsec"
    smoke_args = ["--help"]
    grep_pattern = "crowdsec"
    grep_stderr = True
