#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, OcamlProperties, register
from lifter_eval.docker import OcamlDockerImage


class DockerImageImpl(OcamlDockerImage):
    apt_packages = OcamlDockerImage.apt_packages + [
        "autoconf",
        "graphviz",
        "libgmp-dev",
        "libgtksourceview-3.0-dev",
    ]


@register
class Properties(OcamlProperties, SmokeTestWithGrepMixin):
    name = "frama-c"
    smoke_args = ["-h"]
    grep_pattern = "Frama-C"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
