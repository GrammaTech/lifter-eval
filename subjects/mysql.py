#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, OS, Compiler, LLVMObfuscation, Optimization, Spec


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["bison"]
        isa_dep = ["libncurses5-dev", "libssl-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    supported_traits = Spec(
        os_list=[OS.ubuntu20, OS.ubuntu16],
        compilers=[Compiler.gcc, Compiler.clang],
        optimizations=[
            Optimization._0,
            Optimization._1,
            Optimization._2,
            Optimization._3,
            Optimization.s,
        ],
        obfuscations=[LLVMObfuscation.fla, LLVMObfuscation.sub],
    )

    name = "mysql"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "usr" / "local" / "mysql" / "bin" / "mysqld"

    # do not run the whole test suite, it runs forever and some test fail
    # "engines/funcs" suite runs for ~40 minutes and seems good enough
    evaluate_subdir = (
        Path(name) / prefix_subdir / "usr" / "local" / "mysql" / "mysql-test"
    )
    evaluate = "./mysql-test-run.pl --big-test --suite=engines/funcs"

    smoke_args = ["--help"]
    grep_pattern = "MySQL"

    boost_name = "boost_1_73_0"
    cleanup_exclude_patterns = [f"/{boost_name}"]

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        build_dir = (self.repo_dir / "build").as_posix()
        boost_dir = (self.repo_dir / self.boost_name).as_posix()
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        # -DCMAKE_INSTALL_RPATH=$prefix/usr/local/mysql/lib/private
        return f"""\
if [ ! -d {self.boost_name} ]; then
    wget -O - https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/{self.boost_name}.tar.gz \
        | tar -xz
fi
mkdir -p {build_dir} && cd {build_dir}
cmake -DWITH_BOOST={boost_dir} -DCMAKE_BUILD_TYPE:STRING=None -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON ..
make -j 2
make DESTDIR={prefix} install
"""

    @property
    def cflags(self):
        # trying to workaround "error: use of undeclared identifier 'debug_sync_set_action'"
        return super().cflags + ["-DNDEBUG"]
