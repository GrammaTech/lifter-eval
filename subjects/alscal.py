#!/usr/bin/env python3
from shlex import quote
from typing import List
from lifter_eval.subject_properties import FortranProperties, SmokeTestWithGrepMixin, register


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "alscal"
    test_output_file = "car_test_output.txt"
    smoke_stdin = f"car.txt\n{test_output_file}"
    grep_pattern = "Normal end of execution"

    @property
    def compiler_flags(self) -> List[str]:
        return ["-w"] + super().compiler_flags

    @property
    def evaluate(self) -> str:
        binary = self.binary_path.as_posix()
        return f"""\
rm -f {self.test_output_file}
{binary} <<< {quote(self.smoke_stdin)}
diff car_output.txt {self.test_output_file}
"""
