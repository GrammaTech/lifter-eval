# Building Notepad++ in the Windows system
## Prerequisite
Just install the MS Visual Studio (2017 or 2019)
## Clone the repo
Clone the Notepad++ repo from here - https://github.com/notepad-plus-plus/notepad-plus-plus   
## Build the solution
Open the `notepad-plus-plus/PowerEditor/visual.net/notepadPlus.sln` solution file. The solution contains 2 projects: `SciLexer` and `Notepad++`.   
After opening just select Build -> Build Solution from the menu.
## Optimization
MS Visual Studio has 2 separate sets of optimization:   
- /Od, /O1, /O2, /Ox (Optimization)   
- Favor small code (/Os), Favor fast code (/Ot), Neither (no keys) (Favor Size or Speed)   
To select the keys open properties of desired project (Mouse R-Click) an select C/C++ -> Optimization   
## Pie or No-Pie
To select Pie or No-Pie option open properties of the `Notepad++` project (Mouse R-Click) an select Linker -> Advanced -> Randomized Base Address   
