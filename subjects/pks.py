#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "pks"
    binary_path_in_repo = Path("pksd")
    build = "./configure && make"
    smoke_expected_code = 1
    grep_pattern = "usage: .* conf_file"
    grep_stderr = True
