#!/usr/bin/env python3
import os
from pathlib import Path
from typing import List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler


class DockerImageImpl(SubjectDockerImage):
    user = "unrealircd"
    user_home = "/home/" + user

    def __init__(self, props: "Properties"):
        super().__init__(props.name, props.env.traits())
        self.__props = props

    @property
    def apt_packages(self):
        common = ["python"]
        isa_dep = ["libsodium-dev", "libargon2-dev", "libssl-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep

    @property
    def commands(self) -> List[str]:
        # Set up user permissions since we are using "unrealrcd" user
        mount_points = [p.as_posix() for p in self.__props.docker_volumes.values()]
        user_permissions = " && ".join(
            f"mkdir -p {dir} && chown {self.user}:{self.user} {dir}"
            for dir in mount_points
        )

        try:
            gid = os.getgid()
            uid = os.getuid()
        except AttributeError:
            gid = 10
            uid = 1000

        return super().commands + [
            f"RUN groupadd -f -g {gid} {self.user} && useradd -g {gid} -u {uid} {self.user} && mkdir {self.user_home} && chown {self.user}:{self.user} {self.user_home}",
            "RUN " + user_permissions,
            f"USER {self.user}",
        ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "unrealircd"
    inst_dir = Path(
        "inst-path-long-enough-for-patchelf"
    )  # see also "./unrealircd.config.settings"
    binary_path_in_repo = inst_dir / "bin" / name
    test_repo_subdir = Path("unrealircd-tests")
    evaluate_subdir = test_repo_subdir

    smoke_args = ["-v"]
    grep_pattern = "UnrealIRCd"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self)

    @property
    def git_submodules(self):
        return super().git_submodules + [self.test_repo_subdir]

    @property
    def tp_modules(self) -> Path:
        return self.repo_dir / "src" / "modules" / "third"

    @property
    def cflags(self):
        result = super().cflags
        if self.traits.compiler == Compiler.icx:
            # workaround configure failure
            result.append("-Wno-implicit-function-declaration")
        return result

    @property
    def build(self) -> str:
        test_repo = self.env.sandbox_path(self.test_repo_subdir).as_posix()
        settings = self.env.sandbox_path(Path("unrealircd.config.settings")).as_posix()
        return f"""\
# required for functional tests
cp -av {test_repo}/serverconfig/unrealircd/modules/*.c {self.tp_modules.as_posix()}/

# Use these settings to avoid user interaction
cp {settings} ./config.settings
./Config -quick

make
make pem # generate certificate files, used by functional tests
make install

chmod go+rx {self.binary_path.as_posix()} # fix permissions, e9patch is sensitive for them
"""

    @property
    def evaluate(self) -> str:
        inst_dir = (self.repo_dir / self.inst_dir).as_posix()
        return f"""
    unrealircd_test_dir={DockerImageImpl.user_home}/unrealircd
    test -L $unrealircd_test_dir || ln -s {inst_dir} $unrealircd_test_dir
    ./run -services none
"""
