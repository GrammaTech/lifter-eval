#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, OS, Compiler, Optimization, Spec


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "zip",
        "ccache",
        "junit4",
        "libkrb5-dev",
        "nasm",
        "graphviz",
        "qtbase5-dev",
        "libkf5coreaddons-dev",
        "libkf5i18n-dev",
        "libkf5config-dev",
        "libkf5windowsystem-dev",
        "libkf5kio-dev",
        "autoconf",
        "libcups2-dev",
        "libfontconfig1-dev",
        "gperf",
        "default-jdk",
        "doxygen",
        "libxslt1-dev",
        "xsltproc",
        "libxml2-utils",
        "libxrandr-dev",
        "bison",
        "flex",
        "libgtk-3-dev",
        "libgstreamer-plugins-base1.0-dev",
        "libgstreamer1.0-dev",
        "ant",
        "ant-optional",
        "python3-pip",
    ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    supported_traits = Spec(
        [ISA.x64],
        [OS.ubuntu20],
        [Compiler.gcc, Compiler.clang],
        optimizations=list(Optimization),
    )
    name = "libreoffice"
    build_subdir = Path("instdir") / "program"
    binary_path_in_repo = build_subdir / "libsofficeapp.so"
    cleanup_exclude_patterns = ["/external/tarballs"]

    smoke_args = ["--version"]
    smoke_test_binary = build_subdir / "soffice.bin"
    grep_pattern = "LibreOffice"

    evaluate = "make subsequentcheck"

    # Won't build with pie options
    cflags_pie = ""
    ldflags_pie = ""

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        return "./autogen.sh && make GMAKE_OPTIONS='VERBOSE=1' && make install"
