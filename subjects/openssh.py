#!/usr/bin/env python3
from pathlib import Path
from typing import List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import Pie


class DockerImageImpl(SubjectDockerImage):
    @property
    def run_commands(self) -> List[str]:
        return super().run_commands + ["useradd sshd && mkdir /var/empty"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "openssh"
    binary_path_in_repo = Path("ssh")

    grep_pattern = "usage: ssh"
    grep_stderr = True
    smoke_expected_code = 255

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        if self.traits.pie == Pie.pie:
            confgure_options = "--with-pie"
        else:
            confgure_options = "--without-pie"

        return f"""\
autoreconf -i -v -f
./configure {confgure_options}
make
make regress-binaries # required for `evaluate()``
"""

    @property
    def evaluate(self) -> str:
        return "make interop-tests t-exec"
