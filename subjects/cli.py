#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, GoProperties, register
from pathlib import Path


@register
class Properties(GoProperties, SmokeTestWithGrepMixin):
    name = "cli"
    binary_path_in_repo = Path('bin') / "gh"
    grep_pattern = "GitHub"
