# Installing VLC for Windows
The tar archive contains a folder with 32bit version of VLC. After extraction just run the vlc.exe file.
## Building the Windows version
The wiki-page is located here - https://wiki.videolan.org/Win32Compile/ but it is kind of incomplete.
Here are the findings od building VLC on Ubuntu 20.
### Missed packages
Following packages should be installed
```
apt-get install gcc-mingw-w64-x86-64 g++-mingw-w64-x86-64 mingw-w64-tools
apt-get install lua5.2 libtool automake autoconf autopoint make gettext pkg-config
add-apt-repository ppa:rock-core/qt4
apt-get update
apt-get install qt4-dev-tools qt5-default git subversion cmake cvs
apt-get install wine64-development-tools libwine-dev zip p7zip nsis bzip2
apt-get install meson
apt-get install libavcodec-dev libavutil-dev
apt-get install flex bison
```
### Configuration issues
The configuration script 
```
../extras/package/win32/configure.sh --host=HOST-TRIPLET --build=x86_64-pc-linux-gnu
```
failed because 32-bits version of lua required. So to complete configuration following options shpould be used
```
../extras/package/win32/configure.sh --host=HOST-TRIPLET --build=x86_64-pc-linux-gnu --disable-lua --disable-qt
```
### Compilation issue
The `make` failed because following types were not defined in the DirectX header file:
```
D3D11_VIDEO_PROCESSOR_PROCESSOR_CAPS
D3D11_VIDEO_PROCESSOR_PROCESSOR_CAPS_DEINTERLACE_BLEND
D3D11_VIDEO_PROCESSOR_PROCESSOR_CAPS_DEINTERLACE_BOB
D3D11_VIDEO_PROCESSOR_PROCESSOR_CAPS_DEINTERLACE_MOTION_COMPENSATION
```
Probably the header file is not updated correctly and the newer version not uploaded to the version control system.

