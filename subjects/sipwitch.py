#!/usr/bin/env python3
from pathlib import Path
from typing import List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["autoconf", "libtool", "libucommon-dev"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "sipwitch"
    build_subdir = Path("inst")
    binary_path_in_repo = build_subdir / "sbin" / "sipw"

    smoke_args = ["--version"]
    grep_pattern = "SIP Witch"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def prefix(self) -> Path:
        return self.repo_dir / self.build_subdir

    @property
    def cflags(self) -> List[str]:
        # libeXosip2 does not have PKG_CONFIG
        return super().cflags + [f"-I{self.prefix.as_posix()}/include"]

    @property
    def build(self) -> str:
        prefix = self.prefix.as_posix()
        return f"""
{self.build_dep_lib("http://ftp.gnu.org/gnu/osip/libosip2-4.1.0.tar.gz")}
{self.build_dep_lib(
    "http://download.savannah.nongnu.org/releases/exosip/libeXosip2-4.1.0.tar.gz",
    "--enable-openssl=no")}
./autogen.sh
PKG_CONFIG_PATH={prefix}/lib/pkgconfig ./configure --prefix={prefix}
make
make install
"""

    def build_dep_lib(self, tar_gz_url: str, opts=""):
        lib_dir = tar_gz_url[tar_gz_url.rfind("/") + 1 : tar_gz_url.rfind(".tar.gz")]
        opts += f" --prefix={self.prefix.as_posix()}"
        # workaround compilation failure with "-Ofast"
        opts += " --disable-debug"
        return f"""\
wget -O - {tar_gz_url} | tar -xz
cd {lib_dir}
./autogen.sh
CFLAGS="{self.dep_cflags}" ./configure {opts}
make
make install
cd ..
"""

    @property
    def dep_cflags(self) -> str:
        # workaround "-fla/-sub option: may only occur zero or one times!"
        result = self.build_variables["CFLAGS"]
        obf_flags = " ".join(self.cflags_obfuscate)
        i = result.find(obf_flags)
        if i != -1:
            result = result[:i] + result[i + len(obf_flags) :]
        return result
