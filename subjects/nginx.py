#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "nginx"
    binary_path_in_repo = Path("objs") / name
    test_repo_subdir = Path("nginx-tests")

    build = './auto/configure "--with-ld-opt=$LDFLAGS" && make'

    smoke_args = ["-V"]
    grep_pattern = "nginx version"
    grep_stderr = True

    evaluate_subdir = test_repo_subdir
    evaluate = "prove $(cat ../nginx.tests.txt)"

    @property
    def git_submodules(self):
        return super().git_submodules + [self.test_repo_subdir]
