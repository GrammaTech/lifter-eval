#!/usr/bin/env python3
from pathlib import Path
from typing import Dict

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler, Pie


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["autoconf", "tmux"]
        isa_dep = [
            "libncurses5-dev",
            "libprotobuf-dev",
            "libssl-dev",
            "protobuf-compiler",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "mosh"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "bin" / "mosh-server"

    smoke_args = ["--version"]
    grep_pattern = "mosh-server"
    evaluate = "make check"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build_variables(self) -> Dict[str, str]:
        result = super().build_variables
        if self.traits.compiler == Compiler.icx:
            # current icx image finds "/opt/intel/oneapi/intelpython/latest/bin/protoc"
            # first and it does not work
            result["PROTOC"] = "/usr/bin/protoc"
        return result

    @property
    def build(self) -> str:
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        enable_hardening = "yes" if self.traits.pie == Pie.pie else "no"
        confgure_options = f"--prefix={prefix}  --enable-hardening={enable_hardening}"
        return (
            f"./autogen.sh && ./configure {confgure_options}"
            " && make AM_DEFAULT_VERBOSITY=1 && make install"
        )
