#!/usr/bin/env python3
from pathlib import Path
from typing import List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def run_commands(self) -> List[str]:
        result = super().run_commands
        if self.traits.isa == ISA.arm:
            # CMake 3.16.3 cannot build "anope" and newer packages are not available.
            # Build CMake from source code.
            result += [
                "git clone https://github.com/Kitware/CMake.git"
                " && cd CMake"
                " && ./bootstrap && make && make install"
                " && cd .. && rm -r CMake"
            ]
        return result


@register
class Properties(CProperties):
    name = "anope"
    binary_path_in_repo = Path("build/src/tools/anopesmtp")

    smoke_args = ["127.0.0.1"]
    smoke_stdin = """\
From: from@anope_test
To: to@anope_test

Hello
"""

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        return f"""\
echo -e '\\n\\n\\n\\n\\n\\n\\n\\n-DCMAKE_BUILD_TYPE:STRING=None -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON'\
    | ./Config -nocache -nointro
cd build
make $binary_name
"""
