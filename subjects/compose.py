#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, GoProperties, register
from pathlib import Path


@register
class Properties(GoProperties, SmokeTestWithGrepMixin):
    name = "compose"
    binary_path_in_repo = Path("bin") / "build" / "docker-compose"
    smoke_args = ["--help"]
    grep_pattern = "docker compose"
