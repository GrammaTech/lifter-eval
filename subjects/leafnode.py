#!/usr/bin/env python3
from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        isa_dep = ["libpcre3-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "leafnode"

    smoke_args = ["--version"]
    smoke_expected_code = 1
    grep_pattern = "503 Unable to read configuration"

    build = "./configure && make"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
