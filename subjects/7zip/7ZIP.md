# Installing 7-zip
The tar archive contains 2 folder with 32bit and 64bit version of 7-zip
## The folder content
Each folder contains:
- 7-zip installer with 7-zip version 19.00 - `7z1900.exe`   
- 7-zip command line utility - `7z.exe`   
- 7-zip file manager - `7zFM.exe`   
Install the 7-zip launching the installer, patch the needed `exe` file and copy the patched `exe` file to the 7-zip location.
