#!/usr/bin/env python3
from lifter_eval.docker import OcamlDockerImage
from lifter_eval.subject_properties import OcamlProperties, SmokeTestWithGrepMixin, register


class DockerImageImpl(OcamlDockerImage):
    apt_packages = OcamlDockerImage.apt_packages + [
        "libcairo2-dev",
        "libexpat1-dev",
        "libgtk-3-dev",
        "pkg-config",
    ]


@register
class Properties(OcamlProperties, SmokeTestWithGrepMixin):
    name = "unison"
    smoke_args = ["-version"]
    grep_pattern = "unison"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
