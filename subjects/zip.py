#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["ruby", "xxd"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "zip"
    test_repo_subdir = Path("zip-tests")

    build = "make -f unix/Makefile generic"
    evaluate_subdir = test_repo_subdir
    smoke_args = ["--version"]
    grep_pattern = "Zip"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def git_submodules(self):
        return super().git_submodules + [self.test_repo_subdir]

    @property
    def evaluate(self):
        return "./tests.sh --zip " + self.binary_path.as_posix()
