#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["intltool"]
        isa_dep = [
            "libgtk2.0-dev",
            "libluajit-5.1-dev",
            "libsasl2-dev",
            "libxml2-dev",
            "libxt-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "pidgin"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "bin" / name

    smoke_args = ["--version"]
    grep_pattern = "Pidgin"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def cflags(self):
        result = super().cflags
        if self.traits.compiler == Compiler.icx:
            # icx treats this warning as an error
            result.append("-Wno-incompatible-function-pointer-types")
        return result

    @property
    def build(self) -> str:
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        disabled_features = [
            "gstreamer",
            "screensaver",
            "gtkspell",
            "gevolution",
            "vv",
            "idn",
            "meanwhile",
            "avahi",
            "dbus",
            "perl",
            "tcl",
            "nss",
            "gnutls",
        ]
        opts = " ".join(
            ["--prefix=" + prefix] + ["--disable-" + f for f in disabled_features]
        )
        return (
            f"autoreconf -fi && ./configure {opts}"
            " && make AM_DEFAULT_VERBOSITY=1 && make install"
        )
