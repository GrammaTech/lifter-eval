#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["autoconf", "ed", "libtool-bin", "libcppunit-dev", "libnss-db"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "squid"
    build_subdir = Path("src")

    smoke_args = ["--version"]
    grep_pattern = "Squid"

    evaluate = "make check"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        pkg_config_path_var = ""
        configure_opts = "--prefix=/usr/local/squid --disable-strict-error-checking"
        if self.traits.isa == ISA.x86:
            lib_dir = "/usr/lib/i386-linux-gnu"
            pkg_config_path_var = f"PKG_CONFIG_PATH={lib_dir}/pkgconfig"
            configure_opts += f" --build=i386-pc-linux-gnu --libdir={lib_dir} --with-ltdl-lib={lib_dir} --with-ltdl-include=/usr/include"

        return (
            f"{pkg_config_path_var} ./bootstrap.sh\n"
            f"{pkg_config_path_var} ./configure {configure_opts}\n"
            "make all\n"
            "make install"
        )
