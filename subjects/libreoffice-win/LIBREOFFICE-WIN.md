# Installing LibreOffice for Windows
The tar archive contains the LibreOffice installer and a folder with LibreOffice binaries.   
After installation the binaries will be located at `C:\Program Files\LibreOffice\program\`   
## Content of the binaries folder
The `binaries` folder contains following executables:   
- sbase.exe - database   
- scalc.exe - spreadsheet   
- sdraw.exe - diagrams   
- simpress.exe - presentations   
- smath.exe - formula editor  
- soffice.exe - main application   
- swriter.exe - word processor  
When any of these binaries is updated/modified it should be copied to the LibreOffice location
# Building LibreOffice for Windows
The building process is complicated, required installation of many dependencies and not fully formalized yet. Details can be found here - https://wiki.documentfoundation.org/Development/BuildingOnWindows and here - https://wiki.documentfoundation.org/Development/msvc-x86_64

