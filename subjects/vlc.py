#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["autopoint", "bison", "flex", "gettext"]
        isa_dep = [
            "lua5.2",
            "liblua5.2-dev",
            "libmpg123-dev",
            "libavutil-dev",
            "libavcodec-dev",
            "libavformat-dev",
            "libswscale-dev",
            "liba52-0.7.4-dev",
            "libxcb-composite0-dev",
            "libxcb-xvmc0-dev",
            "libxcb-randr0-dev",
            "libasound2-dev",
            "libgl-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "vlc"
    prefix_subdir = Path("inst")
    build_subdir = prefix_subdir / "bin"

    smoke_args = ["--version"]
    grep_pattern = "VLC"
    smoke_expected_code = 1
    grep_stderr = True

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def cflags(self):
        result = super().cflags
        if self.traits.compiler == Compiler.icx:
            # icx treats this warning as an error
            result.append("-Wno-incompatible-function-pointer-types")
        return result

    @property
    def build(self) -> str:
        pkg_config_path_var = ""
        configure_opts = "--prefix=" + (self.repo_dir / self.prefix_subdir).as_posix()
        configure_opts += " --disable-optimizations"  # prevent from adding -O3
        if self.traits.isa == ISA.x86:
            lib_dir = "/usr/lib/i386-linux-gnu"
            pkg_config_path_var = f"PKG_CONFIG_PATH={lib_dir}/pkgconfig"
            configure_opts += f" --build=i386-pc-linux-gnu"

        protoc_var = ""
        if self.traits.compiler == Compiler.icx:
            # current icx image finds "/opt/intel/oneapi/intelpython/latest/bin/protoc"
            # first and it does not work
            protoc_var = "PROTOC=/usr/bin/protoc"

        return (
            "autoreconf -fi\n"
            f"{protoc_var} {pkg_config_path_var} ./configure {configure_opts}\n"
            "make AM_DEFAULT_VERBOSITY=1\n"
            "make install"
        )
