#!/usr/bin/env python3
from pathlib import Path
from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "libidn11-dev",
        "libnet-telnet-perl",
        "libtest-unit-perl",
        "libwww-perl",
    ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "proftpd"
    binary_path_in_repo = Path("proftpd")
    smoke_args = ["--version"]
    grep_pattern = "ProFTPD"
    build = "./configure && make"
    evaluate_subdir = Path(name) / "tests"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def evaluate(self) -> str:
        test_cases = self.env.sandbox_path(Path("proftpd.tests.txt"))
        return f"perl tests.pl $(cat {test_cases.as_posix()})"
