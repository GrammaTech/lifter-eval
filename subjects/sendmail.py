#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["m4"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "sendmail"

    build = 'CC_="$CC" CFLAGS_="$CFLAGS" LDFLAGS_="$LDFLAGS" ./Build'
    docker_hostname = "test.com"
    smoke_args = ["-d"]
    smoke_expected_code = 72
    grep_pattern = "Version"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def binary_path_in_repo(self) -> Path:
        suffix = {ISA.x86: "x86_64", ISA.arm: "armv7l"}.get(
            self.traits.isa, self.traits.isa.value
        )
        return Path(f"obj.Linux.rel.{suffix}") / "sendmail" / "sendmail"
