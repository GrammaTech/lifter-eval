#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "qmail"
    binary_path_in_repo = Path("qmail-send")
    build = 'echo "$CC $CFLAGS" > conf-cc && echo "$CC $LDFLAGS" > conf-ld && make'
    smoke_expected_code = 111
    grep_pattern = "unable to switch to home directory"
    smoke_stdin = None
    grep_stdin = True
