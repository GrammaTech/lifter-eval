#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, GoProperties, register
from pathlib import Path


@register
class Properties(GoProperties, SmokeTestWithGrepMixin):
    name = "osv-scanner"
    binary_path_in_repo = Path("osv-scanner")
    smoke_args = ["--help"]
    grep_pattern = "osv-scanner"
    build = "go mod tidy && go build ./cmd/osv-scanner/"
