#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["autoconf", "libtool"]
        isa_dep = [
            "libssl-dev",
            "libkrb5-dev",
            "libcap-dev",
            "libnghttp2-dev",
            "libuv1-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "bind"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "sbin" / "named"

    smoke_args = ["-v"]
    grep_pattern = "BIND"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        return (
            f"autoreconf -fi && ./configure --prefix={prefix}"
            " && make && make install"
        )
