#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, HaskellProperties, register


@register
class Properties(HaskellProperties, SmokeTestWithGrepMixin):
    name = "ImplicitCAD"
    binary_name = "extopenscad"
    smoke_args = ["-h"]
    grep_pattern = "extopenscad"
