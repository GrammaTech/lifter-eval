#!/usr/bin/env python3
from pathlib import Path
from typing import List, cast

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, OS, Compiler, Optimization, Pie, Spec


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "keyboard-configuration",
        "libgtk-3-dev",
        "libgtksourceview-4-0",
        "itstool",
        "libgee-0.8-dev",
        "libsoup-gnome2.4-dev",
        "meson",
        "valac",
        "libmpc-dev",
        "libgtksourceview-4-dev",
        "software-properties-common",
    ]

    @property
    def run_commands(self) -> List[str]:
        return super().run_commands + [
            "add-apt-repository ppa:apandada1/libhandy-1 && apt update -y && apt install -y libhandy-1-0 libhandy-1-dev"
        ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    supported_traits = Spec(
        [ISA.x64, ISA.arm64, ISA.arm],
        [OS.ubuntu20],
        [Compiler.gcc],
        [Pie.pie],
        [
            Optimization._0,
            Optimization._1,
            Optimization._2,
            Optimization._3,
            Optimization.s,
        ],
    )

    name = "gnome-calculator"
    build_subdir = Path("build")
    binary_path_in_repo = build_subdir / "src" / name

    # CC CFLAG and LDFLAGS interfere with the meson-generated build config so clear them
    build_variables = {}

    smoke_args = ["--version"]
    grep_pattern = name
    grep_stderr = True

    evaluate = f"cd {build_subdir} && ninja test"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        meson_opt = {
            Optimization._0: "--optimization=0",
            Optimization._1: "--optimization=1",
            Optimization._2: "--optimization=2",
            Optimization._3: "--optimization=3",
            Optimization.s: "--optimization=s",
        }[cast(Optimization, self.traits.flags)]

        build_dir = (self.repo_dir / self.build_subdir).as_posix()
        return f"""\
rm -rf {build_dir} && mkdir {build_dir} && cd {build_dir}
meson --prefix=/usr {meson_opt} ..
ninja -v
"""
