#!/usr/bin/env python3
from pathlib import Path
from lifter_eval.subject_properties import (
    FortranProperties,
    SmokeTestWithGrepMixin,
    register,
)


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "f77_to_f90"
    smoke_stdin = ""
    grep_pattern = "Processing complete"

    @property
    def evaluate(self) -> str:
        result_file = "name.f90"
        return (
            f"rm -f {result_file}"
            f" && {self.binary_path.as_posix()} <<< ''"
            f" && diff expected_prog.f90 {result_file}"
        )
