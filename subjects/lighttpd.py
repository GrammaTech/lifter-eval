#!/usr/bin/env python3
from pathlib import Path
from typing import List
from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import OS


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "autoconf",
        "automake",
        "libtool",
        "m4",
        "libpcre3-dev",
        "pkg-config",
    ]

    @property
    def run_commands(self) -> List[str]:
        result = super().run_commands

        if self.traits.os == OS.ubuntu16:
            # Upgrade perl to version 5.30, required for lighttpd test suite
            result.append(
                "wget http://www.cpan.org/src/5.0/perl-5.30.0.tar.gz"
                " && tar xvfz perl-5.30.0.tar.gz"
                " && cd perl-5.30.0"
                " && ./Configure -Duseithreads -des"
                " && make && make test && make install"
                " && cd .. && rm -rf perl-5.30.0"
            )

        return result


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "lighttpd"
    binary_path_in_repo = Path("src/lighttpd")
    smoke_args = ["-v"]
    grep_pattern = "a light and fast webserver"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        return f"""\
./autogen.sh
./configure -C --prefix=/usr/local
make
"""

    @property
    def evaluate(self) -> str:
        return "make check"
