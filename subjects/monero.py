#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = [
        "libboost-chrono-dev",
        "libboost-date-time-dev",
        "libboost-filesystem-dev",
        "libboost-locale-dev",
        "libboost-program-options-dev",
        "libboost-regex-dev",
        "libboost-serialization-dev",
        "libboost-thread-dev",
        "libsodium-dev",
        "libssl-dev",
        "libunbound-dev",
        "libzmq3-dev",
        "python3-monotonic",
        "python3-psutil",
        "python3-requests",
    ]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "monero"
    binary_path_in_repo = Path("build") / "release" / "bin" / "monerod"

    smoke_args = ["--version"]
    grep_pattern = "Monero"

    evaluate = (
        "python3 tests/functional_tests/functional_tests_rpc.py"
        " /usr/bin/python3 tests/functional_tests build/release all"
    )

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
