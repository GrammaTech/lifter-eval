#!/usr/bin/env python3
from typing import List
from lifter_eval.subject_properties import FortranProperties, SmokeTestWithGrepMixin, register


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "dijkstra_openmp"
    grep_pattern = "DIJKSTRA"

    @property
    def compiler_flags(self) -> List[str]:
        return ["-fopenmp"] + super().compiler_flags

    @property
    def smoke_test(self) -> str:
        return "OMP_NUM_THREADS=1 " + super().smoke_test

    @property
    def evaluate(self) -> str:
        binary = self.binary_path.as_posix()
        case1 = f"OMP_NUM_THREADS=1 {binary} | diff dijkstra_openmp_output.txt -"
        mt_cases = [
            f"OMP_NUM_THREADS={n} {binary} | grep -q 'Normal end of execution'"
            for n in range(2, 4 + 1)
        ]
        return "\n".join([case1] + mt_cases)
