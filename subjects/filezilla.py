#!/usr/bin/env python3
from pathlib import Path
from typing import List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self) -> List[str]:
        result = ["autoconf", "gettext", "libsqlite3-dev", "libgtk-3-dev", "libtool", "wget", "xvfb", "xdg-utils"]
        if self.traits.isa == ISA.x86:
            return result + [
                "libgnutls28-dev:i386",
                "libwxgtk3.0-gtk3-dev:i386",
                "libldap2-dev:i386",
            ]
        return result + [
            "libgnutls28-dev",
            "libwxgtk3.0-gtk3-dev",
            "libdbus-1-dev",
            "libglib2.0-dev",
        ]

    @property
    def run_commands(self) -> List[str]:
        arch_flags = ""
        if self.traits.isa == ISA.x86:
            arch_flags = "--build=i386-pc-linux-gnu --libdir=/usr/lib/i386-linux-gnu CFLAGS=-m32 CXXFLAGS=-m32 LDFLAGS=-m32"
        gnutls_ver = "gnutls-3.6.16"
        result = [
            "update-ca-certificates -f"  # trying to resolve "ERROR: cannot verify raw.githubusercontent.com's certificate" for the following "wget" (observed on "linux/arm/v7")
            f" && wget -O- https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/{gnutls_ver}.tar.xz | tar -xJ"
            f" && cd {gnutls_ver}"
            f" && ./configure --with-included-unistring {arch_flags}"
            " && make && make install"
            f" && cd .. && rm -r {gnutls_ver}"
        ]
        if self.traits.isa == ISA.x86:
            wx_widgets_ver = "wxWidgets-3.0.5"
            result += [
                f"wget -O- https://github.com/wxWidgets/wxWidgets/releases/download/v3.0.5/{wx_widgets_ver}.tar.bz2 | tar -xj"
                f" && cd {wx_widgets_ver}"
                f" && ./configure {arch_flags} --enable-printfposparam --with-gtk"
                " && make && make install"
                f" && cd .. && rm -r {wx_widgets_ver}"
            ]
        return super().run_commands + result


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "filezilla"
    prefix_subdir = Path("inst")
    build_subdir = prefix_subdir / "bin"

    smoke_test_binary = Path("xvfb-run")
    smoke_expected_code = 255
    grep_pattern = "FileZilla"

    afl_fuzz = ""

    @property
    def smoke_args(self) -> List[str]:
        return [self.binary_path.as_posix(), "-v"]

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        pkg_config_path_var = ""
        prefix_dir = (self.repo_dir / self.prefix_subdir).as_posix()
        configure_opts = "--prefix=" + prefix_dir
        if self.traits.isa == ISA.x86:
            lib_dir = "/usr/lib/i386-linux-gnu"
            pkg_config_path_var = f"PKG_CONFIG_PATH={lib_dir}/pkgconfig"
            configure_opts += f" --build=i386-pc-linux-gnu"

        return f"""\
cd libfilezilla
autoreconf -fi
PKG_CONFIG_PATH={pkg_config_path_var} ./configure {configure_opts}
make install
cd ..
PKG_CONFIG_PATH={prefix_dir}/lib/pkgconfig ./configure {configure_opts} --with-pugixml=builtin
make install
"""
