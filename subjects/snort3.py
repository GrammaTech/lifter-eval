#!/usr/bin/env python3
from pathlib import Path
from typing import Dict, List

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, OS


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["bats", "flex"]
        isa_dep = [
            "libpcap-dev",
            "libpcre++-dev",
            "libssl-dev",
            "libhwloc-dev",
            "libluajit-5.1-dev",
            "libz-dev",
            "liblzma-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "snort3"
    common_subdir = Path(name)
    repo_subdir = common_subdir / name

    # needed for functional tests
    test_extra_subdir = common_subdir / "extra"
    test_demo_subdir = common_subdir / "demo"
    evaluate_subdir = test_demo_subdir

    prefix_subdir = Path("inst")
    binary_name = "snort"
    binary_path_in_repo = prefix_subdir / "bin" / binary_name

    smoke_args = ["-V"]
    grep_pattern = "Snort"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def dependency_repo_subdirs(self):
        result = ["libdaq", "libdnet"]
        if self.traits.os == OS.ubuntu16:
            # Ubuntu 16 "libhwloc-dev" has an issue when run in a docker container leading several functional tests to fail.
            # It is fixed in hwloc 1.11.3, see https://stackoverflow.com/questions/46138549/docker-openmpi-and-unexpected-end-of-proc-mounts-line.
            result.append("hwloc")
        return [self.common_subdir / d for d in result]

    @property
    def git_submodules(self) -> List[Path]:
        return (
            super().git_submodules
            + self.dependency_repo_subdirs
            + [self.test_extra_subdir, self.test_demo_subdir]
        )

    @property
    def docker_volumes(self) -> Dict[str, Path]:
        volume_name = f"{self.name}{self.docker_volume_suffix}"
        return {volume_name: self.env.sandbox_path(self.common_subdir)}

    @property
    def build(self) -> str:
        sub_builds = [
            self.__build_dependency(dep) for dep in self.dependency_repo_subdirs
        ] + [self.__build(d) for d in [self.repo_subdir, self.test_extra_subdir]]
        return "\n".join(sub_builds)

    @property
    def prefix(self):
        return self.repo_dir / self.prefix_subdir

    def __build(self, subdir):
        prefix = self.prefix.as_posix()
        return (
            f"cd {self.env.sandbox_path(subdir).as_posix()}\n"
            f"./configure_cmake.sh --prefix={prefix}\n"
            "cd build\n"
            "make\n"
            "make install\n"
        )

    def __build_dependency(self, subdir: Path):
        dep_dir = self.env.sandbox_path(subdir).as_posix()
        return (
            f"cd {dep_dir}\n"
            f"autoreconf -f -i && ./configure --prefix={self.prefix.as_posix()}\n"
            "make\n"
            "make install\n"
        )

    @property
    def build_variables(self):
        base_variables = super().build_variables
        ldflags = base_variables["LDFLAGS"]
        prefix = self.prefix.as_posix()
        return dict(
            PKG_CONFIG_PATH=f"{prefix}/lib/pkgconfig",
            CMAKE_EXE_LINKER_FLAGS=f"{ldflags} -Wl,-rpath={prefix}/lib",
            **base_variables,
        )

    @property
    def evaluate(self) -> str:
        prefix = self.prefix.as_posix()
        return f"SNORT3_DAQ_DIR={prefix}/lib/daq ./run_test.sh {prefix}"
