# Installing MySQL for Windows
The tar archive contains the MySQL installer and a folder with MySQL binaries.    
After installation the binaries will be located at `C:\Program Files\mySQL\bin\`   
## Content of the binaries folder
The `mssql_bin` folder contains all binary files from the `bin` folder of MySQL.
When any of these binaries is updated/modified it should be copied to the MySQL location.
# Building MySQL for Windows
Theoretically it should be easy. :-) MS Visual Studio requires Cmake tool installed as described here - https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio?view=msvc-160 but sometimes the CMake tool does not work properly for unknown reasons. The solution suggested by MS is to uninstall MS Visual Studio, clean up all uninstalled files and install it again.   
Oracle provides no documentation how to build MySQL for Windows and no dependencies described. So the CMake script will display number of errors related to missed packages (f. e. boost) and Windows version of these packages should be found and installed.

