# Building Firefox in the Lifter Docker container
Unfortunately, the Firefox project is too complicated and cannot be prepared and built by the Lifter common way.
Here are the steps how to build Firefox
## Run the Docker container
```
docker run --rm -it --network host -v $(pwd):/lifter-eval registry.gitlab.com/metis/lifter-eval /bin/bash
```
## Install Mercurial
Mozilla’s source code is hosted in Mercurial repositories. You will need Mercurial to download and update the code.
```
python3 -m pip install --user mercurial
```
You can test that Mercurial is installed by running:
```
hg version
```
If your shell is showing `command not found: hg`, then Python’s packages aren’t being found in the `$PATH`. You can resolve this by doing the following
```
export PATH="$PATH:/root/.local/bin"
```
## Bootstrap a copy of the Firefox source code
```
cd /path/to/firefox
curl https://hg.mozilla.org/mozilla-central/raw-file/default/python/mozboot/bin/bootstrap.py -O
apt-get update
python3 bootstrap.py
```
If the script fails because it cannot find packages try the following command
```
apt-get install --fix-missing libasound2-dev libcurl4-openssl-dev libdbus-1-dev libdbus-glib-1-dev libdrm-dev libgtk-3-dev libpulse-dev libx11-xcb-dev libxt-dev xvfb
```
and re-start the bootstrap script.   
Select all default answers when the script asks you about configuration.
## Build Firefox
```
rm bootstrap.py
cd mozilla-unified
./mach build
```
## Run Firefox
Once it is built, you can simply run the browser:
```
obj-x86_64-pc-linux-gnu/dist/bin/firefox
```
But the X Terminal should be connected to the Docker container.   
Also the quick test can be run:
```
obj-x86_64-pc-linux-gnu/dist/bin/firefox --version
```
## Moving Chrome binary to other location
The `firefox` binary cannot start alone. So all content of the `bin` should be copied with the main binary.
## Additional information
Additional information can be found here:
- https://firefox-source-docs.mozilla.org/setup/linux_build.html#building-firefox-on-linux
## Build Firefox for Windows
Build instructions located here - https://firefox-source-docs.mozilla.org/setup/windows_build.html
