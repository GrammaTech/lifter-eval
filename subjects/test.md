# Patch files

A subject program may have an associated patch file "<subject>.patch". It is used to apply our changes to third-party code (if needed for building/testing in our environment). This approach is intended to make it easier to update subject programs while keeping our changes.


# Functional testing

## Already tested
- bitcoin
- libreoffice
- libzmq
- lighttpd
- memcached
- monero
- mosh
- mysql
- nginx
- openssh
- poppler
- proftpd
- redis
- snort3
- vim
- unrealircd
- zip

## Assessment
- anope: no existing test suite and no answer why: https://forum.anope.org/index.php?topic=4427.0
- asterisk: external test suite: https://github.com/asterisk/testsuite. It seems like the test suite is relying on specific pre-existing asterisk configuration which is difficult to deduce without a full dive into asterisk.
- bind: nontrivial setup: bind/bin/tests/system/README. Uses `ip` or `ifconfig` to set up a apecific network. Cannot run in a docker container because ip/ifconfig fail with "operation is not permitted" (even when the container run with `--privileged` option).
- dnsmasq: external test suite, "not yet testing anything exceptional": https://github.com/InfrastructureServices/dnsmasq-tests
- filezilla: has unit tests, other than that "testing is done by using the software" (c) one of the authors: https://forum.filezilla-project.org/viewtopic.php?t=40096#p143979.
- gnome-calculator: has some tests in "gnome-calculator/tests", not documented and not obvious how to run them.
- leafnode: no functional tests, very old
rewritten binary. https://wiki.documentfoundation.org/QA/Testing/Subsequenttests.
- openvpn: nontrivial setup, requires custom configuration. See https://community.openvpn.net/openvpn/wiki/SettingUpBuildslave.
- pidgin: no functional tests
- pks: no functional tests, very old
- postfix: only one binary is used, no functional tests for that binary.
- qmail: no functional tests, very old
- samba: has an extensive long-running test suite, has failures. It is not obvious if it runs against the rewritten binary. `waf configure  --enable-selftest &&  waf test`.
- sendmail: no functional tests.
- sipwitch: no functional tests.
- sqlite: has an extensive test suite but does not test the binary itself (sqlite shell), see https://www.sqlite.org/testing.html. It seems possible to test the dynamic library using "SQLite Test Harness #3": https://sqlite.org/th3.html. Though access to this harness is restricted to the licensed users: https://www.sqlite.org/th3/doc/trunk/www/index.wiki.
- squid: no functional tests that run the binary itself.
- vlc: has unit tests and a very limited test for the final binary ("test/run_vlc.sh").
