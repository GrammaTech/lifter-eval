#!/usr/bin/env python3
from lifter_eval.subject_properties import FortranProperties, SmokeTestWithGrepMixin, register


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "dijkstra"
    grep_pattern = "DIJKSTRA"

    @property
    def evaluate(self) -> str:
        return f"{self.binary_path.as_posix()} | diff dijkstra_output.txt -"
