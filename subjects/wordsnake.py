#!/usr/bin/env python3
from shlex import quote
from lifter_eval.subject_properties import FortranProperties, SmokeTestWithGrepMixin, register


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "wordsnake"
    smoke_stdin = "wordsnake_input.txt"
    grep_pattern = "WORDSNAKE"

    @property
    def evaluate(self) -> str:
        binary = self.binary_path.as_posix()
        # cannot "diff wordsnake_output.txt" because the result are non-deterministic
        return f"{binary} <<< {quote(self.smoke_stdin)} | grep -q 'Total number of characters =      254' -"
