#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, OcamlProperties, register


@register
class Properties(OcamlProperties, SmokeTestWithGrepMixin):
    name = "dune"
    smoke_args = ["--help"]
    grep_pattern = "OCaml"
