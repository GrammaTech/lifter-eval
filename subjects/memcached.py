#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["autoconf", "libevent-dev"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "memcached"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "bin" / name

    smoke_args = ["--version"]
    grep_pattern = "memcached [0-9]"

    evaluate = "prove t/"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        return f"./autogen.sh && ./configure --prefix={prefix} && make && make install"
