#!/usr/bin/env python3
from pathlib import Path
from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        isa_dep = ["libgmp3-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "dnsmasq"
    binary_path_in_repo = Path("src") / name

    smoke_args = ["--version"]
    grep_pattern = "Dnsmasq"

    build = "make all"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
