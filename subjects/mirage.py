#!/usr/bin/env python3
from lifter_eval.subject_properties import OcamlProperties, SmokeTestWithGrepMixin, register


@register
class Properties(OcamlProperties, SmokeTestWithGrepMixin):
    name = "mirage"
    smoke_args = ["--help"]
    grep_pattern = "mirage"
