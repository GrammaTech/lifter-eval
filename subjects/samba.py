#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Pie


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = [
            "bison",
            "flex",
            "libparse-yapp-perl",
            "python3-markdown",
            "python3-dnspython",
        ]
        isa_dep = [
            "libgpgme11-dev",
            "libarchive-dev",
            "libacl1-dev",
            "libdbus-glib-1-dev",
            "libgnutls28-dev",
            "libldap2-dev",
            "libjansson-dev",
            "liblmdb-dev",
            "libpam0g-dev",
            "libpopt-dev",
            "libpython3-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "samba"
    build_subdir = Path("inst")
    binary_path_in_repo = build_subdir / "sbin" / name

    smoke_args = ["--help"]
    grep_pattern = "smb.conf"

    # Passing pie options in CFLAGS/LDFLAGS causes "configure" errors
    cflags_pie = ""
    ldflags_pie = ""

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        pie_arg = "--with-pie" if self.traits.pie == Pie.pie else "--without-pie"
        prefix = (self.repo_dir / self.build_subdir).as_posix()
        # waf -v
        waf = (self.repo_dir / "buildtools" / "bin" / "waf").as_posix()
        return f"{waf} configure --prefix={prefix} {pie_arg} && {waf} && {waf} install"
