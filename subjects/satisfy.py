#!/usr/bin/env python3
from lifter_eval.subject_properties import FortranProperties, SmokeTestWithGrepMixin, register


@register
class Properties(FortranProperties, SmokeTestWithGrepMixin):
    name = "satisfy"
    grep_pattern = "SATISFY"

    @property
    def evaluate(self) -> str:
        return f"{self.binary_path.as_posix()} | diff satisfy_output.txt -"
