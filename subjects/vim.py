#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, Compiler


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["autoconf", "automake", "libtool", "libtool-bin"]
        isa_dep = ["libncurses5-dev"]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "vim"
    prefix_subdir = Path("inst")
    binary_path_in_repo = prefix_subdir / "bin" / name

    smoke_args = ["--version"]
    grep_pattern = "VIM"

    evaluate_subdir = Path(name) / "src" / "testdir"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def cflags(self):
        result = super().cflags
        if self.traits.compiler == Compiler.icx:
            # workaround configure failure
            result.append("-Wno-implicit-int")
        return result

    @property
    def build(self) -> str:
        # does not build on Ubuntu 16 with installed (older?) `libsodium`
        prefix = (self.repo_dir / self.prefix_subdir).as_posix()
        opts = f"--disable-gui --disable-libsodium --prefix={prefix}"
        if self.traits.compiler == Compiler.icx:
            # workaround configure failure
            opts += " --with-tlib=termcap"
        return f"./configure {opts} && make && make install"

    @property
    def evaluate(self) -> str:
        return f"make VIMPROG={self.binary_path.as_posix()} SCRIPTSOURCE=../../runtime"
