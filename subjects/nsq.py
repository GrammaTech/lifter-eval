#!/usr/bin/env python3
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, GoProperties, register
from pathlib import Path


@register
class Properties(GoProperties, SmokeTestWithGrepMixin):
    name = "nsq"
    binary_path_in_repo = Path("build") / "nsqd"
    smoke_args = ["-version"]
    grep_pattern = "nsqd"
