#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register


class DockerImageImpl(SubjectDockerImage):
    apt_packages = ["tcl"]


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "redis"
    binary_path_in_repo = Path("src") / "redis-server"
    build = "make MALLOC=libc"
    smoke_args = ["--version"]
    grep_pattern = "Redis"
    evaluate = "make test"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())
