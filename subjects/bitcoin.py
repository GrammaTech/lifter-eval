#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.docker import SubjectDockerImage
from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register
from lifter_eval.traits import ISA, OS, Optimization, Spec


class DockerImageImpl(SubjectDockerImage):
    @property
    def apt_packages(self):
        common = ["python3-zmq"]
        isa_dep = [
            "libboost-system-dev",
            "libboost-filesystem-dev",
            "libboost-chrono-dev",
            "libboost-program-options-dev",
            "libboost-test-dev",
            "libboost-thread-dev",
            "libboost-iostreams-dev",
            "libevent-dev",
        ]
        if self.traits.isa == ISA.x86:
            isa_dep = [p + ":i386" for p in isa_dep]
        return common + isa_dep


@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    supported_traits = Spec(
        os_list=[OS.ubuntu20], optimizations=[Optimization._0, Optimization._1]
    )

    name = "bitcoin"
    build_subdir = Path("src")
    binary_name = "bitcoind"

    smoke_args = ["--version"]
    grep_pattern = "Bitcoin"

    evaluate = "test/functional/test_runner.py"

    @property
    def building_image(self) -> DockerImageImpl:
        return DockerImageImpl(self.name, self.env.traits())

    @property
    def build(self) -> str:
        opts = "--disable-wallet"
        if self.traits.isa == ISA.x86:
            opts += " --with-boost-libdir=/usr/lib/i386-linux-gnu"
        make_flags = 'CC=$CC CFLAGS="$CFLAGS" AM_DEFAULT_VERBOSITY=1'
        return f"""\
./autogen.sh && ./configure {opts}
make {self.binary_path_in_repo.as_posix()} {make_flags}
make {self.build_subdir.as_posix()}/bitcoin-cli {make_flags} # needed for functional tests
"""
