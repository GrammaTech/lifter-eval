#!/usr/bin/env python3
from pathlib import Path

from lifter_eval.subject_properties import SmokeTestWithGrepMixin, CProperties, register

@register
class Properties(CProperties, SmokeTestWithGrepMixin):
    name = "hello"
    binary_path_in_repo = Path("hello")
    grep_pattern = "Hello World!"

    @property
    def build(self) -> str:
        cflags = " ".join(self.cflags)
        ldflags = " ".join(self.ldflags)
        return f"""\
{self.cc} {cflags} {ldflags} -o {self.binary_path.as_posix()} hello.c
"""
