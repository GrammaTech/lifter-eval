---
title: Binary Lifter Evaluation
author:
  - |
      Eric Schulte\
      eschulte@grammatech.com
  - |
      Vlad Folts\
      vfolts@grammatech.com
  - |
      Michael Brown\
      michael.brown@trailofbits.com
date: 2022-02-10
header-includes: |
    \usepackage{pifont}
    \usepackage{fdsymbol}
    \usepackage{newunicodechar}
    \newunicodechar{⌀}{\ensuremath{\diameter}}
    \newunicodechar{✓}{\ding{51}}
    \newunicodechar{✘}{\ding{55}}
---
<!-- pandoc -s lifter-eval.md --defaults defaults.yaml --self-contained -o lifter-eval.html;open lifter-eval.html -->
<style>
  html { font-size: 12pt; }
  .abstract{ width: 80%; font-size: 11pt; font-style: italic; margin: auto; }
</style>

::: abstract

Binary rewriting gives software developers, consumers, attackers, and
defenders the ability to modify both their own and third-party
software for instrumentation, customization, optimization, and
hardening. Unfortunately, the practical limitations of binary
rewriting tools are often glossed over in academic publications in
this field, making it unclear to users if and when these tools will
work in practice. This, among other challenges, has prohibited the
widespread adoption of binary rewriting tools. To address this
shortcoming, we collect eight popular binary rewriting tools and
assess their *generality* across a broad range of input binary classes
and the functional *reliability* of the resulting rewritten
binaries. Additionally, we evaluate the performance of the rewriting
tools themselves as well as the rewritten binaries they produce. We
also identify features that are predictive of rewriting success and
show that a simple decision tree model trained on these features can
accurately predict whether a particular tool can rewrite a target
binary.

The goal of this broad evaluation is to provide a *state of the
practice* for binary rewriting tools. We hope our findings will inform
potential users of binary rewriting, support binary rewriting tool
developers, and set a shared context for future research in this area.
The binary rewriters, our corpus of 3344 sample binaries, and the
evaluation infrastructure itself are all freely available as
open-source software.

:::

# Introduction

Software supply chains have long been the target of offensive cyber
operations by individual and organized actors alike. These supply chains
often include software libraries and executables available only as
binaries. While the art of reverse engineering binaries is well
established, emerging techniques for analyzing, mutating, hardening, and
exploiting binaries are now being enabled by advances in binary
rewriting.

For binary rewriting tools to be viable, they must *generalize* to the
full variety of programs available on heterogeneous computing
platforms and *reliably* produce functional rewritten binaries. A
surfeit of research into binary rewriting applications including
instrumentation, optimization, configuration, debloating, and
hardening reveals a wide and largely under-emphasized variance in the
generality and reliability of binary rewriting tools
[@romer1997instrumentation; @PSI; @pebil; @williams2020egalito;
@dinesh2019retrowrite; @schwarz2001plto; @diablo; @tilevich2005binary;
@qian2019razor; @bincfi; @secondwrite; @reins].  Frequently, papers
presenting these tools only briefly mention large gaps in generality
such as support limited to binaries with relocation information and/or
symbols -- neither of which are typical of commercial off-the-shelf
(COTS) software [@williams2020egalito;
@dinesh2019retrowrite]. Similarly, many tools support only a single
file format or instruction set architecture (ISA).

While there has been significant research to date seeking to systematize
knowledge of general binary rewriting techniques [@wenzl2019hack] and
evaluate the quality of binary lifters and
disassemblers [@meng2016binary; @andriesse2016], no broad systematic
comparative evaluation of binary rewriters has yet been conducted. In
this work, we address this important knowledge gap by conducting such an
evaluation of 8 binary rewriters across 3344 variant binaries sourced
from 34 benchmark programs and 3 production compilers. The tools
evaluated in this work are *static* binary rewriting tools; we exclude
dynamic binary instrumentation and rewriting tools (e.g., PIN and
DynInst [@luk2005pin; @Dyninst]) whose runtime harnesses and overhead
often make them impractical for the applications considered by this
work.

Our work differs from previous surveys in two key ways. First, prior
work systematizing knowledge on binary rewriters [@wenzl2019hack]
focused primarily on their underlying techniques and algorithms and as
such did not evaluate their artifacts empirically. In contrast, our
evaluation focuses on measuring and comparing the *generality* and
*reliability* of a broad collection of publicly available binary
lifter and rewriter tools.[^no-closed] Second, prior works performing
comparative evaluation of binary disassmblers and lifters
[@meng2016binary; @andriesse2016] focus on *depth* achieving near
complete measurement of binary analysis accuracy across a small pool
of binaries. The difficulties implicit in a truly thorough analysis
limits the breadth of these works to small numbers of binaries or to
specific classes of binaries. Our evaluation focuses on input program
*breadth* to directly address tool *generality* and rewritten binary
functionality to directly address *reliability*.

[^no-closed]: We are not aware of any comparable closed source binary rewriters.

**Summary of Contributions.** In this paper, we first review related
work evaluating binary analysis and transformation tools in Section
@sec:background.  We then describe our experimental methodology to
assess the generality and reliability of existing tools in Section
@sec:methodology.  Next, we present our experimental results and
predictive models derived from them in Section @sec:experimental-results. Finally,
we discuss the state of the practice in binary rewriting tools and the
options for potential users of these tools in Section @sec:conclusion.


# Background

## Types of Binary Rewriters

**Direct Rewriting.** Zipr [@hiser2014framework] lifts a binary into an
Intermediate Representation Data Base (IRDB [@irdb]) upon which
transformations may be applied. The IRDB can then be written directly to
a binary executable. Similarly, Egalito [@williams2020egalito] lifts a
binary into a low-level Intermediate Representation (IR), provides an
API for transforming this IR, and provides support for lowering this IR
back to a functioning binary.

**Reassemblable Disassemblers.** Uroboros [@uroboros] -- superseded by
the Phoenix reassemblable decompiler [@phoenix] -- popularized the idea
of reassemblable disassembly (e.g., disassembly to assembly code that
could be readily reassembled). Retrowrite [@dinesh2019retrowrite] also
emits reassemblable assembly code. DDisasm [@ddisasm] lifts to
GrammaTech's Intermediate Representation for Binaries (GTIRB) [@gtirb],
and can also directly emit assembly code that can be reassembled.

**LLVM Rewriting.** The now defunct SecondWrite [@secondwrite] was the
first tool to lift binaries to LLVM IR. More recently, McSema [@mcsema]
has become the predominant binary lifter to target LLVM IR. Although
LLVM has a large user community and provides many analysis,
optimization, and hardening passes, there are two properties of its IR
that make it difficult for lifters to target. First, it requires *type*
information to represent data. This requires binary type analysis, which
is prohibitively difficult at the scale required to rewrite large
programs. Instead, many tools explicitly *emulate* the stack, stack
maintenance around function calls, and memory as a large array of bytes.
The lack of true types and stack/memory emulation limits the utility of
many existing LLVM passes and results in baroque and inefficient
rewritten binaries. Second, LLVM IR represents code in static single
assignment form resulting in a loss of specificity with respect to the
original binary's concrete machine instructions.

## Related Work

Our work aims to complement and go beyond prior work systematizing
knowledge relevant to and evaluating tools for binary analysis,
disassembly, lifting, and rewriting. We summarize and compare work
related to our own in this subsection.

Andriesse et. al. [@andriesse2016] reviewed 30 prior publications
concerning static disassemblers with the goal of clearly identifying the
actual shortcomings of these tools. Their work also includes an
evaluation of the accuracy of nine disassemblers across 981 binaries.
They found that modern disassemblers achieve close to 100% accuracy
during disassembly, even in the presence of challenging (although rare)
compiler-introduced constructs such as inlining data with code and
overlapping instructions.

Meng and Miller [@meng2016binary] evaluated seven binary analyzers to
determine how their internal algorithms handled eight different
challenging code constructs. This evaluation was conducted across a
corpus of test programs generated from the SPEC 2006 benchmark set
produced at three different optimization levels using two compilers, as
well as a number of handcrafted toy programs. They found that while
challenging code constructs inhibit code discovery, control-flow graph
(CFG) construction, and CFG partitioning, binary analysis algorithms can
be improved or supplemented to handle them.

Pang et. al. [@pang2021sok] published a broad study of x86 and x86-64
disassemblers. Their work first systematizes the the algorithms,
heuristics, and goals of nine open-source disassemblers derived from
source code review. Their work also includes a large scale evaluation of
disassembly accuracy across 3,328 Linux and 460 Windows binaries. They
found that heuristics for handling complex code constructs are
indispensable, but are inherently subject to correctness-coverage trade
offs.

The evaluations presented in these three works are similar in scale to
our work, however our work evaluates the performance of several tools
with their own custom disassembly routines not covered in these works
(all except uroboros). Further, our work focuses on binary rewriting as
opposed to disassembly.

Woodruff et. al. [@woodruff21differential] proposed Mishegos, a tool for
evaluating x86-64 instruction decoders. This work performs a large-scale
evaluation of nine x86-64 instruction decoders via a novel differential
fuzzing method. Instruction decoding is a component function of binary
disassembly, and errors in this task can have cascading effects on
binary disassembly such as misidentifying code as data due to an
erroneous decoding.

Wenzl et. al. [@wenzl2019hack] survey over 50 years of research
referencing binary rewriters in their work. Their primary objective is
to categorize binary rewriting approaches by end use-case, analysis
techniques, code transformation methods, and code generation methods.
While this survey covers 67 prior publications, it does not conduct a
comparative evaluation of the artifacts generated by these works.

Li et. al. [@li2020generation] present a benchmark suite for
evaluating binary disassemblers and an accompanying ground truth
generator. This suite consists of 879 test binaries built using one
Windows and three Linux compilers sourced from 15 different
projects. Additionally, this work provides a comparative evaluation of
four binary disassemblers using this suite. While similar in scale,
provenance, and purpose to our corpus of test binaries, the evaluation
performed in this work is geared towards disassembly rather than
rewriting.

Dasgupta et. al. [@10.1145/3385412.3385964] present a method for
validating binary lifters that achieves scale by avoiding semantics
checks. This tool was used to evaluate McSema [@mcsema] and
successfully identified lifter bugs via reference to the standard
generated by their method. Their work provides a deep evaluation of
one binary lifter over a relatively small pool of benchmark programs,
whereas our work presents a broad comparative evaluation of many tools
across a large set of real-world program variants using a
straightforward lifting and rewriting task as the evaluation
mechanism.

<!--
## Binary Rewriter Evaluations

There have been numerous previous evaluations of tools for binary
analysis, lifting, and rewriting.  *However*, the evaluation presented
herein is distinguished by our focus on *breadth* across many large
real-world programs and a relatively shallow rewriting focused
evaluation across this large breadth.  By contrast much previous work
focuses primarily on deep evaluations of analysis over a relatively
small pool of benchmark subject programs.

> TODO: Brief descriptions of the following previous evaluations.

- [Mcsema: comparison with other machine code to llvm bitcode lifters](https://github.com/lifting-bits/mcsema#comparison-with-other-machine-code-to-llvm-bitcode-lifters)
- [SoK: All You Ever Wanted to Know About x86/x64 Binary Disassembly But Were Afraid to Ask](https://arxiv.org/abs/2007.14266)
- [On the Generation of Disassembly Ground Truth and the Evaluation of Disassemblers](https://dl.acm.org/doi/10.1145/3411502.3418429)
  - [https://github.com/pangine](https://github.com/pangine)
  - [https://github.com/pangine/disasm-benchmark](https://github.com/pangine/disasm-benchmark)
- Scalable Validation of Binary Lifters

## Binary Rewriting Workflows

The following workflows are examples of the applications of binary
rewriting in a secure software development (DevSecOps) environment or
in end user software deployment.

Fuzz Testing
:   Static instrumentation provides for more performant fuzz testing
    than alternatives.  Many binary rewriting tools have used AFL++ as
    a motivating use case.  The AFL++ page on "Fuzzing binary-only
    programs"[^afl-plus-plus-binary-only] provides clear-eyed
    assessments of practical utility of many of the available static
    binary rewriting options.

Hardening
:   Binary rewriting may be used to add control flow protections such
    as CFI around indirect control flow in binaries and to add memory
    protections around indirect memory access in binaries.

Debloating
:   Debloating a binary is the process of removing unused code and
    data.  Reducing a binary reduces the attack surface effectively
    securing the binary.  There can also be performance benefits from
    binary reduction.

Configuration and Customization
:   In addition to removing unwanted functionality in some environment
    it may be useful to "hard code" critical configurations as part of
    the software deployment process.

Optimization
:   Optimization of binaries through binary rewriting is popular with
    approaches ranging from link time optimization (LTO) to end-user
    work load specific profile guided optimization (PGO) to larger
    scale program refactorings.

[^afl-plus-plus-binary-only]: https://aflplus.plus/docs/binaryonly_fuzzing/#mcsema
-->

# Methodology

## Tools

We selected eight binary rewriters for our evaluation, listed in Table
@tbl:tools.  While our corpus of tools is not exhaustive, it provides
excellent coverage of tools that are mature, robust, and scale via
automation. We excluded some notable binary rewriting tools,
McSema[@mcsema] and Ramblr[@ramblr], from our evaluation because their
rewriting workflows involve manual steps and/or lack interfaces that
scale via automation.  Additionally, the tools evaluated in this work
are all publicly available as open source software; we are not aware
of any closed source binary rewriting tools.

```{=latex}
{\tiny
```

Table: Tools selected for this evaluation and their ISA coverage. {#tbl:tools}

| Tool                               | x86-64 Linux/ELF | x86-32 Linux/ELF | x86-64 Windows/PE | x86-32 Windows/PE | ARM64 Linux/Elf | ARM32 Linux/ELF | Mips64 Linux/Elf | Mips32 Linux/Elf | PPC64 Linux/Elf | PPC32 Linux/Elf |
|------------------------------------|------------------|------------------|-------------------|-------------------|-----------------|-----------------|------------------|------------------|-----------------|-----------------|
| DDisasm  [@ddisasm]                | ✓                | ✓                | ✓                 | ✓                 | ✓               | ✓               | ✘                | ✓                | ✘               | ✘               |
| Egalito [@williams2020egalito]     | ✓                | ✘                | ✘                 | ✘                 | ✓               | ✘               | ✘                | ✘                | ✘               | ✘               |
| McToll [@mctoll]                   | ✓                | ✘                | ✘                 | ✘                 | ✘               | ✘               | ✘                | ✘                | ✘               | ✘               |
| multiverse [@superset]             | ✓                | ✓                | ✘                 | ✘                 | ✘               | ✘               | ✘                | ✘                | ✘               | ✘               |
| Retrowrite [@dinesh2019retrowrite] | ✓                | ✘                | ✘                 | ✘                 | ✓               | ✘               | ✘                | ✘                | ✘               | ✘               |
| Uroboros [@uroboros]               | ✓                | ✓                | ✘                 | ✘                 | ✘               | ✘               | ✘                | ✘                | ✘               | ✘               |
| Zipr [@hiser2014framework]         | ✓                | ✓                | ✓                 | ✓                 | ✓               | ✓               | ✓                | ✓                | ✓               | ✓               |

```{=latex}
}
```

## Benchmark Selection and Variant Generation

In order to obtain realistic evaluation results, we combined benchmark
lists compiled by two program managers from the United States Department
of Defense to arrive at a diverse list of 33 real-world benchmark
programs. We added a trivial "Hello World!" program to our corpus to
provide a low-water mark for program complexity, resulting in a total of
34 benchmarks shown in Table @tbl:benchmarks.

Table: Benchmark programs with size and number of binaries {#tbl:benchmarks}

Program                                                                           SLOC   Bins Description
-------------------------------------------------------------------------- ----------- ------ --------------------------
[anope](https://github.com/anope/anope)                                         65,441    176 IRC Services
[asterisk](https://www.asterisk.org/)                                          771,247    120 Communication Framework
[bind](https://www.isc.org/bind/)                                              376,147     97 DNS System
[bitcoind](https://en.bitcoin.it/wiki/Bitcoind)                                229,928    104 Bitcoin Client
[dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html)                        34,671    176 Network Services
[filezilla](https://filezilla-project.org/)                                    176,324    120 FTP Client and Server
[gnome-calculator](https://gitlab.gnome.org/GNOME/gnome-calculator)                301     20 Calculator
hello world                                                                          5    148 Hello World
[leafnode](http://leafnode.sourceforge.net/download.shtml)                      12,945    180 NNTP Proxy
[Libreoffice](https://github.com/LibreOffice/core)                           5,090,852     44 Office Suite
[libzmq](https://github.com/zeromq/libzmq)                                      62,442     67 Messaging Library
[lighttpd](https://www.lighttpd.net/)                                           89,668     90 Web Server
[memcached](https://github.com/memcached/memcached)                             33,533     84 In-memory Object Cache
[monerod](https://getmonero.org/resources/user-guides/vps_run_node.html)       394,783    148 Blockchain Daemon
[mosh](https://mosh.org/)                                                       12,890    176 Mobile Shell
[mysql](https://github.com/mysql/mysql-server)                               3,331,683    110 SQL Server
[nginx](https://nginx.org/en/)                                                 170,602    176 Web Server
[ssh](https://github.com/openssh/openssh-portable)                             127,363     88 SSH Client and Server
[openvpn](https://openvpn.net/)                                                 89,312    180 VPN Client
[pidgin](https://developer.pidgin.im/wiki/UsingPidginMercurial)                259,398    180 Chat Client
[pks](http://pks.sourceforge.net/)                                              40,788    176 Public Key Server
[poppler](https://poppler.freedesktop.org)                                     188,156     61 PDF Reader
[postfix](http://mirror.jaleco.com/postfix-release/)                           134,957    180 Mail Server
[proftpd](http://www.proftpd.org/)                                             544,178    180 FTP Server
[qmail](http://cr.yp.to/qmail.html)                                             14,685    152 Message Transfer Agent
[redis](https://redis.io/)                                                      14,685     90 In-memory Data Store
[samba](https://www.samba.org/)                                              1,863,980    126 Windows Interoperability
[sendmail](https://www.proofpoint.com/us/open-source-email-solution)           104,450    176 Mail Server
[sipwitch](http://www.gnu.org/software/sipwitch/)                               17,134    128 VoIP Server
[snort](https://www.snort.org/)                                                344,877    120 Intrusion Prevention
[sqlite](https://sqlite.org/index.html)                                        292,398    180 SQL Server
[squid](http://www.squid-cache.org/)                                           212,848    176 Caching Web Proxy
[unrealircd](https://www.unrealircd.org/)                                       90,988    132 IRC Server
[vi/vim](https://github.com/vim/vim)                                           394,056    180 Text Editor
[zip](http://infozip.sourceforge.net/Zip.html#Sources)                          54,390    176 Compression Utility

For each of our 34 benchmarks, we compiled an x86-64 variant of the
program using one permutation of the compilers, optimization levels,
code layout, symbol options, and operating systems listed in Table
@tbl:perms. The total size of our corpus of distinct x86-64 program
variants is 3344.

Table: Variant configuration options. {#tbl:perms}


| Compiler | Flags     | Relocation (Position-) | Symbols      | Operating Systems             |
|----------|-----------|------------------------|--------------|-------------------------------|
| clang    | O0        | Independent            | Not Stripped | Ubuntu 16.04[^unavailable-16] |
| gcc      | O1        | Dependent              | Stripped     | Ubuntu 20.04                  |
| icx      | O2        |                        |              |                               |
|          | O3        |                        |              |                               |
|          | Os        |                        |              |                               |
|          | Ofast     |                        |              |                               |
|          |           |                        |              |                               |
| OLLVM    | fla       | Independent            | Not Stripped | Ubuntu 20.04                  |
|          | sub       | Dependent              | Stripped     |                               |
|          | bcf[^bcf] |                        |              |                               |

[^unavailable-16]: Some binaries could be built on this OS due to unavailable dependencies.
[^bcf]: Probability variable set to always insert (100%)

## Evaluation Tasks {#sec:eval-tasks}

We evaluate our selected binary rewriters based on their ability to
successfully perform two rewriting *tasks* and record their progress at
multiple *checkpoints*. In the interest of breadth we use a proxy for
successful (i.e., correct) rewriting. Specifically, we consider a
rewrite to be successful if the output executable passes a very simple
test suite. In practice many binary rewriting tools fail fast when
problems arise, meaning they either completely fail to produce a new
executable or they produce a executable that is unable to start
execution. For a small subset of our benchmark with readily executable
test suites with high coverage we also tested programs against the full
test suite.

**Tasks.** The two tasks we use to evaluate our tools are:

NOP

:   This task is a minimal NOP (i.e., No Operation) transform that
    simply lifts the binary and then rewrites without modification. The
    NOP transform tests the ability of a binary rewriter to successfully
    process the input binary, populate its internal or external
    intermediate representation of the binary, and then produce a new
    rewritten executable. Despite its name this transform is decidedly
    non-trivial for most rewriters, evidenced by the fact that rewritten
    binaries typically look very different from the original due to the
    undecidable nature of binary disassembly.

AFL

:   This task is a more complex transform characteristic of the needs
    of instrumentation to support gray-box fuzz testing. It evaluates
    our tools' ability to extensively *transform* a binary with
    instrumentation to support AFL++ [@aflpp].[^no-multiverse] This
    task is important to include as many rewriters cover up analysis
    errors by incorporating reasonable defaults (e.g., linking code
    from the original binary on lifting failure, or preserving
    unidentified symbols which continue to resolve correctly if code
    is left undisturbed in memory).

[^no-multiverse]: Each tool we selected except multiverse claims to support AFL++'s instrumentation.

**Checkpoints.** For every attempted rewrite operation, we collect a
subset of the following artifacts to checkpoint multiple stages of the
process:

IR

:   For every binary rewriting tool that leverages some form of external
    IR, we collect that IR at this checkpoint. Specifically, we collect
    the ASM files generated by tools that emit reassemblable disassembly
    and the LLVM IR for tools targeting LLVM. Zipr and Egalito use a
    purely internal IR that are not easily serialized to disk. As such,
    we do not track successful IR generation for these tools.

EXE

:   We next check if the rewriter successfully creates a new executable.
    In some cases rewritten executables are trivially recreated and are
    not an indicator of success (e.g., Egalito almost always generates a
    new executable even if most of them are non-functional). However, in
    most cases the ability to re-assemble and re-link a new executable
    indicates that the rewriting tool both successfully disassembled
    reasonable assembly instructions and generated the required
    combinations of symbols and included libraries.

## Evaluation Metrics

**Functional Metrics.** To measure rewriter correctness, we first
observe the rewrite success rate for each tool across all variants for
both tasks (i.e., NOP and AFL) at both checkpoints (i.e., IR and EXE).
Next, we perform a simple invocation of the NOP rewritten programs
(e.g., running `help`) to ensure the rewrite did not obviously corrupt
the program. We refer to this test as the *Null Function* test. Finally,
we execute the AFL rewritten programs with the driver provided by AFL++
to ensure instrumenting the program via binary rewriting did not corrupt
the program and that instrumentation was successfully incorporated. We
refer to this test as the *AFL Function* test.

**Non-Functional Metrics.** To measure rewriter runtime performance we
observe the total required runtime and the memory high-water mark used
by tools during rewriting. This is important as the limiting factor for
rewriting is often the amount of available memory because many
underlying analyses scale super-linearly in the input binary size.

To determine the performance impacts of rewriting on binaries, we first
measure file size impacts using Bloaty [@bloaty]. Size is an important
metric as it measures the degree to which a rewriting tool has inserted
dynamic emulation or runtime supports to supplement static binary
rewriting that have complexity and efficiency costs. Finally, for
successfully rewritten program variants with publicly available test
suites we measure the impact of rewriting on performance. Specifically,
we measure pass rate for all tests in the suite, runtime of the test
suite, and the memory consumption high-water mark during execution of
the full test suite.

# Experimental Results

We present binary rewriter success both in aggregate across our entire
benchmark set and broken out into cohorts. Each cohort of binaries has
like characteristics that highlight the comparative strengths and
weaknesses of the evaluated tools.

## Aggregate Rewriting Success Rates

Table: Number and percentage of the full suite of 3344 x64 Linux binaries for which the rewriter successfully produces IR, produces a NOP-transformed executable ("EXE"), passes the Null Function test, produces an AFL++ instrumented executable ("AFL EXE"), and passes the AFL Function test {#tbl:aggregate_success}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 3282   | 2972   | 2861       | 3020    | 2346      |
| %          | 98.05% | 88.80% | 85.48%     | 90.23%  | 70.09%    |
| egalito    | NA     | 3294   | 983        | 2493    | 0         |
| %          | NA     | 98.42% | 29.37%     | 74.48%  | 0.00%     |
| mctoll     | 30     | 30     | 30         | 30      | 30        |
| %          | 0.90%  | 0.90%  | 0.90%      | 0.90%   | 0.90%     |
| multiverse | NA     | 880    | 362        | NA      | NA        |
| %          | NA     | 26.29% | 10.82%     | NA      | NA        |
| reopt      | 3007   | 2556   | 1134       | 0       | 0         |
| %          | 89.84% | 76.37% | 33.88%     | 0.00%   | 0.00%     |
| retrowrite | 817    | 334    | 309        | 330     | 254       |
| %          | 24.41% | 9.98%  | 9.23%      | 9.86%   | 7.59%     |
| uroboros   | 364    | 216    | 96         | 210     | 0         |
| %          | 10.88% | 6.45%  | 2.87%      | 6.27%   | 0.00%     |
| zipr       | NA     | 3059   | 2719       | 2705    | 1592      |
| %          | NA     | 91.40% | 81.24%     | 80.82%  | 47.56%    |

Our aggregate success results are presented in . Overall, we observed a
very broad range of success rates (and by extension levels of support)
achieved by our selected binary rewriting tools. For the NOP transform,
the tools fall into four distinct categories characterized by the
fraction of the universe of potential binaries they can handle:

1.  Tools that work only on a tiny fraction ($\le$`<!-- -->`{=html}5%)
    of binaries. This group includes mctoll and uroboros.

2.  Tools which work on a few ($\sim$`<!-- -->`{=html}10%) binaries.
    This group includes multiverse and retrowrite.

3.  Tools that work on some ($\sim$`<!-- -->`{=html}33%) binaries. This
    group includes egalito and reopt.

4.  Tools that work on most ($\ge$`<!-- -->`{=html}80%) binaries. This
    group includes ddisasm and zipr.

In category (1) we find tools that only handle a very limited set of
binaries. For example mctoll only successfully transformed
` hello-world` binaries. The tools in category (2) support a wider range
of binaries but in many cases make hard and fast assumptions. For
example, multiverse is only able to successfully rewrite binaries
compiled by the older versions of clang and gcc available on Ubuntu 16.
These tools still do not handle binaries that make use of fairly common
code structures (e.g., C++ exceptions). The tools in category (3)
largely only work with relocation and debug information, but are able to
handle a wide range of the binaries meeting these restrictions. Finally,
category (4) tools do *not* require relocation or debug information and
support a wide range of both complex code structures and
compiler-specific binary generation behaviors such as multiple forms of
jump tables, data intermixed with code, and specialized control-flow
constructs.

Given that so many tools require relocation and debug information we
present a second view of our results limited to binaries that include
this information (i.e., non-stripped, position-independent variants) in
. Although position-independent binaries are increasingly common as ASLR
becomes the norm, it is still quite uncommon for COTS binaries to
include debug information.

Table: Number and percentage of 1673 non-stripped, position-independent x64 Linux binaries for which the rewriter successfully reaches task checkpoints and passes functional tests. {#tbl:easy}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 1638   | 1428   | 1373       | 1456    | 1125      |
| %          | 97.91% | 85.36% | 82.07%     | 87.03%  | 67.24%    |
| egalito    | NA     | 1661   | 492        | 1261    | 0         |
| %          | NA     | 99.28% | 29.41%     | 75.37%  | 0.00%     |
| mctoll     | 30     | 30     | 30         | 30      | 30        |
| %          | 1.79%  | 1.79%  | 1.79%      | 1.79%   | 1.79%     |
| multiverse | NA     | 437    | 181        | NA      | NA        |
| %          | NA     | 26.12% | 10.82%     | NA      | NA        |
| reopt      | 1504   | 1425   | 684        | 0       | 0         |
| %          | 89.90% | 85.18% | 40.88%     | 0.00%   | 0.00%     |
| retrowrite | 817    | 334    | 309        | 330     | 254       |
| %          | 48.83% | 19.96% | 18.47%     | 19.73%  | 15.18%    |
| uroboros   | 183    | 108    | 48         | 105     | 0         |
| %          | 10.94% | 6.46%  | 2.87%      | 6.28%   | 0.00%     |
| zipr       | NA     | 1529   | 1359       | 1560    | 1071      |
| %          | NA     | 91.39% | 81.23%     | 93.25%  | 64.02%    |

The results in show the increase in rewriting success rate that a
developer might expect if they compile their binaries with relocation
and debug information to support binary rewriting. Unsurprisingly, both
Retrowrite and Zipr do much better in this case. However, such binaries
are not characteristic of the stripped COTS binaries likely to be
received from third parties or found in the wild.

As shown by our results in Tables @tbl:aggregate_success and
@tbl:easy, the AFL transform provides a much better proxy for actual
performance of a binary rewriters than the NOP transform. This is true
for at least two reasons. First, when applying the NOP transform many
relative and absolute locations in a rewritten binary will continue to
match their locations in the original binary because no attempt is
made to modify the lifted code. This provides a great deal of grace
for rewriters that missed code or symbols in the original binary
because symbols treated as literals or as data in NOP transformed
binaries remains sound surprisingly often. Second, the AFL test is
stricter because the rewritten binary actually has to interact with
the AFL++ harness to record a successful execution.

Every rewriter included in this evaluation except multiverse provides
some out-of-the-box support for AFL++ instrumentation. ddisasm,
retrowrite, and uroboros all produce assembly-level IR that an AFL++
provided tool can directly instrument. Similarly mctoll and reopt both
produce LLVM IR that an AFL++ provided LLVM pass can directly
instrument. Both egalito and zipr do not expose their proprietary IRs
but do ship with an AFL++ instrumentation pass compatible with their
frameworks.

Despite this broad support, only ddisasm, mctoll, retrowrite, and zipr
successfully transform *any* of our test binaries for use with AFL++. In
Egalito's case, the included AFL++ transform requires a patched
`afl-fuzz` program
[@egalito-afl-readme; @egalito-afl-readelf; @egalito-afl-setup]. Reopt
appears to produce LLVM IR that is not suitable for transformation.
Further, it appears reopt may only perform well on the NOP transform
because it falls back to directly re-linking sections of the original
binary when rewriting fails. Uroboros appears to fail to produce any
functional AFL transformed binaries not due to any uniform systematic
reason but simply because the rewritten assembly code is very brittle
due to incorrect analyses during lifting.

## Rewriting Success Rates by Compiler

In this section we present the success rates of our binary rewriters
broken out by the compiler used to generate variants. Binary rewriting
success is often dependent on the compiler used to produce the input
binary as many of the heuristics baked into rewriting tools target
binary code generation logic or optimizations specific to certain
compilers. For example, the Intel compiler (`icx`) in-lines data into
the code section on Linux whereas Clang and GCC do not. As a result of
this behavior and other ICX-specific optimizations, some binary
rewriters have a significantly lower success rate against ICX-produced
binaries.

The results restricted to GCC-compiled variants in are most similar to
the aggregate results. This is unsurprising as GCC is the prototypical
compiler for Linux systems and represents a middle ground in
optimization aggressiveness between the relatively conservative Clang
optimizations and the very aggressive Intel optimizations.

Interestingly, the success rate for Clang-compiled variants () across
all tools is largely similar to but slightly higher than GCC's success
rate. This could be due to a number of factors including programs more
frequently including GCC-only optimizations that prove difficult for
binary rewriting tools, GCC leveraging non-standard ELF file format
extensions that are not produced by clang, or GCC performing more
aggressive optimizations than Clang in some cases.

Our ICX-compiled results are shown in . They vary widely from both GCC
and Clang and also across our evaluated tools. DDisasm performs better
on ICX binaries generating an IR in 99% of cases and generating
functional AFL rewrites 2% more frequently with these binaries than in
aggregate. By contrast, Egalito's NOP transform success rate drops for
ICX-produced variants, mctoll, Multiverse, and Uroboros are unable to
process any ICX binaries, and Retrowrite and Zipr both perform
significantly worse on ICX binaries although they are still able to
successfully generate functional AFL++ instrumented binaries in some
cases.

Although we expected the tools to perform worst against ollvm compiled
binaries, we were surprised to see that in most cases the binary
rewriting success rate increased for these binaries. It is not clear if
this is because those programs which could be compiled with ollvm
represent the simpler end of our benchmark set, or if there is something
about the ollvm transformations that are amenable to binary rewriting if
not to traditional reverse engineering.


Table: Number and percentage of 1280 GCC-compiled x64 Linux binaries for which the rewriter successfully reaches task checkpoints and passes functional tests. {#tbl:gcc}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 1237   | 1146   | 1100       | 1157    | 871       |
| %          | 96.64% | 89.53% | 85.94%     | 90.39%  | 68.05%    |
| egalito    | 0      | 1257   | 235        | 1113    | 0         |
| %          | NA     | 98.20% | 18.36%     | 86.95%  | 0.00%     |
| mctoll     | 16     | 16     | 16         | 16      | 16        |
| %          | 1.25%  | 1.25%  | 1.25%      | 1.25%   | 1.25%     |
| multiverse | 0      | 438    | 176        | 0       | 0         |
| %          | NA     | 34.22% | 13.75%     | 0.00%   | 0.00%     |
| reopt      | 1168   | 874    | 387        | 0       | 0         |
| %          | 91.25% | 68.28% | 30.23%     | 0.00%   | 0.00%     |
| retrowrite | 308    | 129    | 124        | 126     | 99        |
| %          | 24.06% | 10.08% | 9.69%      | 9.84%   | 7.73%     |
| uroboros   | 150    | 92     | 16         | 92      | 0         |
| %          | 11.72% | 7.19%  | 1.25%      | 7.19%   | 0.00%     |
| zipr       | 0      | 1188   | 1164       | 1030    | 674       |
| %          | NA     | 92.81% | 90.94%     | 80.47%  | 52.66%    |


Table: Number and percentage of 1176 Clang-compiled x64 Linux binaries for which the rewriter successfully reaches task checkpoints and passes functional tests. {#tbl:clang}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 1167   | 1034   | 984        | 1057    | 799       |
| %          | 99.23% | 87.93% | 83.67%     | 89.88%  | 67.94%    |
| egalito    | 0      | 1161   | 494        | 1052    | 0         |
| %          | NA     | 98.72% | 42.01%     | 89.46%  | 0.00%     |
| mctoll     | 11     | 11     | 11         | 11      | 11        |
| %          | 0.94%  | 0.94%  | 0.94%      | 0.94%   | 0.94%     |
| multiverse | 0      | 442    | 186        | 0       | 0         |
| %          | NA     | 37.59% | 15.82%     | 0.00%   | 0.00%     |
| reopt      | 1057   | 901    | 463        | 0       | 0         |
| %          | 89.88% | 76.62% | 39.37%     | 0.00%   | 0.00%     |
| retrowrite | 288    | 147    | 136        | 146     | 108       |
| %          | 24.49% | 12.50% | 11.56%     | 12.41%  | 9.18%     |
| uroboros   | 144    | 92     | 56         | 86      | 0         |
| %          | 12.24% | 7.82%  | 4.76%      | 7.31%   | 0.00%     |
| zipr       | 0      | 1021   | 999        | 1010    | 664       |
| %          | NA     | 86.82% | 84.95%     | 85.88%  | 56.46%    |

Table: Number and percentage of 646 ICX-compiled x64 Linux binaries for which the rewriter successfully reaches task checkpoints and passes functional tests. {#tbl:icx}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 641    | 561    | 552        | 574     | 468       |
| %          | 99.23% | 86.84% | 85.45%     | 88.85%  | 72.45%    |
| egalito    | 0      | 636    | 136        | 119     | 0         |
| %          | NA     | 98.45% | 21.05%     | 18.42%  | 0.00%     |
| mctoll     | 0      | 0      | 0          | 0       | 0         |
| %          | 0.00%  | 0.00%  | 0.00%      | 0.00%   | 0.00%     |
| multiverse | 0      | 0      | 0          | 0       | 0         |
| %          | NA     | 0.00%  | 0.00%      | 0.00%   | 0.00%     |
| reopt      | 575    | 571    | 173        | 0       | 0         |
| %          | 89.01% | 88.39% | 26.78%     | 0.00%   | 0.00%     |
| retrowrite | 157    | 24     | 23         | 24      | 18        |
| %          | 24.30% | 3.72%  | 3.56%      | 3.72%   | 2.79%     |
| uroboros   | 4      | 0      | 0          | 0       | 0         |
| %          | 0.62%  | 0.00%  | 0.00%      | 0.00%   | 0.00%     |
| zipr       | 0      | 622    | 340        | 479     | 144       |
| %          | NA     | 96.28% | 52.63%     | 74.15%  | 22.29%    |


Table: Number and percentage of 244 Ollvm compiled and obfuscated x64 Linux binaries for which the rewriter successfully reaches task checkpoints and passes functional tests. {#tbl:ollvm}

| Tool       | IR     | ELF    | Null Func. | AFL ELF | AFL Func. |
|------------|--------|--------|------------|---------|-----------|
| ddisasm    | 237    | 231    | 225        | 232     | 208       |
| %          | 97.13% | 94.67% | 92.21%     | 95.08%  | 85.25%    |
| egalito    | 0      | 240    | 118        | 209     | 0         |
| %          | NA     | 98.36% | 48.36%     | 85.66%  | 0.00%     |
| mctoll     | 3      | 3      | 3          | 3       | 3         |
| %          | 1.23%  | 1.23%  | 1.23%      | 1.23%   | 1.23%     |
| multiverse | 0      | 0      | 0          | 0       | 0         |
| %          | NA     | 0.00%  | 0.00%      | 0.00%   | 0.00%     |
| reopt      | 207    | 210    | 111        | 0       | 0         |
| %          | 84.84% | 86.07% | 45.49%     | 0.00%   | 0.00%     |
| retrowrite | 64     | 34     | 26         | 34      | 29        |
| %          | 26.23% | 13.93% | 10.66%     | 13.93%  | 11.89%    |
| uroboros   | 66     | 32     | 24         | 32      | 0         |
| %          | 27.05% | 13.11% | 9.84%      | 13.11%  | 0.00%     |
| zipr       | 0      | 228    | 216        | 186     | 110       |
| %          | NA     | 93.44% | 88.52%     | 76.23%  | 45.08%    |

## Analysis of Binary Rewriter Success

In this section, we investigate binary formatting options to determine
if they are correlates for binary rewriter success. We collected
features that may be relevant to binary rewriter performance and are
readily available using ` readelf` from standard binutils. Specifically
we collect:

1.  **Position independence**

        readelf -h $elf|grep Type|awk '{print $2}'

2.  **Stripped or unstripped**

        readelf -SW $elf|grep -q .symtab \
          && echo unstripped || echo stripped

3.  **Included sections**

        readelf -SW $elf|grep "[[:digit:]]] \."\
            |sed 's/^.* \././;s/ .*$//'

Note that for included sections we eliminate all sections which appeared
in every one of our binaries (e.g., `.text`) and any section which
appeared only in binaries from a single program (e.g.,
`.gresource.gnome_calculator`).

We collated the success and failure rate across these features for each
tool against our corpus of variants considering both rewriting tasks
(i.e., NOP and AFL). Then we identified the four most predictive
features for rewriting success or failure of the AFL transform for each
tool. These features are presented in . In many cases these features are
expected and match the advertised capabilities of each tool. For
example, retrowrite only supports relocatable (i.e., `pi`) and
non-stripped (i.e., `strip`) binaries which are its two most predictive
features for success.

Next we train a decision tree based on this feature collection to
predict the likelihood of success of each rewriter against an example
binary when using the AFL transform. Before training we use linear
support vector classification to select the most discriminating features
for that rewriter. The resulting decision trees are printed as Python
code in Appendix [6.1](#trees){reference-type="ref" reference="trees"}.
We evaluate the resulting decision tree using 70% of our binaries for
training and reserving 30% for testing. The accuracy of the resulting
tree is shown in .

Table: Decision tree accuracy predicting binary rewriting success based on simple binary features {#tbl:tree}

      Rewriter      NOP AFL
  ------------ -------- --------
       ddisasm   90.03% 81.47%
       egalito   87.15% 
        mctoll   98.80% 98.80%
    multiverse   97.80% 
         reopt   67.82% 
    retrowrite   94.32% 93.02%
      uroboros   96.31% 
          zipr   86.65% 79.98%

As shown in the resulting decision trees, despite their reliance on very
simple binary features were very accurate in predicting the changes of
tool success. We anticipate two benefits from this analysis. First, tool
developers will have insight into properties of binaries that cause
their rewriting tools to fail. Second, users can nearly instantaneously
run a combination of `readelf` and our decision tree to see what tools,
if any, will reliably transform a given target binary. This is useful
when many binary rewriting tools can run for minutes and even hours on a
single binary. The success of this simple machine learning model trained
on simple inputs indicates promising new directions for the practical
application of binary rewriting technology discussed in Section
[5](#sec:conclusion){reference-type="ref" reference="sec:conclusion"}.
The decision trees and the code used to build and train them are
included in our publicly available artifact repository [@lifter-eval].

## Size of Rewritten Binaries

shows the size of rewritten binaries as a percentage of the size of the
original. A value of 100% means that the rewritten binaries are exactly
the same size as the originals, 50% means they are half the size of the
originals, 200% means they are twice the size of the originals, and so
on. The change in the size of binaries reflects the design decisions
made by the rewriting engine and can impact the utility, efficiency, and
potential use cases for the rewritten binary.

Table: Average size of rewritten binaries as a percentage of the original binary's size. {#tbl:size}

| Tool       | Rewritten subject size increase |
|------------|---------------------------------|
| ddisasm    | 91.05%                          |
| egalito    | 168.45%                         |
| mctoll     | 128.22%                         |
| multiverse | 871.76%                         |
| retrowrite | 80.65%                          |
| uroboros   | 148.97%                         |
| zipr       | 139.94%                         |

In most cases the rewritten binary is close to or slightly larger than
the the original.  To investigate the specific causes  of size changes
we collected  the changes in size  per section per rewriting  tool in.
Note that in  many cases rewriting tools break elf  section tables. In
these cases bloaty [@bloaty], the tool we use to collect section size,
is  unable  to  determine  sizes for  that  section  in  corresponding
binaries. In nearly every rewriting  tool the largest increase in size
of the elf file  is in unmapped bytes or bytes  that are not accounted
for by the section table. This is likely due to at least the following
two factors.  First, because any extra  non-standard runtime harnesses
or extra rewriting-specific supports are not properly entered into the
section table of the rewritten  binary. Second, binary rewriting tools
are  not penalized  for dropping  sections  or breaking  parts of  the
section header table that are not required for execution.

::: table*
           Section            ddisasm    egalito    mctoll   multiverse     reopt   retrowrite   uroboros     zipr
  ------------------------- --------- ---------- --------- ------------ --------- ------------ ---------- --------
          .got.plt             99.96%    100.08%      100%           NA   100.38%         100%     99.69%       NA
            .data             100.42%    100.42%        NA           NA   100.02%      100.16%    101.37%       NA
          .dynamic             98.31%     64.95%    94.93%           NA   100.01%      101.46%    100.01%       NA
          .rela.dyn            98.41%   1666.59%      100%           NA   100.08%       99.94%     97.61%       NA
           .strtab            106.11%     89.53%    97.05%           NA    98.94%      108.96%    104.05%       NA
           .dynsym             95.24%    100.15%      100%           NA   100.75%       87.06%     99.58%       NA
           .dynstr             95.50%    100.41%      100%           NA   100.77%       97.13%     99.83%       NA
           .symtab            116.62%         NA    96.61%           NA   100.29%       94.72%         NA       NA
        .eh_frame_hdr         100.21%         NA        NA           NA    99.70%       93.33%    100.93%       NA
            .plt               99.93%    100.14%      100%           NA   100.38%       99.89%     99.90%       NA
          .rela.plt            99.92%     99.94%        NA           NA   100.43%       99.89%       100%       NA
          .eh_frame            99.81%         NA    98.27%           NA    98.44%       93.33%    101.43%       NA
   \[ELF-Program-Headers\]     96.18%     94.24%    92.76%           NA    94.47%       102.23     96.85%       NA
   \[ELF-Section-Headers\]     97.37%     72.16%    95.98%           NA   103.67%       93.48%     97.30%       NA
           .rodata            100.07%    100.47%        NA           NA   100.00%      100.05%    100.03%       NA
            .text              99.18%    108.76%   103.63%           NA   100.66%       96.93%     97.51%       NA
        \[Unmapped\]          130.21%    673.91%   350.39%           NA   181.97%      225.88%    165.30%   10.17%
:::

For ddisasm and retrowrite the rewritten binaries are slightly smaller
on average. This is likely due to symbol and debug information being
dropped by the rewriting process. For Egalito, mctoll, uroboros, and
zipr the size of the binary increases by a non-trivial amount.

Multiverse is the outlier with rewritten binaries that are nearly nine
times the size of the original. This is due to the defining design
decision of Multiverse which is that it reassembles all *possible*
disassemblies of the original binary, leading to much more code in the
rewritten binary.

It is important to note that the percentages reported in report the size
increase for a different domain of binaries for every tool. Specifically
they are calculated for binaries the tool can successfully rewrite. As a
result, comparing tools with respect to the code size increases in this
table is not a direct comparison. Thus, we include to report comparative
results for every pair of tools. For each pair of tools we report the
relative size of the binaries in the intersection of those programs
which are successfully rewritten by both tools. In each cell, the
percentage of the successfully rewritten binary sizes by both tools are
calculated as a ratio of the row tool to the column tool. For example,
multiverse rewritten binaries are just over 9 times bigger on average
than ddisasm rewritten binaries. An entry of "NA" indicates that *no*
binaries were successfully rewritten by both tools.

Table: Comparative size of rewritten binaries between rewriting tools.  In each cell, the ratio of the rewritten sizes of those benchmark binaries successfully rewritten by both tools are given as the row tool as a percent of the size of the column tool.  So, e.g., multiverse binaries are just over 9 times bigger than ddisasm rewritten binaries on average.

| Tool       | ddisasm | egalito | mctoll  | multiverse | reopt   | retrowrite | uroboros | zipr    |
|------------|---------|---------|---------|------------|---------|------------|----------|---------|
| ddisasm    | 100     | 51.4561 | 100.52  | 10.6701    | 92.6033 | 106.394    | 64.5098  | 63.6723 |
| egalito    | 194.341 | 100     | 189.775 | 21.5827    | 167.908 | 164.719    | 95.5763  | 121.583 |
| mctoll     | 99.4828 | 52.6941 | 100     | 27.0833    | 351.829 | 98.8889    | 129.167  | 84.6041 |
| multiverse | 937.2   | 463.335 | 369.231 | 100        | 854.065 | 907.586    | 567.148  | 624.122 |
| reopt      | 107.987 | 59.5564 | 28.4229 | 11.7087    | 100     | 114.934    | 56.2145  | 69.8109 |
| retrowrite | 93.9901 | 60.7094 | 101.124 | 11.0182    | 87.0065 | 100        | ERR      | 67.4713 |
| uroboros   | 155.015 | 104.628 | 77.4194 | 17.6321    | 177.89  | ERR        | 100      | 109.413 |
| zipr       | 157.054 | 82.2481 | 118.198 | 16.0225    | 143.244 | 148.211    | 91.3966  | 100     |

## Binary Rewriter Performance

To accurately and successfully rewrite a binary executable requires
significant static analysis. These analyses often scale super-linearly
with the size of the program being rewritten. We summarize the average
run time of each tool in .

Table: Average tool runtime in seconds and memory high-water mark across successfully rewritten binaries. {#tbl:perf}

------------ ----------- -------------------
                 Runtime   Memory high-water
    Tool       (seconds)       Mark (kbytes)
  ddisasm         746.79          3355465.52
  egalito         454.40         10433233.07
   mctoll           0.00             1405.30
 multiverse      1201.84           694814.23
   reopt          169.89          4061695.00
 retrowrite       115.07          2008647.93
  uroboros         19.17            93575.58
    zipr          380.04          1305405.48
------------ ----------- -------------------

As with rewritten program size, the reported averages are skewed because
they are calculated across the set of binaries successfully rewritten by
each tool. Thus, rewriters that successfully rewrite larger and more
complicated binaries have an average that skews higher. To account for
this we also present the comparative average in . In each cell, the
comparative average tool runtime across successfully rewritten binaries
by both tools is expressed as a percentage of the row tool to the column
tool. For example, uroboros runtime is roughly 2.3% of the runtime of
ddisasm. A significant trend observable in is that tools with higher
success rates tend to run longer than tools with low rewriting success
rates. This is not surprising as successful tools perform more analyses
and more detailed analyses. They also explicitly handle more portions of
the ELF file and more edge cases. All of this takes time.

Table: Comparative runtime in seconds between rewriting tools.  In each cell, the ratio of the rewritten sizes of those benchmark binaries successfully rewritten by both tools are given as the row tool as a percent of the size of the column tool.  So, e.g., uroboros runtime is roughly 2.3% of the runtime of ddisasm.

| Tool       | ddisasm     | egalito     | mctoll      | multiverse  | reopt       | retrowrite  | uroboros   | zipr        |
|------------|-------------|-------------|-------------|-------------|-------------|-------------|------------|-------------|
| ddisasm    | 100         | 163.637     | 3.61924e+08 | 42.5608     | 439.571     | 563.515     | 4379.81    | 200.044     |
| egalito    | 61.1108     | 100         | 2.19759e+08 | 18.205      | 270.906     | 501.46      | 2123.71    | 128.836     |
| mctoll     | 2.76301e-05 | 4.55044e-05 | 100         | 2.25315e-05 | 0.000121454 | 0.000306798 | 0.00141218 | 6.01777e-05 |
| multiverse | 234.958     | 549.3       | 4.43823e+08 | 100         | 753.241     | 1963.07     | 20446.2    | 863.876     |
| reopt      | 22.7494     | 36.9132     | 8.23357e+07 | 13.276      | 100         | 165.336     | 811.098    | 46.3087     |
| retrowrite | 17.7458     | 19.9418     | 3.25947e+07 | 5.09407     | 60.483      | 100         | 33.4505    | 32.9835     |
| uroboros   | 2.28321     | 4.70874     | 7.08125e+06 | 0.489089    | 12.329      | 298.95      | 100        | 6.27128     |
| zipr       | 49.9891     | 77.6182     | 1.66174e+08 | 11.5757     | 215.942     | 303.182     | 1594.57    | 100         |

## Binary Rewriter Memory High-Water Mark

As with tool runtime, the memory requirements of a binary rewriter may
make rewriting impractical in many circumstances. For a large binary the
memory requirements will frequently outstrip the memory available on a
standard server class machine. We present the average memory high-water
mark during binary rewriting in and for the same reasons as above also
present comparative memory high-water marks in . In each cell, the
comparative memory high-water mark across successfully rewritten
binaries by both tools is expressed as a percentage of the row tool to
the column tool. For example, uroboros' maximum memory consumption is
roughly 2.7% of the maximum memory consumption of ddisasm. Again we see
that successful tools require more resources during rewriting.

Table: Average tool memory high-water mark in kilobytes for those binaries the tool was able to rewrite.

| Tool       | Memory high-water mark |
|------------|------------------------|
| ddisasm    | 3355465.5287081        |
| egalito    | 10433233.077615        |
| mctoll     | 1405.3086124402        |
| multiverse | 694814.23597679        |
| reopt      | 4061695                |
| retrowrite | 2008647.9376947        |
| uroboros   | 93575.588354773        |
| zipr       | 1305405.4888963        |

Table: Comparative memory high-water mark in kilobytes between rewriting tools.  In each cell, the ratio of the rewritten sizes of those benchmark binaries successfully rewritten by both tools are given as the row tool as a percent of the size of the column tool.  So, e.g., uroboros memory consumption roughly 2.7% of the memory consumption of ddisasm.

| Tool       | ddisasm   | egalito   | mctoll  | multiverse | reopt     | retrowrite | uroboros | zipr     |
|------------|-----------|-----------|---------|------------|-----------|------------|----------|----------|
| ddisasm    | 100       | 31.8241   | 238771  | 362.347    | 82.6124   | 162.217    | 3631.61  | 261.96   |
| egalito    | 314.228   | 100       | 741944  | 1431.48    | 259.7     | 489.282    | 11511.5  | 828.157  |
| mctoll     | 0.0418812 | 0.0134781 | 100     | 0.229632   | 0.0345991 | 0.0874121  | 1.64368  | 0.111593 |
| multiverse | 27.5979   | 6.98578   | 43547.9 | 100        | 18.6142   | 63.6278    | 822.128  | 104.691  |
| reopt      | 121.047   | 38.506    | 289025  | 537.224    | 100       | 202.12     | 4336.39  | 318.145  |
| retrowrite | 61.6458   | 20.4381   | 114401  | 157.164    | 49.4755   | 100        | 251.503  | 154.504  |
| uroboros   | 2.7536    | 0.868699  | 6083.9  | 12.1636    | 2.30607   | 39.7609    | 100      | 8.17977  |
| zipr       | 38.1737   | 12.075    | 89611   | 95.5195    | 31.4322   | 64.7234    | 1222.53  | 100      |

## Functionality Against Full Test Suite

For three benchmark programs with readily available test suites we check
the degree to which successful execution of the Null Function test
predicts successful execution of the complete test suite.[^full]  Our
results are presented in . In each cell we report the number of binaries
that completely pass the full test suite and the number of binaries that
pass the Null Function test as "full/null". We report this for each
binary rewriter as well as for the original input binaries files. There
are up to 60 binaries for each program due to the multiple build options
(e.g., compiler, optimization level, pie, stripped, etc.). We do not
include ICX-compiled binaries due to the extra runtime dependencies they
require that impose significant extra burden when running full test
suites in the test environment.

[^full]: We report full results for only three benchmark programs due to the dearth of high-quality test suites for real-world programs and the high level of effort required for properly configuring them.

Overall we find a weak correlation with only 258 rewritten programs
passing their full test suite of the 395 rewritten programs that passed
their Null Function tests. However, it is worth noting that some
original binaries (i.e., inputs to the binary rewriter) that pass the
Null Function test do not pass the full test suite (e.g., `redis` when
compiled with `-Ofast`).

Table: Number of binaries passing their full test suite compared to the number of binaries passing the Null Function test. {#tbl:test-func}

    Tool      lighttpd   nginx   redis
------------ ---------- ------- -------
  original     30/30     60/60   26/30
  ddisasm       0/30     60/60   26/30
  egalito      18/18     18/18   10/10
 multiverse     0/0       0/0     0/0
   reopt        2/19     4/60     2/8
 retrowrite     0/9      16/26    0/0
  uroboros      0/0       0/0     0/0
    zipr       28/30     58/58   22/30

Note that we disable one test in `redis` because it looks for a specific
symbol in the stack trace. This is sufficiently *internal* that we
believe it does not compromise program soundness for binary rewriters to
change this behavior. Most otherwise correct rewrites do change this
stack introspection behavior.

## Rewritten Binary Performance Against Full Test Suite

The performance of rewritten binaries is critical to many use cases for
static binary rewriting. If performance degradation falls below that of
dynamic binary rewriting then in many cases dynamic rewriting is a
better alternative as it is able to leverage dynamic information and
harnessing to more reliably execute the rewritten program. We report the
average change in runtime and memory high-water mark for successfully
rewritten programs as they run against their full test suite in . Only
those rewriters which produced binaries capable of passing all tests are
included. For each rewriter, the percentage change in average runtime
and change in memory high-water mark is calculated only over
successfully rewritten binaries. With the exception of Reopt-rewritten
binaries which had resource consumption at least an order of magnitude
over the original, runtime and memory consumption of the rewritten
binaries is close to that of the original binary. This is especially
true of the memory high-water mark.

Table: Performance of rewritten binaries when run against the full test suite. {#tbl:test-perf}

------------ ----------- -------------------
                 Runtime   Memory High-water
    Tool       (seconds)       Mark (kbytes)
  ddisasm        109.43%             100.21%
  egalito        104.45%              99.85%
   reopt        1324.66%           51937.17%
 retrowrite      103.84%             100.17%
    zipr         102.50%             102.38%
------------ ----------- -------------------

# Discussion {#sec:conclusion}

We identify several trends with respect to binary rewriter IR from our
results. First, rewriting via LLVM IR appears to be infeasible given the
current state of binary type analysis. Only one binary (the trivial
`hello-world`) was successfully instrumented for AFL++ using an LLVM
rewriter (mctoll). Additionally, non-trivial NOP-transformed binaries
successfully produced with reopt had dramatically increased runtime and
memory consumption as compared to the original. Second, direct rewriting
as performed by Egalito and Zipr successfully produced executables even
in the presence of analysis errors; however their ouutput binaries also
demonstrated a higher functional failure rate. Conversely, reassemblable
disassemblers were more likely to raise errors during re-assembling and
re-linking and thus fail to create an executable.

During our evaluation, we communicated with the developers of our
evaluated tools to share our partial results and our benchmark set.
Unsurprisingly, the best-performing tools in our evaluation, ddisasm and
zipr, had sufficient development resources to respond to specific
failures encountered in our work. Thus, their performance against this
evaluation set likely outperforms their expected performance in general.
This is indicative of a *defining* characteristic of binary rewriting at
this point in time; binary rewriting is eminently practical in many
*particular* cases that have been addressed and considered by tool
developers, but impossible in the *general* case as the universe of
binary formats and features is simply too large with too many edge cases
to handle.

We believe that our evaluation indicates that practical applications of
binary rewriting should be preceded by a scoping stage. In this stage,
the target binary is classified as either "in scope" or "out of scope"
for the binary rewriting tool(s) of interest. While scoping can be
accomplished via traditional binary analysis, the high success rate
demonstrated by our simple predictive model with trivially collected
features shows that accurate scoping can be conducted with little
effort. Further, the relative simplicity of our model implies that more
reliable and potentially accurate predictive models are easily within
reach. With such a model, users of binary rewriting tools can quickly
ensure their target binaries meet the expectations of the available
rewriting tools before initiating expensive binary rewriting tasks at
scale. For binaries that are likely to fail during static rewriting, the
user could either conserve their resources by forgoing binary rewriting
or spend them employing more expensive techniques such as dynamic binary
rewriting where necessary.

# Conclusion

In this work, we presented a comparative evaluation of eight binary
rewriting tools on two rewriting tasks across a corpus of 3344 variant
binaries produced using three compilers and 34 source programs. Our
evaluation measured the performance of the tools themselves as well as
the performance and soundness of the rewritten binaries they produce. In
general, our evaluation indicates that binary rewriters that avoid
lifting to machine-independent IRs (e.g., LLVM IR) were most successful
in terms of generality and reliability. Additionally, we identified
binary features that are predictive of rewriting success and showed that
a simple decision tree model trained on these features can accurately
predict whether a particular tool can rewrite a target binary. The
findings and artifacts contributed by this evaluation are intended to
support users and developers of binary rewriting tools and drive
rewriter adoption and maturity.

# Artifact Availability {#artifact-availability .unnumbered}

We have made the full set of artifacts generated in this work including
our evaluation infrastructure, corpus of test binaries, predictive
models, and the evaluated tools publicly available at:
<https://gitlab.com/GrammaTech/lifter-eval> [@lifter-eval].

# Acknowledgment {#acknowledgment .unnumbered}

This material is based upon work supported by the Office of Naval
Research (ONR) under Contract No. N00014-21-C-1032. Any opinions,
findings and conclusions or recommendations expressed in this material
are those of the author(s) and do not necessarily reflect the views of
the ONR.

# References

<div id="refs"></div>

# Appendix

## Predictive Models {#trees}

``` { .Python .numberlines id="ddisasm" caption="Decision tree to predict the success of AFL instrumentation with ddisasm.  Accuracy of 81.47%." }
def ddisasm_tree(note.abi_tag, interp, strip, rela.plt, pi):
  if not note.abi_tag:
    if not interp:
      if strip:
        if interp:
          return {'FAIL': 50.0, 'PASS': 112.0}
        else: # not interp
          return {'FAIL': 37.0, 'PASS': 33.0}
      else: # not strip
        return {'FAIL': 12.0, 'PASS': 0.0}
    else: # interp
      if rela.plt:
        if interp:
          return {'FAIL': 47.0, 'PASS': 910.0}
        else: # not interp
          return {'FAIL': 92.0, 'PASS': 368.0}
      else: # not rela.plt
        return {'FAIL': 10.0, 'PASS': 0.0}
  else: # note.abi_tag
    if not strip:
      return {'FAIL': 53.0, 'PASS': 0.0}
    else: # strip
      if not interp:
        if interp:
          return {'FAIL': 64.0, 'PASS': 11.0}
        else: # not interp
          return {'FAIL': 22.0, 'PASS': 3.0}
      else: # interp
        if not pi:
          if interp:
            return {'FAIL': 215.0, 'PASS': 168.0}
          else: # not interp
            return {'FAIL': 82.0, 'PASS': 38.0}
        else: # pi
          return {'FAIL': 0.0, 'PASS': 15.0}
```

``` { .Python id="mctoll" caption="Decision tree to predict the success of AFL instrumentation with mctoll.  Accuracy of 98.80%." }
def mctoll_tree(note.abi_tag, strip, pi, got.plt,
                data.rel.ro, symtab, note.gnu.build_id):
  if note.abi_tag:
    return {'FAIL': 1672.0, 'PASS': 0.0}
  else: # not note.abi_tag
    if strip:
      if pi:
        if not got.plt:
          if data.rel.ro:
            if not symtab:
              return {'FAIL': 5.0, 'PASS': 6.0}
            else: # symtab
              return {'FAIL': 3.0, 'PASS': 0.0}
          else: # not data.rel.ro
            if symtab:
              return {'FAIL': 21.0, 'PASS': 4.0}
            else: # not symtab
              return {'FAIL': 3.0, 'PASS': 0.0}
        else: # got.plt
          return {'FAIL': 17.0, 'PASS': 0.0}
      else: # not pi
        if symtab:
          if got.plt:
            return {'FAIL': 21.0, 'PASS': 0.0}
          else: # not got.plt
            if not note.gnu.build_id:
              return {'FAIL': 98.0, 'PASS': 6.0}
            else: # note.gnu.build_id
              return {'FAIL': 80.0, 'PASS': 3.0}
        else: # not symtab
          return {'FAIL': 69.0, 'PASS': 0.0}
    else: # not strip
      return {'FAIL': 334.0, 'PASS': 0.0}
```

``` { .Python id="retrowrite" caption="Decision tree to predict the success of AFL instrumentation with retrowrite.  Accuracy of 93.02%." }
def retrowrite_tree(note.gnu.build_id, pi, got.plt,
                    note.abi_tag, rela.plt,
                    data.rel.ro,interp):
  if note.gnu.build_id:
    if not pi:
      return {'FAIL': 531.0, 'PASS': 0.0}
    else: # pi
      if got.plt:
        return {'FAIL': 169.0, 'PASS': 0.0}
      else: # not got.plt
        if note.abi_tag:
          if not note.abi_tag:
            if rela.plt:
              if data.rel.ro:
                return {'FAIL': 36.0, 'PASS': 50.0}
              else: # not data.rel.ro
                return {'FAIL': 78.0, 'PASS': 64.0}
            else: # not rela.plt
              return {'FAIL': 8.0, 'PASS': 0.0}
          else: # note.abi_tag
            if not data.rel.ro:
              return {'FAIL': 64.0, 'PASS': 32.0}
            else: # data.rel.ro
              return {'FAIL': 11.0, 'PASS': 0.0}
        else: # not note.abi_tag
          if interp:
            return {'FAIL': 11.0, 'PASS': 0.0}
          else: # not interp
            if not rela.plt:
              return {'FAIL': 82.0, 'PASS': 36.0}
            else: # rela.plt
              return {'FAIL': 4.0, 'PASS': 0.0}
  else: # not note.gnu.build_id
    return {'FAIL': 1166.0, 'PASS': 0.0}
```

``` { .Python id="zipr" caption="Decision tree to predict the success of AFL instrumentation with zipr.  Accuracy of 79.98%." }
def zipr_tree(got.plt, interp, pi, rela.plt,
              note.gnu.build_id, note.abi_tag,
              strip):
  if not got.plt:
    if got.plt:
      if interp:
        if not interp:
          return {'FAIL': 19.0, 'PASS': 114.0}
        else: # interp
          return {'FAIL': 17.0, 'PASS': 113.0}
      else: # not interp
        if not pi:
          return {'FAIL': 30.0, 'PASS': 103.0}
        else: # pi
          return {'FAIL': 26.0, 'PASS': 108.0}
    else: # not got.plt
      if interp:
        if not pi:
          return {'FAIL': 10.0, 'PASS': 26.0}
        else: # pi
          return {'FAIL': 7.0, 'PASS': 24.0}
      else: # not interp
        if pi:
          if rela.plt:
            return {'FAIL': 14.0, 'PASS': 0.0}
          else: # not rela.plt
            if not note.gnu.build_id:
              if not pi:
                return {'FAIL': 29.0, 'PASS': 5.0}
              else: # pi
                return {'FAIL': 21.0, 'PASS': 10.0}
            else: # note.gnu.build_id
              return {'FAIL': 13.0, 'PASS': 0.0}
        else: # not pi
          return {'FAIL': 0.0, 'PASS': 15.0}
  else: # got.plt
    if not pi:
      if interp:
        if got.plt:
          if not note.abi_tag:
            if not note.gnu.build_id:
              return {'FAIL': 4.0, 'PASS': 10.0}
            else: # note.gnu.build_id
              return {'FAIL': 11.0, 'PASS': 76.0}
          else: # note.abi_tag
            if not note.gnu.build_id:
              return {'FAIL': 0.0, 'PASS': 14.0}
            else: # note.gnu.build_id
              return {'FAIL': 7.0, 'PASS': 29.0}
        else: # not got.plt
          return {'FAIL': 0.0, 'PASS': 16.0}
      else: # not interp
        if not note.abi_tag:
          if not strip:
            if got.plt:
              if not note.gnu.build_id:
                return {'FAIL': 41.0, 'PASS': 133.0}
              else: # note.gnu.build_id
                return {'FAIL': 10.0, 'PASS': 45.0}
            else: # not got.plt
              if not note.gnu.build_id:
                return {'FAIL': 23.0, 'PASS': 43.0}
              else: # note.gnu.build_id
                return {'FAIL': 41.0, 'PASS': 34.0}
          else: # strip
            return {'FAIL': 21.0, 'PASS': 0.0}
        else: # note.abi_tag
          if got.plt:
            if not strip:
              if not note.gnu.build_id:
                return {'FAIL': 63.0, 'PASS': 55.0}
              else: # note.gnu.build_id
                return {'FAIL': 31.0, 'PASS': 27.0}
            else: # strip
              return {'FAIL': 4.0, 'PASS': 0.0}
          else: # not got.plt
            if rela.plt:
              return {'FAIL': 6.0, 'PASS': 0.0}
            else: # not rela.plt
              if not note.gnu.build_id:
                return {'FAIL': 32.0, 'PASS': 9.0}
              else: # note.gnu.build_id
                return {'FAIL': 30.0, 'PASS': 6.0}
    else: # pi
      if not note.gnu.build_id:
        if note.abi_tag:
          return {'FAIL': 30.0, 'PASS': 0.0}
        else: # not note.abi_tag
          if got.plt:
            if not note.abi_tag:
              if interp:
                return {'FAIL': 13.0, 'PASS': 1.0}
              else: # not interp
                return {'FAIL': 134.0, 'PASS': 39.0}
            else: # note.abi_tag
              if interp:
                return {'FAIL': 8.0, 'PASS': 3.0}
              else: # not interp
                return {'FAIL': 98.0, 'PASS': 21.0}
          else: # not got.plt
            if interp:
              return {'FAIL': 0.0, 'PASS': 9.0}
            else: # not interp
              if not note.abi_tag:
                return {'FAIL': 40.0, 'PASS': 22.0}
              else: # note.abi_tag
                return {'FAIL': 37.0, 'PASS': 7.0}
      else: # note.gnu.build_id
        return {'FAIL': 355.0, 'PASS': 0.0}
```
