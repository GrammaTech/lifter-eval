<!-- Ideall we fill this in with a script enabling easy re-runs. -->

Successful executions.
```bash
for i in $(seq 2 9);do
    (
        echo $(head -1 results.txt|cut -f$i)
        echo $(cut -f$i results.txt|grep -c 0,)
        echo $(cut -f$i results.txt|grep -c ,0)
    )|tr '\n' ' '
    echo
done|sed 's/ / | /g;s/^/| /'
```

Successful assemblies.
```bash
for i in $(seq 2 9);do
    (
        echo $(head -1 asm.txt|cut -f$i)
        echo $(cut -f$i asm.txt|grep -c ✓)
    )|tr '\n' ' '
    echo
done|sed 's/ / | /g;s/^/| /'
```

Successful ELF.
```bash
for i in $(seq 2 9);do
    (
        echo $(head -1 elf.txt|cut -f$i)
        echo $(cut -f$i elf.txt|grep -c ✓)
    )|tr '\n' ' '
    echo
done|sed 's/ / | /g;s/^/| /'
```

Everything
```bash
for i in $(seq 2 9);do
    (
        echo $(head -1 results.txt|cut -f$i)
        echo $(cut -f$i results.txt|grep -c  ✓,.,.,.,.)
        echo $(cut -f$i results.txt|grep -c  .,✓,.,.,.)
        echo $(cut -f$i results.txt|grep -c  .,.,0,.,.)
        echo $(cut -f$i results.txt|grep -c  .,.,.,✓,.)
        echo $(cut -f$i results.txt|grep -c  .,.,.,.,0)
    )|tr '\n' ' '
    echo
done|sed 's/ / | /g;s/^/| /'
```

Just the PIE and unstripped binaries.
```bash
cat results.txt|grep pie.nostrip > easy.txt
for i in $(seq 2 9);do
    (
        echo $(head -1 results.txt|cut -f$i)
        echo $(cut -f$i easy.txt|grep -c  ✓,.,.,.,.)
        echo $(cut -f$i easy.txt|grep -c  .,✓,.,.,.)
        echo $(cut -f$i easy.txt|grep -c  .,.,0,.,.)
        echo $(cut -f$i easy.txt|grep -c  .,.,.,✓,.)
        echo $(cut -f$i easy.txt|grep -c  .,.,.,.,0)
    )|tr '\n' ' '
    echo
done|sed 's/ / | /g;s/^/| /'
rm easy.txt
```

Broken out by compiler.
```bash
for compiler in gcc clang icx ollvm;do
  cat results.txt|grep $compiler > comp.txt
  (
      echo $compiler
      wc -l comp.txt
  )|tr '\n' ' '
  echo
  for i in $(seq 2 9);do
      (
          echo $(head -1 results.txt|cut -f$i)
          echo $(cut -f$i comp.txt|grep -c  ✓,.,.,.,.)
          echo $(cut -f$i comp.txt|grep -c  .,✓,.,.,.)
          echo $(cut -f$i comp.txt|grep -c  .,.,0,.,.)
          echo $(cut -f$i comp.txt|grep -c  .,.,.,✓,.)
          echo $(cut -f$i comp.txt|grep -c  .,.,.,.,0)
      )|tr '\n' ' '
      echo
  done|sed 's/ / | /g;s/^/| /'
  rm comp.txt
  echo
done
```

Change in size of binaries after rewriting.

```bash
for i in $(seq 2 9);do
  echo "$(head -1 sizes.txt|cut -f$i) $(cut -f$i sizes.txt|grep -v '∅'|sed '1d'|datamash mean 1)"
done|sed 's/ / | /;s/^/| /'
```

Comparative size.
```bash
(
  echo -n "Tool "
  head -1 sizes.txt|cut -f2-
  for i in $(seq 2 9);do
    echo -n "$(head -1 sizes.txt|cut -f$i) "
    for j in $(seq 2 9);do
    if [ $i -eq $j ];then
      echo -n "100 "
    elif [ $i -lt $j ];then
      echo -n "$(cut -f$i,$j sizes.txt|grep -v ∅|awk '{left += $1; right += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    else
      echo -n "$(cut -f$i,$j sizes.txt|grep -v ∅|awk '{right += $1; left += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    fi
    done
  echo
  done 2>/dev/null
)|sed 's/[[:space:]]/ | /g;s/^/| /'
```

## Tool Runtime

```bash
for i in $(seq 2 8);do
  echo "$(head -1 perf.txt|cut -f$i) $(cut -f$i perf.txt|sed 's/|[^[:space:]]\+//g;s/|/∅/g'|sed '1d'|grep -v '∅'|datamash mean 1)"
done|sed 's/ / | /;s/^/| /'
```

comparative runtime

```bash
(
  echo -n "Tool "
  head -1 perf.txt|cut -f2-
  for i in $(seq 2 9);do
    echo -n "$(head -1 perf.txt|cut -f$i) "
    for j in $(seq 2 9);do
    if [ $i -eq $j ];then
      echo -n "100 "
    elif [ $i -lt $j ];then
      echo -n "$(cut -f$i,$j perf.txt|sed 's/|[^[:space:]]\+//g;s/|/∅/g'|grep -v ∅|awk '{left += $1; right += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    else
      echo -n "$(cut -f$i,$j perf.txt|sed 's/|[^[:space:]]\+//g;s/|/∅/g'|grep -v ∅|awk '{right += $1; left += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    fi
    done
  echo
  done 2>/dev/null
)|sed 's/[[:space:]]/ | /g;s/^/| /'
```

## Tool Memory High-water Mark

```bash
for i in $(seq 2 9);do
  echo "$(head -1 perf.txt|cut -f$i) $(cut -f$i perf.txt|sed 's/[^[:space:]]\+|//g;s/|/∅/g'|sed '1d'|grep -v '∅'|datamash mean 1)"
done|sed 's/ / | /;s/^/| /'
```

```bash
(
  echo -n "Tool "
  head -1 perf.txt|cut -f2-
  for i in $(seq 2 9);do
    echo -n "$(head -1 perf.txt|cut -f$i) "
    for j in $(seq 2 9);do
    if [ $i -eq $j ];then
      echo -n "100 "
    elif [ $i -lt $j ];then
      echo -n "$(cut -f$i,$j perf.txt|sed 's/[^[:space:]]\+|//g;s/|/∅/g'|grep -v ∅|awk '{left += $1; right += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    else
      echo -n "$(cut -f$i,$j perf.txt|sed 's/[^[:space:]]\+|//g;s/|/∅/g'|grep -v ∅|awk '{right += $1; left += $2} END { print ((left / right) * 100)}' || echo "ERR") "
    fi
    done
  echo
  done 2>/dev/null
)|sed 's/[[:space:]]/ | /g;s/^/| /'
```
