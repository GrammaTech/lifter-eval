# From: https://learn.co/lessons/dsc-decision-trees-with-sklearn-codealong
import sys
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import _tree
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import OneHotEncoder
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn import tree

try:
    sys.argv[1]
    df = pd.read_csv(sys.argv[1])
except:
    print("Require CSV input as first command line argument.")
    sys.exit(1)

k = 4

x = df[
    [
        "pi",
        "strip",
        "rela.plt",
        "note.abi-tag",
        "interp",
        "note.gnu.build-id",
        "got.plt",
        "data.rel.ro",
        "symtab",
        "strtab",
        "plt.got",
        "gcc_except_table",
        "jcr",
        "debug_str",
        "debug_line",
        "debug_info",
        "debug_abbrev",
        "note.gnu.property",
        "debug_ranges",
    ]
]

def uniq(lst):
    return list(dict.fromkeys(lst))

for rewriter in [
    "ddisasm",
    "egalito",
    "mctoll",
    "multiverse",
    "reopt",
    "retrowrite",
    "uroboros",
    "zipr",
]:
    y = df[[rewriter]]
    if not len(y.value_counts()) == 2:
        print(
            f"Every output {y.values[0][0]} for {rewriter} so no need for a predictor.",
            file=sys.stderr,
        )
        continue

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)

    # One-hot encode the training data and show the resulting DataFrame with proper column names
    ohe = OneHotEncoder()
    ohe.fit(x_train)
    x_train_ohe = ohe.transform(x_train).toarray()

    # Create the classifier, fit it on the training data and make predictions on the test set
    # clf = DecisionTreeClassifier()
    clf = Pipeline(
        [
            ("feature_selection", SelectFromModel(LinearSVC())),
            # ('classification', RandomForestClassifier())
            ("classification", DecisionTreeClassifier()),
        ]
    )
    clf.fit(x_train_ohe, y_train.values.ravel())

    x_test_ohe = ohe.transform(x_test)
    y_preds = clf.predict(x_test_ohe)

    # tree.plot_tree(clf)
    # plt.show()

    x_test_ohe = ohe.transform(x_test)
    y_preds = clf.predict(x_test_ohe)

    # Understanding a tree.
    # From: https://stackoverflow.com/questions/20224526/how-to-extract-the-decision-rules-from-scikit-learn-decision-tree
    feature_names = ohe.get_feature_names(x_train.columns)

    def tree_feature_names(tree):
        return list(map(lambda n: feature_names[n], filter(lambda n: n != _tree.TREE_UNDEFINED, tree.tree_.feature)))

    def nice_name(name):
        name = name.replace("-", "_")
        if name == "pi_DYN":
            return ("pi", True)
        elif name == "pi_EXEC":
            return ("pi", False)
        elif name == "strip_stripped":
            return ("strip", True)
        elif name == "strip_not_stripped":
            return ("strip", False)
        elif name[-4:] == "_YES":
            return (name[0:-4], True)
        elif name[-3:] == "_NO":
            return (name[0:-3], False)
        else:
            print(f'BAD NAME {name}', file=sys.stderr)
            sys.stderr.flush()
            assert(False)

    def feature(node_id):
        feature_id = clf.steps[-1][1].tree_.feature[node_id]
        if feature_id == _tree.TREE_UNDEFINED:
            return ("UNDEFINED", False)
        else:
            return feature_names[feature_id]

    def tree_to_code(tree):
        tree_ = tree.tree_
        feature_name = [
            feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!" for i in tree_.feature
        ]
        tree_parameters = list(uniq(map(lambda name: nice_name(name)[0], tree_feature_names(tree))))
        print(f'def {rewriter}_tree({", ".join(tree_parameters)}):')

        def recurse(node, depth):
            indent = "  " * depth
            if tree_.feature[node] != _tree.TREE_UNDEFINED:
                nice, positive = nice_name(feature_name[node])
                threshold = tree_.threshold[node]
                if threshold == 0.5:
                    if positive:
                        print(f"{indent}if not {nice}:")
                    else:
                        print(f"{indent}if {nice}:")
                else:
                    if positive:
                        print(f"{indent}if {nice} <= {threshold}:")
                    else:
                        print(f"{indent}if {nice} > {threshold}:")
                recurse(tree_.children_left[node], depth + 1)
                if threshold == 0.5:
                    if positive:
                        print(f"{indent}else: # {nice}")
                    else:
                        print(f"{indent}else: # not {nice}")
                else:
                    if positive:
                        print(f"{indent}else:  # {nice} > {threshold}")
                    else:
                        print(f"{indent}else:  # {nice} <= {threshold}")
                recurse(tree_.children_right[node], depth + 1)
            else:
                result = {"FAIL": tree_.value[node][0][0], "PASS": tree_.value[node][0][1]}
                print(f"{indent}return {result}")

        recurse(0, 1)

    selector = SelectKBest(chi2, k=2*k)
    selector.fit_transform(x_train_ohe, y_train)

    print("################################################################################")
    padding = (78 - len(rewriter)) / 2
    print(f'{"#" * math.floor(padding)} {rewriter} {"#" * math.ceil(padding)}')
    print("##")
    print(f"## {k} most individually discriminating features are:")
    best_features = "\n## - ".join(
        uniq(map(
            lambda pair: nice_name(pair[1])[0],
            filter(lambda pair: pair[0], zip(selector.get_support(), feature_names)),
        ))[0:k]
    )
    print(f"## - {best_features}")
    print("##")
    print(f"## Decision tree accuracy is {accuracy_score(y_test, y_preds)}")
    print("################################################################################")
    tree_to_code(clf.steps[-1][1])
    print("")
    print("")
